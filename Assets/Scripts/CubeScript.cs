using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using WWUtils.Audio;
using UnityToolbag;

using Houndify;

public class CubeScript : MonoBehaviour
{
    public TextMesh caption;
    public Material defaultMat;
    public Material speakingMat;
    
    public AudioSource audioSource = null;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // Did the user press the T key?
        if (Input.GetKeyDown(KeyCode.T))
        {
            // Change the color of the cube
            this.GetComponent<Renderer>().material = speakingMat;
            // Make the Houndify request in a background thread
            var date = DateTime.Now;
            var text = String.Format("Welcome to this space. It's {0}:{1}.", (date.Hour % 12), date.Minute);
            // We do this in a background thread so the animation doesn't freeze
            Task.Run(() => SpeakThis(text));
        }

        // Update the cube rotation
        transform.Rotate(0f, 30.0f * Time.deltaTime, 0f);
    }


    private void SpeakThis(string text) {
        // This is a workaround for the program to work on a system that's not on English
        System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

        HoundCloudRequester requester = new HoundCloudRequester(Settings.ClientId, Settings.ClientKey, Settings.UserId);

        RequestInfoJSON.TypeClientVersion client_version = new RequestInfoJSON.TypeClientVersion();
        client_version.key = 0;
        client_version.choice0 = "1.0";

        // RequestInfoJSON class contains metadata about the current request, like session_id, request id, client_version, etc
        RequestInfoJSON request_info = new RequestInfoJSON();
        request_info.setUnitPreference(RequestInfoJSON.TypeUnitPreference.UnitPreference_US);
        request_info.setRequestID(Guid.NewGuid().ToString());
        request_info.setClientVersion(client_version);
        request_info.setInputLanguageEnglishName("English");
        request_info.setOutputLanguageEnglishName("English");

        // Here we create a client match command and put it in the request info
        // The command will only match the text "speak_this" (which we will send
        // later as a text query). The result of this command will be the text
        // variable that was passed into this function.
        JSONObjectValue clientMatch = new JSONObjectValue();
        clientMatch.setField("Expression", new JSONStringValue("speak_this"));
        clientMatch.setField("Result", new JSONObjectValue());
        clientMatch.setField("SpokenResponse", new JSONStringValue(text));
        clientMatch.setField("SpokenResponseLong", new JSONStringValue(text));
        clientMatch.setField("WrittenResponse", new JSONStringValue(text));
        clientMatch.setField("WrittenResponseLong", new JSONStringValue(text));
        JSONArrayValue clientMatchesArray = new JSONArrayValue();
        clientMatchesArray.appendComponent(clientMatch);
        request_info.extraRequestInfoSetField("ClientMatches", clientMatchesArray);

        // These parameters sets the voice. You can try different voices like:
        // Lily, Ryan, Tracy or any of the ones mentioned in the documentation of 
        // the TTS domain (link below)
        // https://www.houndify.com/domains/540c271e-cf06-4f33-ab26-731f0bd9b79d
        request_info.setResponseAudioVoice(Settings.TTSVoice);
        request_info.setResponseAudioShortOrLong("Long");

        // The HoundServerJSON class is used to handle all server responses
        HoundServerJSON hound_result = requester.do_text_request("speak_this", null, request_info);
        CommandResultJSON my_answer = hound_result.getAllResults()[0];
        string bytes_audio = my_answer.getResponseAudioBytes();
        byte[] bytes = System.Convert.FromBase64String(bytes_audio);

        // Once we're done hitting the Houndify API we can dispatch
        // events on the main thread (in this case we play audio and
        // revert the color of the cube back to default)
        Dispatcher.Invoke(() =>
        {
            audioSource = GetComponent<AudioSource>();

            Debug.Log("Playing response");

            WAV wav = new WAV(bytes);
            AudioClip audioClip = AudioClip.Create("outputSound", wav.SampleCount, 1, wav.Frequency, false, false);
            audioClip.SetData(wav.LeftChannel, 0);
            audioSource.clip = audioClip;
            audioSource.Play();
            
            this.GetComponent<Renderer>().material = defaultMat;
        });

    }
}
