/* file "BuildInfo.cs" */

/* Copyright 2015, 2016 SoundHound, Incorporated.  All rights reserved. */


public class BuildInfo
  {
    static public string SDKVariant = "Slim";
    static public string SDKPrivateDomains = "";
    static public string BuildUser = "jenkinslave";
    static public string BuildDate = "Sun Oct 24 13:44:11 PDT 2021";
    static public string BuildMachine = "b2xlmr2.pnp.melodis.com";
    static public string BuildSVNRevision = "local changes";
    static public string BuildSVNBranch = "local changes";
    static public string BuildGitCommit = "981cabf8e056e1c1d26eaee6ea02d146883f1ee6";
    static public string BuildGitBranch = "origin/master";
    static public string BuildNumber = "7335";
    static public string BuildKind = "Houndify";
    static public string BuildVariant = "unknown";
  }
