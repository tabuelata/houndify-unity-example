/* file "RequestInfoJSON.cs" */

/* Generated automatically by Classy JSON. */


using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.IO;
using System.Numerics;


public class RequestInfoJSON : JSONBase
  {
    public class TypeDeviceInfoJSON : JSONBase
      {
        public enum TypeDriverSideKnownValues
          {
            DriverSide_Left,
            DriverSide_Right,
            DriverSide__none
          };
        public struct TypeDriverSide
          {
            public bool in_known_list;
            public string string_value;
            public TypeDriverSideKnownValues list_value;
          };

        public static TypeDriverSideKnownValues  stringToDriverSide(string chars)
          {
            switch (chars[0])
              {
                case 'L':
                    if ((String.Compare(chars, 1, "eft", 0, 3, false) == 0) && (chars.Length == 4))
                        return TypeDriverSideKnownValues.DriverSide_Left;
                    break;
                case 'R':
                    if ((String.Compare(chars, 1, "ight", 0, 4, false) == 0) && (chars.Length == 5))
                        return TypeDriverSideKnownValues.DriverSide_Right;
                    break;
                default:
                    break;
              }
            return TypeDriverSideKnownValues.DriverSide__none;
          }

        public static string  stringFromDriverSide(TypeDriverSideKnownValues the_enum)
          {
            switch (the_enum)
              {
                case TypeDriverSideKnownValues.DriverSide_Left:
                    return "Left";
                case TypeDriverSideKnownValues.DriverSide_Right:
                    return "Right";
                default:
                    Debug.Assert(false);
                    return null;
              }
          }

        private bool flagHasDeviceModel;
        private string storeDeviceModel;
        private bool flagHasDeviceYear;
        private JSONValue  storeDeviceYear;
        private bool flagHasDeviceName;
        private string storeDeviceName;
        private bool flagHasDriverSide;
        private TypeDriverSide storeDriverSide;
        private List<string> extraKeys;
        private List<JSONValue > extraValues;
        private Dictionary<string, JSONValue > extraIndex;


        private void  fromJSONDeviceModel(JSONValue json_value, bool ignore_extras)
          {
            Debug.Assert(json_value != null);
            JSONStringValue json_string = json_value.string_value();
            if (json_string == null)
                throw new Exception("The value for field DeviceModel of TypeDeviceInfoJSON is not a string.");
            setDeviceModel(json_string.getData());
          }


        private void  fromJSONDeviceYear(JSONValue json_value, bool ignore_extras)
          {
            Debug.Assert(json_value != null);
            setDeviceYear(json_value);
          }


        private void  fromJSONDeviceName(JSONValue json_value, bool ignore_extras)
          {
            Debug.Assert(json_value != null);
            JSONStringValue json_string = json_value.string_value();
            if (json_string == null)
                throw new Exception("The value for field DeviceName of TypeDeviceInfoJSON is not a string.");
            setDeviceName(json_string.getData());
          }


        private void  fromJSONDriverSide(JSONValue json_value, bool ignore_extras)
          {
            Debug.Assert(json_value != null);
            JSONStringValue json_string = json_value.string_value();
            if (json_string == null)
                throw new Exception("The value for field DriverSide of TypeDeviceInfoJSON is not a string.");
            TypeDriverSide the_open_enum = new TypeDriverSide();
            switch (json_string.getData()[0])
              {
                case 'L':
                    if ((String.Compare(json_string.getData(), 1, "eft", 0, 3, false) == 0) && (json_string.getData().Length == 4))
                          {
                            the_open_enum.in_known_list = true;
                            the_open_enum.list_value = TypeDriverSideKnownValues.DriverSide_Left;
                            goto open_enum_is_done;
                          }
                    break;
                case 'R':
                    if ((String.Compare(json_string.getData(), 1, "ight", 0, 4, false) == 0) && (json_string.getData().Length == 5))
                          {
                            the_open_enum.in_known_list = true;
                            the_open_enum.list_value = TypeDriverSideKnownValues.DriverSide_Right;
                            goto open_enum_is_done;
                          }
                    break;
                default:
                    break;
              }
            the_open_enum.in_known_list = false;
            the_open_enum.string_value = json_string.getData();
          open_enum_is_done:;
            setDriverSide(the_open_enum);
          }


        public TypeDeviceInfoJSON()
          {
            flagHasDeviceModel = false;
            flagHasDeviceYear = false;
            flagHasDeviceName = false;
            flagHasDriverSide = false;
            extraKeys = new List<string>();
        extraValues = new List<JSONValue >();
        extraIndex = new Dictionary<string, JSONValue >();
          }

        public bool  hasDeviceModel()
          {
            return flagHasDeviceModel;
          }

        public string  getDeviceModel()
          {
            Debug.Assert(flagHasDeviceModel);
            return storeDeviceModel;
          }

        public bool  hasDeviceYear()
          {
            return flagHasDeviceYear;
          }

        public JSONValue   getDeviceYear()
          {
            Debug.Assert(flagHasDeviceYear);
            return storeDeviceYear;
          }

        public bool  hasDeviceName()
          {
            return flagHasDeviceName;
          }

        public string  getDeviceName()
          {
            Debug.Assert(flagHasDeviceName);
            return storeDeviceName;
          }

        public bool  hasDriverSide()
          {
            return flagHasDriverSide;
          }

        public TypeDriverSide  getDriverSide()
          {
            Debug.Assert(flagHasDriverSide);
            return storeDriverSide;
          }

        public string  getDriverSideAsString()
          {
            TypeDriverSide result = getDriverSide();
            if (result.in_known_list)
                return stringFromDriverSide(result.list_value);
            else
                return result.string_value;
          }


        public virtual int extraTypeDeviceInfoComponentCount()
          {
            Debug.Assert(extraKeys.Count == extraValues.Count);
            return extraKeys.Count;
          }
        public virtual string extraTypeDeviceInfoComponentKey(int component_num)
          {
            Debug.Assert(extraKeys.Count == extraValues.Count);
            Debug.Assert(component_num < extraValues.Count);
            return extraKeys[component_num];
          }
        public virtual JSONValue extraTypeDeviceInfoComponentValue(int component_num)
          {
            Debug.Assert(extraKeys.Count == extraValues.Count);
            Debug.Assert(component_num < extraValues.Count);
            return extraValues[component_num];
          }
        public virtual JSONValue extraTypeDeviceInfoLookup(string field_name)
          {
            JSONValue result = (extraIndex.ContainsKey(field_name) ? extraIndex[field_name] : null);
            return result;
          }

        public void setDeviceModel(string new_value)
          {
            flagHasDeviceModel = true;
            storeDeviceModel = new_value;
          }
        public void unsetDeviceModel()
          {
            flagHasDeviceModel = false;
          }
        public void setDeviceYear(JSONValue  new_value)
          {
            if (flagHasDeviceYear)
              {
              }
            flagHasDeviceYear = true;
            storeDeviceYear = new_value;
          }
        public void unsetDeviceYear()
          {
            if (flagHasDeviceYear)
              {
              }
            flagHasDeviceYear = false;
          }
        public void setDeviceName(string new_value)
          {
            flagHasDeviceName = true;
            storeDeviceName = new_value;
          }
        public void unsetDeviceName()
          {
            flagHasDeviceName = false;
          }
        public void setDriverSide(TypeDriverSide new_value)
          {
            flagHasDriverSide = true;
            storeDriverSide = new_value;
          }
        public void setDriverSide(string chars)
          {
            TypeDriverSideKnownValues known = stringToDriverSide(chars);
            TypeDriverSide new_value = new TypeDriverSide();
            if (known == TypeDriverSideKnownValues.DriverSide__none)
              {
                new_value.in_known_list = false;
                new_value.string_value = chars;
              }
            else
              {
                new_value.in_known_list = true;
                new_value.list_value = known;
              }
            setDriverSide(new_value);
          }
        public void setDriverSide(TypeDriverSideKnownValues new_value)
          {
            TypeDriverSide new_full_value = new TypeDriverSide();
            Debug.Assert(new_value != TypeDriverSideKnownValues.DriverSide__none);
            new_full_value.in_known_list = true;
            new_full_value.list_value = new_value;
            setDriverSide(new_full_value);
          }
        public void unsetDriverSide()
          {
            flagHasDriverSide = false;
          }

        public virtual void extraTypeDeviceInfoAppendPair(string key, JSONValue new_component)
          {
            Debug.Assert(key != null);
            Debug.Assert(new_component != null);

            Debug.Assert(extraKeys.Count == extraValues.Count);
            extraKeys.Add(key);
            extraValues.Add(new_component);
            extraIndex.Add(key, new_component);
          }
        public virtual void extraTypeDeviceInfoSetField(string key, JSONValue new_component)
          {
            Debug.Assert(key != null);
            Debug.Assert(new_component != null);

            JSONValue old_field = extraTypeDeviceInfoLookup(key);
            if (old_field == null)
              {
                extraTypeDeviceInfoAppendPair(key, new_component);
              }
            else
              {
                int count = extraKeys.Count;
                Debug.Assert(count == extraValues.Count);
                for (int num = 0; num < count; ++num)
                  {
                    if (extraKeys[num].Equals( key))
                      {
                        extraValues[num] = new_component;
                        break;
                      }
                  }
                extraIndex.Add(key, new_component);
              }
          }

        public override void write_as_json(JSONHandler handler)
          {
            handler.start_object();
            write_fields_as_json(handler);
            int extra_count = extraKeys.Count;
            Debug.Assert(extra_count == extraValues.Count);
            for (int extra_num = 0; extra_num < extra_count; ++extra_num)
              {
                handler.start_pair(extraKeys[extra_num]);
                extraValues[extra_num].write(handler);
              }
            handler.finish_object();
          }

        public virtual void write_fields_as_json(JSONHandler handler)
          {
            write_fields_as_json(handler, false);
          }

        public virtual void write_fields_as_json(JSONHandler handler, bool partial_allowed)
          {
            if (flagHasDeviceModel)
              {
                handler.start_pair("DeviceModel");
                handler.string_value(storeDeviceModel);
              }
            if (flagHasDeviceYear)
              {
                handler.start_pair("DeviceYear");
                storeDeviceYear.write(handler);
              }
            if (flagHasDeviceName)
              {
                handler.start_pair("DeviceName");
                handler.string_value(storeDeviceName);
              }
            if (flagHasDriverSide)
              {
                handler.start_pair("DriverSide");
                if (storeDriverSide.in_known_list)
                  {
                    switch (storeDriverSide.list_value)
                      {
                        case TypeDriverSideKnownValues.DriverSide_Left:
                            handler.string_value("Left");
                            break;
                        case TypeDriverSideKnownValues.DriverSide_Right:
                            handler.string_value("Right");
                            break;
                        default:
                            Debug.Assert(false);
                            break;
                      }
                  }
                else
                  {
                            handler.string_value(storeDriverSide.string_value);
                  }
              }
          }
        public override void write_partial_as_json(JSONHandler handler)
          {
            handler.start_object();
            write_fields_as_json(handler, true);
            int extra_count = extraKeys.Count;
            Debug.Assert(extra_count == extraValues.Count);
            for (int extra_num = 0; extra_num < extra_count; ++extra_num)
              {
                handler.start_pair(extraKeys[extra_num]);
                extraValues[extra_num].write(handler);
              }
            handler.finish_object();
          }
        public virtual string missing_field_error(bool allow_unpolished)
          {
            return null;
          }

        public static TypeDeviceInfoJSON from_json(JSONValue json_value, bool ignore_extras, bool allow_incomplete, bool allow_unpolished)
          {
            TypeDeviceInfoJSON result;
              {
                HoldingGenerator generator = new HoldingGenerator("Type TypeDeviceInfo", ignore_extras);
                generator.set_allow_incomplete(allow_incomplete);
                generator.set_allow_unpolished(allow_unpolished);
                if (allow_incomplete || allow_unpolished)
                    json_value.write(generator);
                else
                    json_value.write(generator);
                Debug.Assert(generator.have_value);
                result = generator.value;
              };
            return result;
          }
        public static TypeDeviceInfoJSON from_json(JSONValue json_value, bool ignore_extras, bool allow_incomplete)
      {
        return from_json(json_value, ignore_extras, allow_incomplete, false);
      }
        public static TypeDeviceInfoJSON from_json(JSONBase json_value, bool ignore_extras, bool allow_incomplete, bool allow_unpolished)
          {
            TypeDeviceInfoJSON result;
              {
                HoldingGenerator generator = new HoldingGenerator("Type TypeDeviceInfo", ignore_extras);
                generator.set_allow_incomplete(allow_incomplete);
                generator.set_allow_unpolished(allow_unpolished);
                if (allow_incomplete || allow_unpolished)
                    json_value.write_partial_as_json(generator);
                else
                    json_value.write_as_json(generator);
                Debug.Assert(generator.have_value);
                result = generator.value;
              };
            return result;
          }
        public static TypeDeviceInfoJSON from_json(JSONBase json_value, bool ignore_extras, bool allow_incomplete)
      {
        return from_json(json_value, ignore_extras, allow_incomplete, false);
      }
        public static TypeDeviceInfoJSON from_text(string text, bool ignore_extras)
          {
            TypeDeviceInfoJSON result;
              {
                HoldingGenerator generator = new HoldingGenerator("Type TypeDeviceInfo", ignore_extras);
                JSONParse.parse_json_value(text, "Text for TypeDeviceInfoJSON", generator);
                Debug.Assert(generator.have_value);
                result = generator.value;
              };
            return result;
          }
        public static TypeDeviceInfoJSON from_file(TextReader fp, string file_name, bool ignore_extras)
          {
            TypeDeviceInfoJSON result;
              {
                HoldingGenerator generator = new HoldingGenerator("Type TypeDeviceInfo", ignore_extras);
                JSONParse.parse_json_value(fp, file_name, generator);
                Debug.Assert(generator.have_value);
                result = generator.value;
              };
            return result;
          }
        public abstract class Generator : JSONObjectGenerator
          {
            private JSONHoldingStringGenerator fieldGeneratorDeviceModel;
            private JSONHoldingValueGenerator fieldGeneratorDeviceYear;
            private JSONHoldingStringGenerator fieldGeneratorDeviceName;
        private abstract class FieldGeneratorDriverSide : JSONStringGenerator
              {
                protected FieldGeneratorDriverSide(string what)
                  {
                    set_what(what);
                  }
                protected FieldGeneratorDriverSide()
                  {
                  }
                protected override void handle_result(string result)
                  {
                    TypeDriverSideKnownValues known = stringToDriverSide(result);
                    TypeDriverSide new_value = new TypeDriverSide();
                    if (known == TypeDriverSideKnownValues.DriverSide__none)
                      {
                        new_value.in_known_list = false;
                        new_value.string_value = result;
                      }
                    else
                      {
                        new_value.in_known_list = true;
                        new_value.list_value = known;
                      }
                    handle_result(new_value);
                  }
                protected abstract void handle_result(TypeDriverSide result);
              };
        private class FieldHoldingGeneratorDriverSide : FieldGeneratorDriverSide
      {
        protected override void handle_result(TypeDriverSide result)
          {
    //@@@        Debug.Assert(!have_value);
            have_value = true;
            value = result;
          }

        public FieldHoldingGeneratorDriverSide(String what)
          {
            have_value = false;
            base.set_what(what);
          }

        public override void reset()
          {
            have_value = false;
            base.reset();
          }

        public bool have_value;
        public TypeDriverSide value;
      };
        private class FieldHoldingArrayGeneratorDriverSide : JSONArrayGenerator
      {
        protected class ElementHandler : FieldGeneratorDriverSide
          {
            private FieldHoldingArrayGeneratorDriverSide top;

            protected override void handle_result(TypeDriverSide result)
              {
                top.value.Add(result);
              }
            protected override string get_what()
              {
                return "element " + top.value.Count + " of " + top.get_what();
              }

            public ElementHandler(FieldHoldingArrayGeneratorDriverSide init_top)
              {
                top = init_top;
              }
          };

        private ElementHandler element_handler;

        protected override JSONHandler start()
          {
            have_value = true;
            value.Clear();
            return element_handler;
          }
        protected override void finish()
          {
            Debug.Assert(have_value);
            handle_result(value);
            element_handler.reset();
          }
        protected virtual void handle_result(List<TypeDriverSide> result)
          {
          }

        public FieldHoldingArrayGeneratorDriverSide(string what)
          {
            element_handler = new ElementHandler(this);
            have_value = false;
            value = new List<TypeDriverSide>();
            base.set_what(what);
          }
        public FieldHoldingArrayGeneratorDriverSide()
          {
            element_handler = new ElementHandler(this);
            have_value = false;
            value = new List<TypeDriverSide>();
          }

        public override void reset()
          {
            element_handler.reset();
            have_value = false;
            value.Clear();
            base.reset();
          }

        public bool have_value;
        public List<TypeDriverSide> value;
      };
            private FieldHoldingGeneratorDriverSide fieldGeneratorDriverSide;
            private class UnknownFieldGenerator : JSONValueHandler
              {
                public bool ignore;
                public List<string> field_names;
                public List<JSONValue > field_values;
                public Dictionary<string, JSONValue > index;
                public UnknownFieldGenerator(bool init_ignore)
                  {
                    ignore = init_ignore;
                    field_names = new List<string>();
                    field_values = new List<JSONValue >();
                index = new Dictionary<string, JSONValue >();
                  }

                protected override void new_value(JSONValue item)
                  {
                    if (ignore)
                        return;
                    Debug.Assert(field_names.Count == (field_values.Count + 1));
                    index.Add(field_names[field_values.Count], item);
                    field_values.Add(item);
                  }
                public override void reset()
                  {
                    field_names.Clear();
                    field_values.Clear();
                index = new Dictionary<string, JSONValue >();
                  }
              };
            private UnknownFieldGenerator unknownFieldGenerator;

            protected bool allow_incomplete;

            protected bool allow_unpolished;

            protected override void start()
              {
              }
            protected override JSONHandler start_field(string field_name)
              {
                JSONHandler result = start_known_field(field_name);
                if (result != null)
                    return result;
                Debug.Assert(unknownFieldGenerator.field_names.Count ==
                       unknownFieldGenerator.field_values.Count);
                if (unknownFieldGenerator.ignore)
                  {
                    Debug.Assert(unknownFieldGenerator.field_names.Count == 0);
                  }
                else
                  {
                    unknownFieldGenerator.field_names.Add(field_name);
                  }
                return unknownFieldGenerator;
              }
            protected override void finish_field(string field_name, JSONHandler field_handler)
              {
              }
            protected override void finish()
              {
                TypeDeviceInfoJSON result = new TypeDeviceInfoJSON();
                Debug.Assert(result != null);
                finish(result);
                int extra_count = unknownFieldGenerator.field_names.Count;
                Debug.Assert(extra_count == unknownFieldGenerator.field_values.Count);
                for (int extra_num = 0; extra_num < extra_count; ++extra_num)
                  {
                    result.extraTypeDeviceInfoAppendPair(unknownFieldGenerator.field_names[extra_num], unknownFieldGenerator.field_values[extra_num]);
                  }
                unknownFieldGenerator.field_names.Clear();
                unknownFieldGenerator.field_values.Clear();
                unknownFieldGenerator.index = new Dictionary<string, JSONValue >();
                handle_result(result);
              }
            protected void finish(TypeDeviceInfoJSON result)
              {
                if (fieldGeneratorDeviceModel.have_value)
                  {
                    result.setDeviceModel(fieldGeneratorDeviceModel.value);
                    fieldGeneratorDeviceModel.have_value = false;
                  }
                if (fieldGeneratorDeviceYear.have_value)
                  {
                    result.setDeviceYear(fieldGeneratorDeviceYear.value);
                    fieldGeneratorDeviceYear.have_value = false;
                  }
                if (fieldGeneratorDeviceName.have_value)
                  {
                    result.setDeviceName(fieldGeneratorDeviceName.value);
                    fieldGeneratorDeviceName.have_value = false;
                  }
                if (fieldGeneratorDriverSide.have_value)
                  {
                    result.setDriverSide(fieldGeneratorDriverSide.value);
                    fieldGeneratorDriverSide.have_value = false;
                  }
              }
            protected abstract void handle_result(TypeDeviceInfoJSON new_result);
            protected virtual JSONHandler start_known_field(string field_name)
              {
                if (String.Compare(field_name, 0, "D", 0, 1, false) == 0)
                  {
                    switch (field_name[1])
                      {
                        case 'e':
                            if (String.Compare(field_name, 2, "vice", 0, 4, false) == 0)
                              {
                                switch (field_name[6])
                                  {
                                    case 'M':
                                        if ((String.Compare(field_name, 7, "odel", 0, 4, false) == 0) && (field_name.Length == 11))
                                            return fieldGeneratorDeviceModel;
                                        break;
                                    case 'N':
                                        if ((String.Compare(field_name, 7, "ame", 0, 3, false) == 0) && (field_name.Length == 10))
                                            return fieldGeneratorDeviceName;
                                        break;
                                    case 'Y':
                                        if ((String.Compare(field_name, 7, "ear", 0, 3, false) == 0) && (field_name.Length == 10))
                                            return fieldGeneratorDeviceYear;
                                        break;
                                    default:
                                        break;
                                  }
                              }
                            break;
                        case 'r':
                            if ((String.Compare(field_name, 2, "iverSide", 0, 8, false) == 0) && (field_name.Length == 10))
                                return fieldGeneratorDriverSide;
                            break;
                        default:
                            break;
                      }
                  }
                return null;
              }
            public Generator(bool ignore_extras)
              {
                fieldGeneratorDeviceModel = new JSONHoldingStringGenerator("field \"DeviceModel\" of the TypeDeviceInfo class");
                fieldGeneratorDeviceYear = new JSONHoldingValueGenerator("field \"DeviceYear\" of the TypeDeviceInfo class");
                fieldGeneratorDeviceName = new JSONHoldingStringGenerator("field \"DeviceName\" of the TypeDeviceInfo class");
                fieldGeneratorDriverSide = new FieldHoldingGeneratorDriverSide("field \"DriverSide\" of the TypeDeviceInfo class");
                unknownFieldGenerator = new UnknownFieldGenerator(ignore_extras);
                set_what("the TypeDeviceInfo class");
                allow_incomplete = false;
                allow_unpolished = false;
              }
            public Generator()
              {
                fieldGeneratorDeviceModel = new JSONHoldingStringGenerator("field \"DeviceModel\" of the TypeDeviceInfo class");
                fieldGeneratorDeviceYear = new JSONHoldingValueGenerator("field \"DeviceYear\" of the TypeDeviceInfo class");
                fieldGeneratorDeviceName = new JSONHoldingStringGenerator("field \"DeviceName\" of the TypeDeviceInfo class");
                fieldGeneratorDriverSide = new FieldHoldingGeneratorDriverSide("field \"DriverSide\" of the TypeDeviceInfo class");
                unknownFieldGenerator = new UnknownFieldGenerator(false);
                set_what("the TypeDeviceInfo class");
                allow_incomplete = false;
                allow_unpolished = false;
              }

            public override void reset()
              {
                fieldGeneratorDeviceModel.reset();
                fieldGeneratorDeviceYear.reset();
                fieldGeneratorDeviceName.reset();
                fieldGeneratorDriverSide.reset();
                base.reset();
                unknownFieldGenerator.reset();
              }
            public void set_allow_incomplete(bool new_allow_incomplete)
              {
                allow_incomplete = new_allow_incomplete;
              }
            public void set_allow_unpolished(bool new_allow_unpolished)
              {
                allow_unpolished = new_allow_unpolished;
              }
          };
        public class HoldingGenerator : Generator
          {
            protected override void handle_result(TypeDeviceInfoJSON  result)
              {
    //@@@            Debug.Assert(!have_value);
                have_value = true;
                value = result;
              }

            public HoldingGenerator(String what, bool ignore_extras) : base(ignore_extras)
              {
                have_value = false;
                base.set_what(what);
              }

            public HoldingGenerator(String what) : base(false)
              {
                have_value = false;
                base.set_what(what);
              }

            public override void reset()
              {
                have_value = false;
                base.reset();
              }

            public bool have_value;
            public TypeDeviceInfoJSON value;
          };
        public class HoldingArrayGenerator : JSONArrayGenerator
      {
        protected class ElementHandler : Generator
          {
            private HoldingArrayGenerator top;

            protected override void handle_result(TypeDeviceInfoJSON  result)
              {
                top.value.Add(result);
              }
            protected override string get_what()
              {
                return "element " + top.value.Count + " of " + top.get_what();
              }

            public ElementHandler(HoldingArrayGenerator init_top, bool ignore_extras) : base(ignore_extras)
              {
                top = init_top;
              }
          };

        private ElementHandler element_handler;

        protected override JSONHandler start()
          {
            have_value = true;
            value.Clear();
            return element_handler;
          }
        protected override void finish()
          {
            Debug.Assert(have_value);
            handle_result(value);
            element_handler.reset();
          }
        protected virtual void handle_result(List<TypeDeviceInfoJSON> result)

          {
          }

        public HoldingArrayGenerator(string what, bool ignore_extras)
          {
            element_handler = new ElementHandler(this, ignore_extras);
            have_value = false;
            value = new List<TypeDeviceInfoJSON>();
            base.set_what(what);
          }
        public HoldingArrayGenerator(bool ignore_extras)
          {
            element_handler = new ElementHandler(this, ignore_extras);
            value = new List<TypeDeviceInfoJSON>();
            have_value = false;
          }

        public void set_allow_incomplete(bool new_allow_incomplete)
          {
            element_handler.set_allow_incomplete(new_allow_incomplete);
          }

        public void set_allow_unpolished(bool new_allow_unpolished)
          {
            element_handler.set_allow_unpolished(new_allow_unpolished);
          }

        public override void reset()
          {
            element_handler.reset();
            have_value = false;
            value.Clear();
            base.reset();
          }

        public bool have_value;
        public List<TypeDeviceInfoJSON> value;
      };
      };
    public enum TypeUnitPreference
      {
        UnitPreference_US,
        UnitPreference_METRIC
      };

    public static TypeUnitPreference  stringToUnitPreference(string chars)
      {
        switch (chars[0])
          {
            case 'M':
                if ((String.Compare(chars, 1, "ETRIC", 0, 5, false) == 0) && (chars.Length == 6))
                    return TypeUnitPreference.UnitPreference_METRIC;
                break;
            case 'U':
                if ((String.Compare(chars, 1, "S", 0, 1, false) == 0) && (chars.Length == 2))
                    return TypeUnitPreference.UnitPreference_US;
                break;
            default:
                break;
          }
        throw new Exception("The value for field `UnitPreference' is not one of the legal values.");
      }

    public static string  stringFromUnitPreference(TypeUnitPreference the_enum)
      {
        switch (the_enum)
          {
            case TypeUnitPreference.UnitPreference_US:
                return "US";
            case TypeUnitPreference.UnitPreference_METRIC:
                return "METRIC";
            default:
                Debug.Assert(false);
                return null;
          }
      }

    public struct TypeClientVersion
      {
        public int key;
        public string choice0;
        public BigInteger choice1;
      };
    public enum TypeProfanityFilter
      {
        ProfanityFilter_AllowAll,
        ProfanityFilter_StarAllButFirst,
        ProfanityFilter_StarAll
      };

    public static TypeProfanityFilter  stringToProfanityFilter(string chars)
      {
        switch (chars[0])
          {
            case 'A':
                if ((String.Compare(chars, 1, "llowAll", 0, 7, false) == 0) && (chars.Length == 8))
                    return TypeProfanityFilter.ProfanityFilter_AllowAll;
                break;
            case 'S':
                if (String.Compare(chars, 1, "tarAll", 0, 6, false) == 0)
                  {
                    if (chars.Length == 7)
                      {
                        return TypeProfanityFilter.ProfanityFilter_StarAll;
                      }
                    switch (chars[7])
                      {
                        case 'B':
                            if ((String.Compare(chars, 8, "utFirst", 0, 7, false) == 0) && (chars.Length == 15))
                                return TypeProfanityFilter.ProfanityFilter_StarAllButFirst;
                            break;
                        default:
                            break;
                      }
                  }
                break;
            default:
                break;
          }
        throw new Exception("The value for field `ProfanityFilter' is not one of the legal values.");
      }

    public static string  stringFromProfanityFilter(TypeProfanityFilter the_enum)
      {
        switch (the_enum)
          {
            case TypeProfanityFilter.ProfanityFilter_AllowAll:
                return "AllowAll";
            case TypeProfanityFilter.ProfanityFilter_StarAllButFirst:
                return "StarAllButFirst";
            case TypeProfanityFilter.ProfanityFilter_StarAll:
                return "StarAll";
            default:
                Debug.Assert(false);
                return null;
          }
      }

    public enum TypeResponseAudioShortOrLong
      {
        ResponseAudioShortOrLong_Short,
        ResponseAudioShortOrLong_Long
      };

    public static TypeResponseAudioShortOrLong  stringToResponseAudioShortOrLong(string chars)
      {
        switch (chars[0])
          {
            case 'L':
                if ((String.Compare(chars, 1, "ong", 0, 3, false) == 0) && (chars.Length == 4))
                    return TypeResponseAudioShortOrLong.ResponseAudioShortOrLong_Long;
                break;
            case 'S':
                if ((String.Compare(chars, 1, "hort", 0, 4, false) == 0) && (chars.Length == 5))
                    return TypeResponseAudioShortOrLong.ResponseAudioShortOrLong_Short;
                break;
            default:
                break;
          }
        throw new Exception("The value for field `ResponseAudioShortOrLong' is not one of the legal values.");
      }

    public static string  stringFromResponseAudioShortOrLong(TypeResponseAudioShortOrLong the_enum)
      {
        switch (the_enum)
          {
            case TypeResponseAudioShortOrLong.ResponseAudioShortOrLong_Short:
                return "Short";
            case TypeResponseAudioShortOrLong.ResponseAudioShortOrLong_Long:
                return "Long";
            default:
                Debug.Assert(false);
                return null;
          }
      }

    public enum TypeResponseAudioAcceptedEncodingsKnownValues
      {
        ResponseAudioAcceptedEncodings_WAV,
        ResponseAudioAcceptedEncodings_Speex,
        ResponseAudioAcceptedEncodings__none
      };
    public struct TypeResponseAudioAcceptedEncodings
      {
        public bool in_known_list;
        public string string_value;
        public TypeResponseAudioAcceptedEncodingsKnownValues list_value;
      };

    public static TypeResponseAudioAcceptedEncodingsKnownValues  stringToResponseAudioAcceptedEncodings(string chars)
      {
        switch (chars[0])
          {
            case 'S':
                if ((String.Compare(chars, 1, "peex", 0, 4, false) == 0) && (chars.Length == 5))
                    return TypeResponseAudioAcceptedEncodingsKnownValues.ResponseAudioAcceptedEncodings_Speex;
                break;
            case 'W':
                if ((String.Compare(chars, 1, "AV", 0, 2, false) == 0) && (chars.Length == 3))
                    return TypeResponseAudioAcceptedEncodingsKnownValues.ResponseAudioAcceptedEncodings_WAV;
                break;
            default:
                break;
          }
        return TypeResponseAudioAcceptedEncodingsKnownValues.ResponseAudioAcceptedEncodings__none;
      }

    public static string  stringFromResponseAudioAcceptedEncodings(TypeResponseAudioAcceptedEncodingsKnownValues the_enum)
      {
        switch (the_enum)
          {
            case TypeResponseAudioAcceptedEncodingsKnownValues.ResponseAudioAcceptedEncodings_WAV:
                return "WAV";
            case TypeResponseAudioAcceptedEncodingsKnownValues.ResponseAudioAcceptedEncodings_Speex:
                return "Speex";
            default:
                Debug.Assert(false);
                return null;
          }
      }

    private bool flagHasLatitude;
    private double storeLatitude;
    private string textStoreLatitude;
    private bool flagHasLongitude;
    private double storeLongitude;
    private string textStoreLongitude;
    private bool flagHasPositionTime;
    private BigInteger storePositionTime;
    private bool flagHasPositionHorizontalAccuracy;
    private double storePositionHorizontalAccuracy;
    private string textStorePositionHorizontalAccuracy;
    private bool flagHasStreet;
    private string storeStreet;
    private bool flagHasCity;
    private string storeCity;
    private bool flagHasState;
    private string storeState;
    private bool flagHasCountry;
    private string storeCountry;
    private bool flagHasRoutePoints;
    private RoutePointsJSON  storeRoutePoints;
    private bool flagHasRouteInformation;
    private ClientRouteInformationJSON  storeRouteInformation;
    private bool flagHasControllableTrackPlaying;
    private bool storeControllableTrackPlaying;
    private bool flagHasTimeStamp;
    private BigInteger storeTimeStamp;
    private bool flagHasTimeZone;
    private string storeTimeZone;
    private bool flagHasConversationState;
    private ConversationStateJSON  storeConversationState;
    private bool flagHasClientState;
    private ClientStateJSON  storeClientState;
    private bool flagHasDeviceInfo;
    private TypeDeviceInfoJSON  storeDeviceInfo;
    private bool flagHasSendBack;
    private JSONValue  storeSendBack;
    private bool flagHasPreferredImageSize;
    private List< BigInteger > storePreferredImageSize;
    private bool flagHasInputLanguageEnglishName;
    private string storeInputLanguageEnglishName;
    private bool flagHasInputLanguageNativeName;
    private string storeInputLanguageNativeName;
    private bool flagHasInputLanguageIETFTag;
    private string storeInputLanguageIETFTag;
    private bool flagHasOutputLanguageEnglishName;
    private string storeOutputLanguageEnglishName;
    private bool flagHasOutputLanguageNativeName;
    private string storeOutputLanguageNativeName;
    private bool flagHasOutputLanguageIETFTag;
    private string storeOutputLanguageIETFTag;
    private bool flagHasResultVersionAccepted;
    private double storeResultVersionAccepted;
    private string textStoreResultVersionAccepted;
    private bool flagHasUnitPreference;
    private TypeUnitPreference storeUnitPreference;
    private bool flagHasDefaultTimeFormat24Hours;
    private bool storeDefaultTimeFormat24Hours;
    private bool flagHasClientID;
    private string storeClientID;
    private bool flagHasClientVersion;
    private TypeClientVersion storeClientVersion;
    private bool flagHasDeviceID;
    private string storeDeviceID;
    private bool flagHasSDK;
    private string storeSDK;
    private bool flagHasSDKInfo;
    private JSONObjectValue  storeSDKInfo;
    private bool flagHasFirstPersonSelf;
    private string storeFirstPersonSelf;
    private bool flagHasFirstPersonSelfSpoken;
    private string storeFirstPersonSelfSpoken;
    private bool flagHasSecondPersonSelf;
    private List< string > storeSecondPersonSelf;
    private bool flagHasSecondPersonSelfSpoken;
    private List< string > storeSecondPersonSelfSpoken;
    private bool flagHasWakeUpPhraseIncludedInAudio;
    private bool storeWakeUpPhraseIncludedInAudio;
    private bool flagHasInitialSecondsOfAudioToIgnore;
    private double storeInitialSecondsOfAudioToIgnore;
    private string textStoreInitialSecondsOfAudioToIgnore;
    private bool flagHasWakeUpPattern;
    private string storeWakeUpPattern;
    private bool flagHasUserID;
    private string storeUserID;
    private bool flagHasRequestID;
    private string storeRequestID;
    private bool flagHasSessionID;
    private string storeSessionID;
    private bool flagHasDomains;
    private DomainsJSON  storeDomains;
    private bool flagHasResultUpdateAllowed;
    private bool storeResultUpdateAllowed;
    private bool flagHasPartialTranscriptsDesired;
    private bool storePartialTranscriptsDesired;
    private bool flagHasMinResults;
    private BigInteger storeMinResults;
    private bool flagHasMaxResults;
    private BigInteger storeMaxResults;
    private bool flagHasObjectByteCountPrefix;
    private bool storeObjectByteCountPrefix;
    private bool flagHasProfanityFilter;
    private TypeProfanityFilter storeProfanityFilter;
    private bool flagHasClientMatches;
    private List< ClientMatchJSON  > storeClientMatches;
    private bool flagHasClientMatchesOnly;
    private bool storeClientMatchesOnly;
    private bool flagHasPagination;
    private PaginationJSON  storePagination;
    private bool flagHasResponseAudioVoice;
    private string storeResponseAudioVoice;
    private bool flagHasResponseAudioShortOrLong;
    private TypeResponseAudioShortOrLong storeResponseAudioShortOrLong;
    private bool flagHasResponseAudioAcceptedEncodings;
    private List< TypeResponseAudioAcceptedEncodings > storeResponseAudioAcceptedEncodings;
    private bool flagHasReturnResponseAudioAsURL;
    private bool storeReturnResponseAudioAsURL;
    private bool flagHasVoiceActivityDetection;
    private VoiceActivityDetectionJSON  storeVoiceActivityDetection;
    private bool flagHasServerDeterminesEndOfAudio;
    private bool storeServerDeterminesEndOfAudio;
    private bool flagHasIntentOnly;
    private bool storeIntentOnly;
    private bool flagHasDisableSpellCorrection;
    private bool storeDisableSpellCorrection;
    private bool flagHasUseContactData;
    private bool storeUseContactData;
    private bool flagHasUseClientTime;
    private bool storeUseClientTime;
    private bool flagHasForceConversationStateTime;
    private BigInteger storeForceConversationStateTime;
    private bool flagHasOutputLatticeSize;
    private BigInteger storeOutputLatticeSize;
    private bool flagHasMatchingMutations;
    private MatchingMutationsJSON  storeMatchingMutations;
    private bool flagHasUseFormattedTranscriptionAsDefault;
    private bool storeUseFormattedTranscriptionAsDefault;
    private bool flagHasResponseRanking;
    private ResponseRankingJSON  storeResponseRanking;
    private List<string> extraKeys;
    private List<JSONValue > extraValues;
    private Dictionary<string, JSONValue > extraIndex;


    private void  fromJSONLatitude(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONRationalValue json_rational = json_value.rational_value();
        string the_rational_text;
        if (json_rational != null)
          {
            the_rational_text = json_rational.getText();
          }
        else
          {
            JSONIntegerValue json_integer = json_value.integer_value();
            if (json_integer != null)
              {
                the_rational_text = json_integer.getText();
              }
            else
              {
                throw new Exception("The value for field Latitude of RequestInfoJSON is not a number.");
              }
          }
        setLatitudeText(the_rational_text);
      }


    private void  fromJSONLongitude(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONRationalValue json_rational = json_value.rational_value();
        string the_rational_text;
        if (json_rational != null)
          {
            the_rational_text = json_rational.getText();
          }
        else
          {
            JSONIntegerValue json_integer = json_value.integer_value();
            if (json_integer != null)
              {
                the_rational_text = json_integer.getText();
              }
            else
              {
                throw new Exception("The value for field Longitude of RequestInfoJSON is not a number.");
              }
          }
        setLongitudeText(the_rational_text);
      }


    private void  fromJSONPositionTime(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        BigInteger extracted_integer;
        JSONIntegerValue json_integer = json_value.integer_value();
        if (json_integer == null)
          {
            JSONRationalValue json_rational = json_value.rational_value();
            if (json_rational == null)
              {
                throw new Exception("The value for field PositionTime of RequestInfoJSON is not a number.");
              }
            if (!(json_rational.isInteger()))
              {
                throw new Exception("The value for field PositionTime of RequestInfoJSON is not an integer.");
              }
            extracted_integer = json_rational.getInteger()        ;
          }
        else
          {
            extracted_integer = json_integer.getData()        ;
          }
        setPositionTime(extracted_integer);
      }


    private void  fromJSONPositionHorizontalAccuracy(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONRationalValue json_rational = json_value.rational_value();
        string the_rational_text;
        if (json_rational != null)
          {
            the_rational_text = json_rational.getText();
          }
        else
          {
            JSONIntegerValue json_integer = json_value.integer_value();
            if (json_integer != null)
              {
                the_rational_text = json_integer.getText();
              }
            else
              {
                throw new Exception("The value for field PositionHorizontalAccuracy of RequestInfoJSON is not a number.");
              }
          }
        setPositionHorizontalAccuracyText(the_rational_text);
      }


    private void  fromJSONStreet(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field Street of RequestInfoJSON is not a string.");
        setStreet(json_string.getData());
      }


    private void  fromJSONCity(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field City of RequestInfoJSON is not a string.");
        setCity(json_string.getData());
      }


    private void  fromJSONState(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field State of RequestInfoJSON is not a string.");
        setState(json_string.getData());
      }


    private void  fromJSONCountry(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field Country of RequestInfoJSON is not a string.");
        setCountry(json_string.getData());
      }


    private void  fromJSONRoutePoints(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        RoutePointsJSON convert_classy = RoutePointsJSON.from_json(json_value, ignore_extras, true);
        setRoutePoints(convert_classy);
      }


    private void  fromJSONRouteInformation(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        ClientRouteInformationJSON convert_classy = ClientRouteInformationJSON.from_json(json_value, ignore_extras, true);
        setRouteInformation(convert_classy);
      }


    private void  fromJSONControllableTrackPlaying(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field ControllableTrackPlaying of RequestInfoJSON is not true for false.");
              }
          }
        setControllableTrackPlaying(the_bool);
      }


    private void  fromJSONTimeStamp(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        BigInteger extracted_integer;
        JSONIntegerValue json_integer = json_value.integer_value();
        if (json_integer == null)
          {
            JSONRationalValue json_rational = json_value.rational_value();
            if (json_rational == null)
              {
                throw new Exception("The value for field TimeStamp of RequestInfoJSON is not a number.");
              }
            if (!(json_rational.isInteger()))
              {
                throw new Exception("The value for field TimeStamp of RequestInfoJSON is not an integer.");
              }
            extracted_integer = json_rational.getInteger()        ;
          }
        else
          {
            extracted_integer = json_integer.getData()        ;
          }
        setTimeStamp(extracted_integer);
      }


    private void  fromJSONTimeZone(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field TimeZone of RequestInfoJSON is not a string.");
        setTimeZone(json_string.getData());
      }


    private void  fromJSONConversationState(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        ConversationStateJSON convert_classy = ConversationStateJSON.from_json(json_value, ignore_extras, true);
        setConversationState(convert_classy);
      }


    private void  fromJSONClientState(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        ClientStateJSON convert_classy = ClientStateJSON.from_json(json_value, ignore_extras, true);
        setClientState(convert_classy);
      }


    private void  fromJSONDeviceInfo(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        TypeDeviceInfoJSON convert_classy = TypeDeviceInfoJSON.from_json(json_value, ignore_extras, true);
        setDeviceInfo(convert_classy);
      }


    private void  fromJSONSendBack(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        setSendBack(json_value);
      }


    private void  fromJSONPreferredImageSize(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONArrayValue json_array1 = json_value.array_value();
        if (json_array1 == null)
            throw new Exception("The value for field PreferredImageSize of RequestInfoJSON is not an array.");
        int count1 = json_array1.componentCount();
        if (count1 < 2)
            throw new Exception("The value for field PreferredImageSize of RequestInfoJSON has too few elements.");
        List< BigInteger > vector_PreferredImageSize1 = new List< BigInteger >(count1);
        for (int num1 = 0; num1 < count1; ++num1)
          {
            BigInteger extracted_integer;
            JSONIntegerValue json_integer = json_array1.component(num1).integer_value();
            if (json_integer == null)
              {
                JSONRationalValue json_rational = json_array1.component(num1).rational_value();
                if (json_rational == null)
                  {
                    throw new Exception("The value for an element of field PreferredImageSize of RequestInfoJSON is not a number.");
                  }
                if (!(json_rational.isInteger()))
                  {
                    throw new Exception("The value for an element of field PreferredImageSize of RequestInfoJSON is not an integer.");
                  }
                extracted_integer = json_rational.getInteger()            ;
              }
            else
              {
                extracted_integer = json_integer.getData()            ;
              }
            vector_PreferredImageSize1.Add(extracted_integer);
          }
        Debug.Assert(vector_PreferredImageSize1.Count >= 2);
        initPreferredImageSize();
        for (int num1 = 0; num1 < vector_PreferredImageSize1.Count; ++num1)
            appendPreferredImageSize(vector_PreferredImageSize1[num1]);
        for (int num1 = 0; num1 < vector_PreferredImageSize1.Count; ++num1)
          {
          }
      }


    private void  fromJSONInputLanguageEnglishName(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field InputLanguageEnglishName of RequestInfoJSON is not a string.");
        setInputLanguageEnglishName(json_string.getData());
      }


    private void  fromJSONInputLanguageNativeName(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field InputLanguageNativeName of RequestInfoJSON is not a string.");
        setInputLanguageNativeName(json_string.getData());
      }


    private void  fromJSONInputLanguageIETFTag(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field InputLanguageIETFTag of RequestInfoJSON is not a string.");
        setInputLanguageIETFTag(json_string.getData());
      }


    private void  fromJSONOutputLanguageEnglishName(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field OutputLanguageEnglishName of RequestInfoJSON is not a string.");
        setOutputLanguageEnglishName(json_string.getData());
      }


    private void  fromJSONOutputLanguageNativeName(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field OutputLanguageNativeName of RequestInfoJSON is not a string.");
        setOutputLanguageNativeName(json_string.getData());
      }


    private void  fromJSONOutputLanguageIETFTag(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field OutputLanguageIETFTag of RequestInfoJSON is not a string.");
        setOutputLanguageIETFTag(json_string.getData());
      }


    private void  fromJSONResultVersionAccepted(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONRationalValue json_rational = json_value.rational_value();
        string the_rational_text;
        if (json_rational != null)
          {
            the_rational_text = json_rational.getText();
          }
        else
          {
            JSONIntegerValue json_integer = json_value.integer_value();
            if (json_integer != null)
              {
                the_rational_text = json_integer.getText();
              }
            else
              {
                throw new Exception("The value for field ResultVersionAccepted of RequestInfoJSON is not a number.");
              }
          }
        setResultVersionAcceptedText(the_rational_text);
      }


    private void  fromJSONUnitPreference(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field UnitPreference of RequestInfoJSON is not a string.");
        TypeUnitPreference the_enum;
        switch (json_string.getData()[0])
          {
            case 'M':
                if ((String.Compare(json_string.getData(), 1, "ETRIC", 0, 5, false) == 0) && (json_string.getData().Length == 6))
                      {
                        the_enum = TypeUnitPreference.UnitPreference_METRIC;
                        goto enum_is_done;
                      }
                break;
            case 'U':
                if ((String.Compare(json_string.getData(), 1, "S", 0, 1, false) == 0) && (json_string.getData().Length == 2))
                      {
                        the_enum = TypeUnitPreference.UnitPreference_US;
                        goto enum_is_done;
                      }
                break;
            default:
                break;
          }
        throw new Exception("The value for field UnitPreference of RequestInfoJSON is not one of the legal strings.");
      enum_is_done:;
        setUnitPreference(the_enum);
      }


    private void  fromJSONDefaultTimeFormat24Hours(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field DefaultTimeFormat24Hours of RequestInfoJSON is not true for false.");
              }
          }
        setDefaultTimeFormat24Hours(the_bool);
      }


    private void  fromJSONClientID(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field ClientID of RequestInfoJSON is not a string.");
        setClientID(json_string.getData());
      }


    private void  fromJSONClientVersion(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        TypeClientVersion or_result = new TypeClientVersion();
        bool or_done = false;
        if (!or_done)
          {
            try
              {
                JSONStringValue json_string = json_value.string_value();
                if (json_string == null)
                    throw new Exception("The value for ??? is not a string.");
                or_result.choice0 = json_string.getData();
                or_result.key = 0;
                or_done = true;
              }
            catch (Exception )
              {
              }
          }
        if (!or_done)
          {
            try
              {
                BigInteger extracted_integer;
                JSONIntegerValue json_integer = json_value.integer_value();
                if (json_integer == null)
                  {
                    JSONRationalValue json_rational = json_value.rational_value();
                    if (json_rational == null)
                      {
                        throw new Exception("The value for ??? is not a number.");
                      }
                    if (!(json_rational.isInteger()))
                      {
                        throw new Exception("The value for ??? is not an integer.");
                      }
                    extracted_integer = json_rational.getInteger()                ;
                  }
                else
                  {
                    extracted_integer = json_integer.getData()                ;
                  }
                if (extracted_integer < 0)
                    throw new Exception("The value for ??? is less than the lower bound (0) for that field.");
                or_result.choice1 = extracted_integer;
                or_result.key = 1;
                or_done = true;
              }
            catch (Exception )
              {
              }
          }
        if (!or_done)
            throw new Exception("The value for field ClientVersion of RequestInfoJSON is not one of the allowed values.");
        setClientVersion(or_result);
        switch (or_result.key)
          {
            case 0:
                break;
            case 1:
                break;
            default:
                Debug.Assert(false);
                break;
          }
      }


    private void  fromJSONDeviceID(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field DeviceID of RequestInfoJSON is not a string.");
        setDeviceID(json_string.getData());
      }


    private void  fromJSONSDK(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field SDK of RequestInfoJSON is not a string.");
        setSDK(json_string.getData());
      }


    private void  fromJSONSDKInfo(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        if (json_value.object_value() == null)
            throw new Exception("The value for field SDKInfo of RequestInfoJSON is not an object.");
        setSDKInfo(json_value.object_value());
      }


    private void  fromJSONFirstPersonSelf(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field FirstPersonSelf of RequestInfoJSON is not a string.");
        setFirstPersonSelf(json_string.getData());
      }


    private void  fromJSONFirstPersonSelfSpoken(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field FirstPersonSelfSpoken of RequestInfoJSON is not a string.");
        setFirstPersonSelfSpoken(json_string.getData());
      }


    private void  fromJSONSecondPersonSelf(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONArrayValue json_array1 = json_value.array_value();
        if (json_array1 == null)
            throw new Exception("The value for field SecondPersonSelf of RequestInfoJSON is not an array.");
        int count1 = json_array1.componentCount();
        List< string > vector_SecondPersonSelf1 = new List< string >(count1);
        for (int num1 = 0; num1 < count1; ++num1)
          {
            JSONStringValue json_string = json_array1.component(num1).string_value();
            if (json_string == null)
                throw new Exception("The value for an element of field SecondPersonSelf of RequestInfoJSON is not a string.");
            vector_SecondPersonSelf1.Add(json_string.getData());
          }
        initSecondPersonSelf();
        for (int num2 = 0; num2 < vector_SecondPersonSelf1.Count; ++num2)
            appendSecondPersonSelf(vector_SecondPersonSelf1[num2]);
        for (int num1 = 0; num1 < vector_SecondPersonSelf1.Count; ++num1)
          {
          }
      }


    private void  fromJSONSecondPersonSelfSpoken(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONArrayValue json_array1 = json_value.array_value();
        if (json_array1 == null)
            throw new Exception("The value for field SecondPersonSelfSpoken of RequestInfoJSON is not an array.");
        int count1 = json_array1.componentCount();
        List< string > vector_SecondPersonSelfSpoken1 = new List< string >(count1);
        for (int num1 = 0; num1 < count1; ++num1)
          {
            JSONStringValue json_string = json_array1.component(num1).string_value();
            if (json_string == null)
                throw new Exception("The value for an element of field SecondPersonSelfSpoken of RequestInfoJSON is not a string.");
            vector_SecondPersonSelfSpoken1.Add(json_string.getData());
          }
        initSecondPersonSelfSpoken();
        for (int num3 = 0; num3 < vector_SecondPersonSelfSpoken1.Count; ++num3)
            appendSecondPersonSelfSpoken(vector_SecondPersonSelfSpoken1[num3]);
        for (int num1 = 0; num1 < vector_SecondPersonSelfSpoken1.Count; ++num1)
          {
          }
      }


    private void  fromJSONWakeUpPhraseIncludedInAudio(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field WakeUpPhraseIncludedInAudio of RequestInfoJSON is not true for false.");
              }
          }
        setWakeUpPhraseIncludedInAudio(the_bool);
      }


    private void  fromJSONInitialSecondsOfAudioToIgnore(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONRationalValue json_rational = json_value.rational_value();
        string the_rational_text;
        if (json_rational != null)
          {
            the_rational_text = json_rational.getText();
          }
        else
          {
            JSONIntegerValue json_integer = json_value.integer_value();
            if (json_integer != null)
              {
                the_rational_text = json_integer.getText();
              }
            else
              {
                throw new Exception("The value for field InitialSecondsOfAudioToIgnore of RequestInfoJSON is not a number.");
              }
          }
        setInitialSecondsOfAudioToIgnoreText(the_rational_text);
      }


    private void  fromJSONWakeUpPattern(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field WakeUpPattern of RequestInfoJSON is not a string.");
        setWakeUpPattern(json_string.getData());
      }


    private void  fromJSONUserID(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field UserID of RequestInfoJSON is not a string.");
        setUserID(json_string.getData());
      }


    private void  fromJSONRequestID(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field RequestID of RequestInfoJSON is not a string.");
        setRequestID(json_string.getData());
      }


    private void  fromJSONSessionID(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field SessionID of RequestInfoJSON is not a string.");
        setSessionID(json_string.getData());
      }


    private void  fromJSONDomains(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        DomainsJSON convert_classy = DomainsJSON.from_json(json_value, ignore_extras, true);
        setDomains(convert_classy);
      }


    private void  fromJSONResultUpdateAllowed(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field ResultUpdateAllowed of RequestInfoJSON is not true for false.");
              }
          }
        setResultUpdateAllowed(the_bool);
      }


    private void  fromJSONPartialTranscriptsDesired(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field PartialTranscriptsDesired of RequestInfoJSON is not true for false.");
              }
          }
        setPartialTranscriptsDesired(the_bool);
      }


    private void  fromJSONMinResults(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        BigInteger extracted_integer;
        JSONIntegerValue json_integer = json_value.integer_value();
        if (json_integer == null)
          {
            JSONRationalValue json_rational = json_value.rational_value();
            if (json_rational == null)
              {
                throw new Exception("The value for field MinResults of RequestInfoJSON is not a number.");
              }
            if (!(json_rational.isInteger()))
              {
                throw new Exception("The value for field MinResults of RequestInfoJSON is not an integer.");
              }
            extracted_integer = json_rational.getInteger()        ;
          }
        else
          {
            extracted_integer = json_integer.getData()        ;
          }
        setMinResults(extracted_integer);
      }


    private void  fromJSONMaxResults(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        BigInteger extracted_integer;
        JSONIntegerValue json_integer = json_value.integer_value();
        if (json_integer == null)
          {
            JSONRationalValue json_rational = json_value.rational_value();
            if (json_rational == null)
              {
                throw new Exception("The value for field MaxResults of RequestInfoJSON is not a number.");
              }
            if (!(json_rational.isInteger()))
              {
                throw new Exception("The value for field MaxResults of RequestInfoJSON is not an integer.");
              }
            extracted_integer = json_rational.getInteger()        ;
          }
        else
          {
            extracted_integer = json_integer.getData()        ;
          }
        setMaxResults(extracted_integer);
      }


    private void  fromJSONObjectByteCountPrefix(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field ObjectByteCountPrefix of RequestInfoJSON is not true for false.");
              }
          }
        setObjectByteCountPrefix(the_bool);
      }


    private void  fromJSONProfanityFilter(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field ProfanityFilter of RequestInfoJSON is not a string.");
        TypeProfanityFilter the_enum;
        switch (json_string.getData()[0])
          {
            case 'A':
                if ((String.Compare(json_string.getData(), 1, "llowAll", 0, 7, false) == 0) && (json_string.getData().Length == 8))
                      {
                        the_enum = TypeProfanityFilter.ProfanityFilter_AllowAll;
                        goto enum_is_done;
                      }
                break;
            case 'S':
                if (String.Compare(json_string.getData(), 1, "tarAll", 0, 6, false) == 0)
                  {
                    if (json_string.getData().Length == 7)
                      {
                          {
                            the_enum = TypeProfanityFilter.ProfanityFilter_StarAll;
                            goto enum_is_done;
                          }
                      }
                    switch (json_string.getData()[7])
                      {
                        case 'B':
                            if ((String.Compare(json_string.getData(), 8, "utFirst", 0, 7, false) == 0) && (json_string.getData().Length == 15))
                                  {
                                    the_enum = TypeProfanityFilter.ProfanityFilter_StarAllButFirst;
                                    goto enum_is_done;
                                  }
                            break;
                        default:
                            break;
                      }
                  }
                break;
            default:
                break;
          }
        throw new Exception("The value for field ProfanityFilter of RequestInfoJSON is not one of the legal strings.");
      enum_is_done:;
        setProfanityFilter(the_enum);
      }


    private void  fromJSONClientMatches(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONArrayValue json_array1 = json_value.array_value();
        if (json_array1 == null)
            throw new Exception("The value for field ClientMatches of RequestInfoJSON is not an array.");
        int count1 = json_array1.componentCount();
        if (count1 < 1)
            throw new Exception("The value for field ClientMatches of RequestInfoJSON has too few elements.");
        List< ClientMatchJSON  > vector_ClientMatches1 = new List< ClientMatchJSON  >(count1);
        for (int num1 = 0; num1 < count1; ++num1)
          {
            ClientMatchJSON convert_classy = ClientMatchJSON.from_json(json_array1.component(num1), ignore_extras, true);
            vector_ClientMatches1.Add(convert_classy);
          }
        Debug.Assert(vector_ClientMatches1.Count >= 1);
        initClientMatches();
        for (int num4 = 0; num4 < vector_ClientMatches1.Count; ++num4)
            appendClientMatches(vector_ClientMatches1[num4]);
        for (int num1 = 0; num1 < vector_ClientMatches1.Count; ++num1)
          {
          }
      }


    private void  fromJSONClientMatchesOnly(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field ClientMatchesOnly of RequestInfoJSON is not true for false.");
              }
          }
        setClientMatchesOnly(the_bool);
      }


    private void  fromJSONPagination(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        PaginationJSON convert_classy = PaginationJSON.from_json(json_value, ignore_extras, true);
        setPagination(convert_classy);
      }


    private void  fromJSONResponseAudioVoice(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field ResponseAudioVoice of RequestInfoJSON is not a string.");
        setResponseAudioVoice(json_string.getData());
      }


    private void  fromJSONResponseAudioShortOrLong(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONStringValue json_string = json_value.string_value();
        if (json_string == null)
            throw new Exception("The value for field ResponseAudioShortOrLong of RequestInfoJSON is not a string.");
        TypeResponseAudioShortOrLong the_enum;
        switch (json_string.getData()[0])
          {
            case 'L':
                if ((String.Compare(json_string.getData(), 1, "ong", 0, 3, false) == 0) && (json_string.getData().Length == 4))
                      {
                        the_enum = TypeResponseAudioShortOrLong.ResponseAudioShortOrLong_Long;
                        goto enum_is_done;
                      }
                break;
            case 'S':
                if ((String.Compare(json_string.getData(), 1, "hort", 0, 4, false) == 0) && (json_string.getData().Length == 5))
                      {
                        the_enum = TypeResponseAudioShortOrLong.ResponseAudioShortOrLong_Short;
                        goto enum_is_done;
                      }
                break;
            default:
                break;
          }
        throw new Exception("The value for field ResponseAudioShortOrLong of RequestInfoJSON is not one of the legal strings.");
      enum_is_done:;
        setResponseAudioShortOrLong(the_enum);
      }


    private void  fromJSONResponseAudioAcceptedEncodings(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONArrayValue json_array1 = json_value.array_value();
        if (json_array1 == null)
            throw new Exception("The value for field ResponseAudioAcceptedEncodings of RequestInfoJSON is not an array.");
        int count1 = json_array1.componentCount();
        if (count1 < 1)
            throw new Exception("The value for field ResponseAudioAcceptedEncodings of RequestInfoJSON has too few elements.");
        List< TypeResponseAudioAcceptedEncodings > vector_ResponseAudioAcceptedEncodings1 = new List< TypeResponseAudioAcceptedEncodings >(count1);
        for (int num1 = 0; num1 < count1; ++num1)
          {
            JSONStringValue json_string = json_array1.component(num1).string_value();
            if (json_string == null)
                throw new Exception("The value for an element of field ResponseAudioAcceptedEncodings of RequestInfoJSON is not a string.");
            TypeResponseAudioAcceptedEncodings the_open_enum = new TypeResponseAudioAcceptedEncodings();
            switch (json_string.getData()[0])
              {
                case 'S':
                    if ((String.Compare(json_string.getData(), 1, "peex", 0, 4, false) == 0) && (json_string.getData().Length == 5))
                          {
                            the_open_enum.in_known_list = true;
                            the_open_enum.list_value = TypeResponseAudioAcceptedEncodingsKnownValues.ResponseAudioAcceptedEncodings_Speex;
                            goto open_enum_is_done;
                          }
                    break;
                case 'W':
                    if ((String.Compare(json_string.getData(), 1, "AV", 0, 2, false) == 0) && (json_string.getData().Length == 3))
                          {
                            the_open_enum.in_known_list = true;
                            the_open_enum.list_value = TypeResponseAudioAcceptedEncodingsKnownValues.ResponseAudioAcceptedEncodings_WAV;
                            goto open_enum_is_done;
                          }
                    break;
                default:
                    break;
              }
            the_open_enum.in_known_list = false;
            the_open_enum.string_value = json_string.getData();
          open_enum_is_done:;
            vector_ResponseAudioAcceptedEncodings1.Add(the_open_enum);
          }
        Debug.Assert(vector_ResponseAudioAcceptedEncodings1.Count >= 1);
        initResponseAudioAcceptedEncodings();
        for (int num5 = 0; num5 < vector_ResponseAudioAcceptedEncodings1.Count; ++num5)
            appendResponseAudioAcceptedEncodings(vector_ResponseAudioAcceptedEncodings1[num5]);
        for (int num1 = 0; num1 < vector_ResponseAudioAcceptedEncodings1.Count; ++num1)
          {
          }
      }


    private void  fromJSONReturnResponseAudioAsURL(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field ReturnResponseAudioAsURL of RequestInfoJSON is not true for false.");
              }
          }
        setReturnResponseAudioAsURL(the_bool);
      }


    private void  fromJSONVoiceActivityDetection(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        VoiceActivityDetectionJSON convert_classy = VoiceActivityDetectionJSON.from_json(json_value, ignore_extras, true);
        setVoiceActivityDetection(convert_classy);
      }


    private void  fromJSONServerDeterminesEndOfAudio(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field ServerDeterminesEndOfAudio of RequestInfoJSON is not true for false.");
              }
          }
        setServerDeterminesEndOfAudio(the_bool);
      }


    private void  fromJSONIntentOnly(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field IntentOnly of RequestInfoJSON is not true for false.");
              }
          }
        setIntentOnly(the_bool);
      }


    private void  fromJSONDisableSpellCorrection(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field DisableSpellCorrection of RequestInfoJSON is not true for false.");
              }
          }
        setDisableSpellCorrection(the_bool);
      }


    private void  fromJSONUseContactData(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field UseContactData of RequestInfoJSON is not true for false.");
              }
          }
        setUseContactData(the_bool);
      }


    private void  fromJSONUseClientTime(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field UseClientTime of RequestInfoJSON is not true for false.");
              }
          }
        setUseClientTime(the_bool);
      }


    private void  fromJSONForceConversationStateTime(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        BigInteger extracted_integer;
        JSONIntegerValue json_integer = json_value.integer_value();
        if (json_integer == null)
          {
            JSONRationalValue json_rational = json_value.rational_value();
            if (json_rational == null)
              {
                throw new Exception("The value for field ForceConversationStateTime of RequestInfoJSON is not a number.");
              }
            if (!(json_rational.isInteger()))
              {
                throw new Exception("The value for field ForceConversationStateTime of RequestInfoJSON is not an integer.");
              }
            extracted_integer = json_rational.getInteger()        ;
          }
        else
          {
            extracted_integer = json_integer.getData()        ;
          }
        setForceConversationStateTime(extracted_integer);
      }


    private void  fromJSONOutputLatticeSize(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        BigInteger extracted_integer;
        JSONIntegerValue json_integer = json_value.integer_value();
        if (json_integer == null)
          {
            JSONRationalValue json_rational = json_value.rational_value();
            if (json_rational == null)
              {
                throw new Exception("The value for field OutputLatticeSize of RequestInfoJSON is not a number.");
              }
            if (!(json_rational.isInteger()))
              {
                throw new Exception("The value for field OutputLatticeSize of RequestInfoJSON is not an integer.");
              }
            extracted_integer = json_rational.getInteger()        ;
          }
        else
          {
            extracted_integer = json_integer.getData()        ;
          }
        setOutputLatticeSize(extracted_integer);
      }


    private void  fromJSONMatchingMutations(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        MatchingMutationsJSON convert_classy = MatchingMutationsJSON.from_json(json_value, ignore_extras, true);
        setMatchingMutations(convert_classy);
      }


    private void  fromJSONUseFormattedTranscriptionAsDefault(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        JSONTrueValue json_true = json_value.true_value();
        bool the_bool;
        if (json_true != null)
          {
            the_bool = true;
          }
        else
          {
            JSONFalseValue json_false = json_value.false_value();
            if (json_false != null)
              {
                the_bool = false;
              }
            else
              {
                throw new Exception("The value for field UseFormattedTranscriptionAsDefault of RequestInfoJSON is not true for false.");
              }
          }
        setUseFormattedTranscriptionAsDefault(the_bool);
      }


    private void  fromJSONResponseRanking(JSONValue json_value, bool ignore_extras)
      {
        Debug.Assert(json_value != null);
        ResponseRankingJSON convert_classy = ResponseRankingJSON.from_json(json_value, ignore_extras, true);
        setResponseRanking(convert_classy);
      }


    public RequestInfoJSON()
      {
        flagHasLatitude = false;
        flagHasLongitude = false;
        flagHasPositionTime = false;
        flagHasPositionHorizontalAccuracy = false;
        flagHasStreet = false;
        flagHasCity = false;
        flagHasState = false;
        flagHasCountry = false;
        flagHasRoutePoints = false;
        flagHasRouteInformation = false;
        flagHasControllableTrackPlaying = false;
        flagHasTimeStamp = false;
        flagHasTimeZone = false;
        flagHasConversationState = false;
        flagHasClientState = false;
        flagHasDeviceInfo = false;
        flagHasSendBack = false;
        flagHasPreferredImageSize = false;
        flagHasInputLanguageEnglishName = false;
        flagHasInputLanguageNativeName = false;
        flagHasInputLanguageIETFTag = false;
        flagHasOutputLanguageEnglishName = false;
        flagHasOutputLanguageNativeName = false;
        flagHasOutputLanguageIETFTag = false;
        flagHasResultVersionAccepted = false;
        flagHasUnitPreference = false;
        flagHasDefaultTimeFormat24Hours = false;
        flagHasClientID = false;
        flagHasClientVersion = false;
        flagHasDeviceID = false;
        flagHasSDK = false;
        flagHasSDKInfo = false;
        flagHasFirstPersonSelf = false;
        flagHasFirstPersonSelfSpoken = false;
        flagHasSecondPersonSelf = false;
        flagHasSecondPersonSelfSpoken = false;
        flagHasWakeUpPhraseIncludedInAudio = false;
        flagHasInitialSecondsOfAudioToIgnore = false;
        flagHasWakeUpPattern = false;
        flagHasUserID = false;
        flagHasRequestID = false;
        flagHasSessionID = false;
        flagHasDomains = false;
        flagHasResultUpdateAllowed = false;
        flagHasPartialTranscriptsDesired = false;
        flagHasMinResults = false;
        flagHasMaxResults = false;
        flagHasObjectByteCountPrefix = false;
        flagHasProfanityFilter = false;
        flagHasClientMatches = false;
        flagHasClientMatchesOnly = false;
        flagHasPagination = false;
        flagHasResponseAudioVoice = false;
        flagHasResponseAudioShortOrLong = false;
        flagHasResponseAudioAcceptedEncodings = false;
        flagHasReturnResponseAudioAsURL = false;
        flagHasVoiceActivityDetection = false;
        flagHasServerDeterminesEndOfAudio = false;
        flagHasIntentOnly = false;
        flagHasDisableSpellCorrection = false;
        flagHasUseContactData = false;
        flagHasUseClientTime = false;
        flagHasForceConversationStateTime = false;
        flagHasOutputLatticeSize = false;
        flagHasMatchingMutations = false;
        flagHasUseFormattedTranscriptionAsDefault = false;
        flagHasResponseRanking = false;
        storeControllableTrackPlaying = false;
        storePreferredImageSize = new List< BigInteger >();
        storeFirstPersonSelf = "Hound";
        storeSecondPersonSelf = new List< string >();
        string element1;
        element1 = "Hound";
        storeSecondPersonSelf.Add(element1);
        storeSecondPersonSelfSpoken = new List< string >();
        storeWakeUpPhraseIncludedInAudio = false;
        storeInitialSecondsOfAudioToIgnore = 0;
        storeWakeUpPattern = "[[\"OK\"] . \"Hound\"]";
        storeResultUpdateAllowed = false;
        storePartialTranscriptsDesired = false;
        storeMinResults = 1;
        storeMaxResults = 1;
        storeObjectByteCountPrefix = false;
        storeClientMatches = new List< ClientMatchJSON  >();
        storeClientMatchesOnly = false;
        storeResponseAudioAcceptedEncodings = new List< TypeResponseAudioAcceptedEncodings >();
        storeUseContactData = true;
        storeUseClientTime = false;
        storeOutputLatticeSize = 0;
        storeUseFormattedTranscriptionAsDefault = false;
        extraKeys = new List<string>();
    extraValues = new List<JSONValue >();
    extraIndex = new Dictionary<string, JSONValue >();
      }

    public bool  hasLatitude()
      {
        return flagHasLatitude;
      }

    public double  getLatitude()
      {
        Debug.Assert(flagHasLatitude);
        if (textStoreLatitude != "")
          {
            return Double.Parse(textStoreLatitude);
          }
        return storeLatitude;
      }

    public string  getLatitudeAsText()
      {
        Debug.Assert(flagHasLatitude);
        if (textStoreLatitude == "")
          {
            return Convert.ToString(storeLatitude);
          }
        else
          {
            return (textStoreLatitude);
          }
      }

    public bool  hasLongitude()
      {
        return flagHasLongitude;
      }

    public double  getLongitude()
      {
        Debug.Assert(flagHasLongitude);
        if (textStoreLongitude != "")
          {
            return Double.Parse(textStoreLongitude);
          }
        return storeLongitude;
      }

    public string  getLongitudeAsText()
      {
        Debug.Assert(flagHasLongitude);
        if (textStoreLongitude == "")
          {
            return Convert.ToString(storeLongitude);
          }
        else
          {
            return (textStoreLongitude);
          }
      }

    public bool  hasPositionTime()
      {
        return flagHasPositionTime;
      }

    public BigInteger  getPositionTime()
      {
        Debug.Assert(flagHasPositionTime);
        return storePositionTime;
      }

    public bool  hasPositionHorizontalAccuracy()
      {
        return flagHasPositionHorizontalAccuracy;
      }

    public double  getPositionHorizontalAccuracy()
      {
        Debug.Assert(flagHasPositionHorizontalAccuracy);
        if (textStorePositionHorizontalAccuracy != "")
          {
            return Double.Parse(textStorePositionHorizontalAccuracy);
          }
        return storePositionHorizontalAccuracy;
      }

    public string  getPositionHorizontalAccuracyAsText()
      {
        Debug.Assert(flagHasPositionHorizontalAccuracy);
        if (textStorePositionHorizontalAccuracy == "")
          {
            return Convert.ToString(storePositionHorizontalAccuracy);
          }
        else
          {
            return (textStorePositionHorizontalAccuracy);
          }
      }

    public bool  hasStreet()
      {
        return flagHasStreet;
      }

    public string  getStreet()
      {
        Debug.Assert(flagHasStreet);
        return storeStreet;
      }

    public bool  hasCity()
      {
        return flagHasCity;
      }

    public string  getCity()
      {
        Debug.Assert(flagHasCity);
        return storeCity;
      }

    public bool  hasState()
      {
        return flagHasState;
      }

    public string  getState()
      {
        Debug.Assert(flagHasState);
        return storeState;
      }

    public bool  hasCountry()
      {
        return flagHasCountry;
      }

    public string  getCountry()
      {
        Debug.Assert(flagHasCountry);
        return storeCountry;
      }

    public bool  hasRoutePoints()
      {
        return flagHasRoutePoints;
      }

    public RoutePointsJSON   getRoutePoints()
      {
        Debug.Assert(flagHasRoutePoints);
        return storeRoutePoints;
      }

    public bool  hasRouteInformation()
      {
        return flagHasRouteInformation;
      }

    public ClientRouteInformationJSON   getRouteInformation()
      {
        Debug.Assert(flagHasRouteInformation);
        return storeRouteInformation;
      }

    public bool  hasControllableTrackPlaying()
      {
        return flagHasControllableTrackPlaying;
      }

    public bool  getControllableTrackPlaying()
      {
        return storeControllableTrackPlaying;
      }

    public bool  hasTimeStamp()
      {
        return flagHasTimeStamp;
      }

    public BigInteger  getTimeStamp()
      {
        Debug.Assert(flagHasTimeStamp);
        return storeTimeStamp;
      }

    public bool  hasTimeZone()
      {
        return flagHasTimeZone;
      }

    public string  getTimeZone()
      {
        Debug.Assert(flagHasTimeZone);
        return storeTimeZone;
      }

    public bool  hasConversationState()
      {
        return flagHasConversationState;
      }

    public ConversationStateJSON   getConversationState()
      {
        Debug.Assert(flagHasConversationState);
        return storeConversationState;
      }

    public bool  hasClientState()
      {
        return flagHasClientState;
      }

    public ClientStateJSON   getClientState()
      {
        Debug.Assert(flagHasClientState);
        return storeClientState;
      }

    public bool  hasDeviceInfo()
      {
        return flagHasDeviceInfo;
      }

    public TypeDeviceInfoJSON   getDeviceInfo()
      {
        Debug.Assert(flagHasDeviceInfo);
        return storeDeviceInfo;
      }

    public bool  hasSendBack()
      {
        return flagHasSendBack;
      }

    public JSONValue   getSendBack()
      {
        Debug.Assert(flagHasSendBack);
        return storeSendBack;
      }

    public bool  hasPreferredImageSize()
      {
        return flagHasPreferredImageSize;
      }

    public int  countOfPreferredImageSize()
      {
        Debug.Assert(flagHasPreferredImageSize);
        return storePreferredImageSize.Count;
      }

    public BigInteger  elementOfPreferredImageSize(int element_num)
      {
        Debug.Assert(flagHasPreferredImageSize);
        return storePreferredImageSize[element_num];
      }

    public List< BigInteger >  getPreferredImageSize()
      {
        Debug.Assert(flagHasPreferredImageSize);
        return storePreferredImageSize;
      }

    public bool  hasInputLanguageEnglishName()
      {
        return flagHasInputLanguageEnglishName;
      }

    public string  getInputLanguageEnglishName()
      {
        Debug.Assert(flagHasInputLanguageEnglishName);
        return storeInputLanguageEnglishName;
      }

    public bool  hasInputLanguageNativeName()
      {
        return flagHasInputLanguageNativeName;
      }

    public string  getInputLanguageNativeName()
      {
        Debug.Assert(flagHasInputLanguageNativeName);
        return storeInputLanguageNativeName;
      }

    public bool  hasInputLanguageIETFTag()
      {
        return flagHasInputLanguageIETFTag;
      }

    public string  getInputLanguageIETFTag()
      {
        Debug.Assert(flagHasInputLanguageIETFTag);
        return storeInputLanguageIETFTag;
      }

    public bool  hasOutputLanguageEnglishName()
      {
        return flagHasOutputLanguageEnglishName;
      }

    public string  getOutputLanguageEnglishName()
      {
        Debug.Assert(flagHasOutputLanguageEnglishName);
        return storeOutputLanguageEnglishName;
      }

    public bool  hasOutputLanguageNativeName()
      {
        return flagHasOutputLanguageNativeName;
      }

    public string  getOutputLanguageNativeName()
      {
        Debug.Assert(flagHasOutputLanguageNativeName);
        return storeOutputLanguageNativeName;
      }

    public bool  hasOutputLanguageIETFTag()
      {
        return flagHasOutputLanguageIETFTag;
      }

    public string  getOutputLanguageIETFTag()
      {
        Debug.Assert(flagHasOutputLanguageIETFTag);
        return storeOutputLanguageIETFTag;
      }

    public bool  hasResultVersionAccepted()
      {
        return flagHasResultVersionAccepted;
      }

    public double  getResultVersionAccepted()
      {
        Debug.Assert(flagHasResultVersionAccepted);
        if (textStoreResultVersionAccepted != "")
          {
            return Double.Parse(textStoreResultVersionAccepted);
          }
        return storeResultVersionAccepted;
      }

    public string  getResultVersionAcceptedAsText()
      {
        Debug.Assert(flagHasResultVersionAccepted);
        if (textStoreResultVersionAccepted == "")
          {
            return Convert.ToString(storeResultVersionAccepted);
          }
        else
          {
            return (textStoreResultVersionAccepted);
          }
      }

    public bool  hasUnitPreference()
      {
        return flagHasUnitPreference;
      }

    public TypeUnitPreference  getUnitPreference()
      {
        Debug.Assert(flagHasUnitPreference);
        return storeUnitPreference;
      }

    public string  getUnitPreferenceAsString()
      {
        return stringFromUnitPreference(getUnitPreference());
      }

    public bool  hasDefaultTimeFormat24Hours()
      {
        return flagHasDefaultTimeFormat24Hours;
      }

    public bool  getDefaultTimeFormat24Hours()
      {
        Debug.Assert(flagHasDefaultTimeFormat24Hours);
        return storeDefaultTimeFormat24Hours;
      }

    public bool  hasClientID()
      {
        return flagHasClientID;
      }

    public string  getClientID()
      {
        Debug.Assert(flagHasClientID);
        return storeClientID;
      }

    public bool  hasClientVersion()
      {
        return flagHasClientVersion;
      }

    public TypeClientVersion  getClientVersion()
      {
        Debug.Assert(flagHasClientVersion);
        return storeClientVersion;
      }

    public bool  hasDeviceID()
      {
        return flagHasDeviceID;
      }

    public string  getDeviceID()
      {
        Debug.Assert(flagHasDeviceID);
        return storeDeviceID;
      }

    public bool  hasSDK()
      {
        return flagHasSDK;
      }

    public string  getSDK()
      {
        Debug.Assert(flagHasSDK);
        return storeSDK;
      }

    public bool  hasSDKInfo()
      {
        return flagHasSDKInfo;
      }

    public JSONObjectValue   getSDKInfo()
      {
        Debug.Assert(flagHasSDKInfo);
        return storeSDKInfo;
      }

    public bool  hasFirstPersonSelf()
      {
        return flagHasFirstPersonSelf;
      }

    public string  getFirstPersonSelf()
      {
        return storeFirstPersonSelf;
      }

    public bool  hasFirstPersonSelfSpoken()
      {
        return flagHasFirstPersonSelfSpoken;
      }

    public string  getFirstPersonSelfSpoken()
      {
        Debug.Assert(flagHasFirstPersonSelfSpoken);
        return storeFirstPersonSelfSpoken;
      }

    public bool  hasSecondPersonSelf()
      {
        return flagHasSecondPersonSelf;
      }

    public int  countOfSecondPersonSelf()
      {
        return storeSecondPersonSelf.Count;
      }

    public string  elementOfSecondPersonSelf(int element_num)
      {
        return storeSecondPersonSelf[element_num];
      }

    public List< string >  getSecondPersonSelf()
      {
        return storeSecondPersonSelf;
      }

    public bool  hasSecondPersonSelfSpoken()
      {
        return flagHasSecondPersonSelfSpoken;
      }

    public int  countOfSecondPersonSelfSpoken()
      {
        Debug.Assert(flagHasSecondPersonSelfSpoken);
        return storeSecondPersonSelfSpoken.Count;
      }

    public string  elementOfSecondPersonSelfSpoken(int element_num)
      {
        Debug.Assert(flagHasSecondPersonSelfSpoken);
        return storeSecondPersonSelfSpoken[element_num];
      }

    public List< string >  getSecondPersonSelfSpoken()
      {
        Debug.Assert(flagHasSecondPersonSelfSpoken);
        return storeSecondPersonSelfSpoken;
      }

    public bool  hasWakeUpPhraseIncludedInAudio()
      {
        return flagHasWakeUpPhraseIncludedInAudio;
      }

    public bool  getWakeUpPhraseIncludedInAudio()
      {
        return storeWakeUpPhraseIncludedInAudio;
      }

    public bool  hasInitialSecondsOfAudioToIgnore()
      {
        return flagHasInitialSecondsOfAudioToIgnore;
      }

    public double  getInitialSecondsOfAudioToIgnore()
      {
        if (textStoreInitialSecondsOfAudioToIgnore != "")
          {
            return Double.Parse(textStoreInitialSecondsOfAudioToIgnore);
          }
        return storeInitialSecondsOfAudioToIgnore;
      }

    public string  getInitialSecondsOfAudioToIgnoreAsText()
      {
        if (textStoreInitialSecondsOfAudioToIgnore == "")
          {
            return Convert.ToString(storeInitialSecondsOfAudioToIgnore);
          }
        else
          {
            return (textStoreInitialSecondsOfAudioToIgnore);
          }
      }

    public bool  hasWakeUpPattern()
      {
        return flagHasWakeUpPattern;
      }

    public string  getWakeUpPattern()
      {
        return storeWakeUpPattern;
      }

    public bool  hasUserID()
      {
        return flagHasUserID;
      }

    public string  getUserID()
      {
        Debug.Assert(flagHasUserID);
        return storeUserID;
      }

    public bool  hasRequestID()
      {
        return flagHasRequestID;
      }

    public string  getRequestID()
      {
        Debug.Assert(flagHasRequestID);
        return storeRequestID;
      }

    public bool  hasSessionID()
      {
        return flagHasSessionID;
      }

    public string  getSessionID()
      {
        Debug.Assert(flagHasSessionID);
        return storeSessionID;
      }

    public bool  hasDomains()
      {
        return flagHasDomains;
      }

    public DomainsJSON   getDomains()
      {
        Debug.Assert(flagHasDomains);
        return storeDomains;
      }

    public bool  hasResultUpdateAllowed()
      {
        return flagHasResultUpdateAllowed;
      }

    public bool  getResultUpdateAllowed()
      {
        return storeResultUpdateAllowed;
      }

    public bool  hasPartialTranscriptsDesired()
      {
        return flagHasPartialTranscriptsDesired;
      }

    public bool  getPartialTranscriptsDesired()
      {
        return storePartialTranscriptsDesired;
      }

    public bool  hasMinResults()
      {
        return flagHasMinResults;
      }

    public BigInteger  getMinResults()
      {
        return storeMinResults;
      }

    public bool  hasMaxResults()
      {
        return flagHasMaxResults;
      }

    public BigInteger  getMaxResults()
      {
        return storeMaxResults;
      }

    public bool  hasObjectByteCountPrefix()
      {
        return flagHasObjectByteCountPrefix;
      }

    public bool  getObjectByteCountPrefix()
      {
        return storeObjectByteCountPrefix;
      }

    public bool  hasProfanityFilter()
      {
        return flagHasProfanityFilter;
      }

    public TypeProfanityFilter  getProfanityFilter()
      {
        Debug.Assert(flagHasProfanityFilter);
        return storeProfanityFilter;
      }

    public string  getProfanityFilterAsString()
      {
        return stringFromProfanityFilter(getProfanityFilter());
      }

    public bool  hasClientMatches()
      {
        return flagHasClientMatches;
      }

    public int  countOfClientMatches()
      {
        Debug.Assert(flagHasClientMatches);
        return storeClientMatches.Count;
      }

    public ClientMatchJSON   elementOfClientMatches(int element_num)
      {
        Debug.Assert(flagHasClientMatches);
        return storeClientMatches[element_num];
      }

    public List< ClientMatchJSON  >  getClientMatches()
      {
        Debug.Assert(flagHasClientMatches);
        return storeClientMatches;
      }

    public bool  hasClientMatchesOnly()
      {
        return flagHasClientMatchesOnly;
      }

    public bool  getClientMatchesOnly()
      {
        return storeClientMatchesOnly;
      }

    public bool  hasPagination()
      {
        return flagHasPagination;
      }

    public PaginationJSON   getPagination()
      {
        Debug.Assert(flagHasPagination);
        return storePagination;
      }

    public bool  hasResponseAudioVoice()
      {
        return flagHasResponseAudioVoice;
      }

    public string  getResponseAudioVoice()
      {
        Debug.Assert(flagHasResponseAudioVoice);
        return storeResponseAudioVoice;
      }

    public bool  hasResponseAudioShortOrLong()
      {
        return flagHasResponseAudioShortOrLong;
      }

    public TypeResponseAudioShortOrLong  getResponseAudioShortOrLong()
      {
        Debug.Assert(flagHasResponseAudioShortOrLong);
        return storeResponseAudioShortOrLong;
      }

    public string  getResponseAudioShortOrLongAsString()
      {
        return stringFromResponseAudioShortOrLong(getResponseAudioShortOrLong());
      }

    public bool  hasResponseAudioAcceptedEncodings()
      {
        return flagHasResponseAudioAcceptedEncodings;
      }

    public int  countOfResponseAudioAcceptedEncodings()
      {
        Debug.Assert(flagHasResponseAudioAcceptedEncodings);
        return storeResponseAudioAcceptedEncodings.Count;
      }

    public TypeResponseAudioAcceptedEncodings  elementOfResponseAudioAcceptedEncodings(int element_num)
      {
        Debug.Assert(flagHasResponseAudioAcceptedEncodings);
        return storeResponseAudioAcceptedEncodings[element_num];
      }

    public List< TypeResponseAudioAcceptedEncodings >  getResponseAudioAcceptedEncodings()
      {
        Debug.Assert(flagHasResponseAudioAcceptedEncodings);
        return storeResponseAudioAcceptedEncodings;
      }

    public bool  hasReturnResponseAudioAsURL()
      {
        return flagHasReturnResponseAudioAsURL;
      }

    public bool  getReturnResponseAudioAsURL()
      {
        Debug.Assert(flagHasReturnResponseAudioAsURL);
        return storeReturnResponseAudioAsURL;
      }

    public bool  hasVoiceActivityDetection()
      {
        return flagHasVoiceActivityDetection;
      }

    public VoiceActivityDetectionJSON   getVoiceActivityDetection()
      {
        Debug.Assert(flagHasVoiceActivityDetection);
        return storeVoiceActivityDetection;
      }

    public bool  hasServerDeterminesEndOfAudio()
      {
        return flagHasServerDeterminesEndOfAudio;
      }

    public bool  getServerDeterminesEndOfAudio()
      {
        Debug.Assert(flagHasServerDeterminesEndOfAudio);
        return storeServerDeterminesEndOfAudio;
      }

    public bool  hasIntentOnly()
      {
        return flagHasIntentOnly;
      }

    public bool  getIntentOnly()
      {
        Debug.Assert(flagHasIntentOnly);
        return storeIntentOnly;
      }

    public bool  hasDisableSpellCorrection()
      {
        return flagHasDisableSpellCorrection;
      }

    public bool  getDisableSpellCorrection()
      {
        Debug.Assert(flagHasDisableSpellCorrection);
        return storeDisableSpellCorrection;
      }

    public bool  hasUseContactData()
      {
        return flagHasUseContactData;
      }

    public bool  getUseContactData()
      {
        return storeUseContactData;
      }

    public bool  hasUseClientTime()
      {
        return flagHasUseClientTime;
      }

    public bool  getUseClientTime()
      {
        return storeUseClientTime;
      }

    public bool  hasForceConversationStateTime()
      {
        return flagHasForceConversationStateTime;
      }

    public BigInteger  getForceConversationStateTime()
      {
        Debug.Assert(flagHasForceConversationStateTime);
        return storeForceConversationStateTime;
      }

    public bool  hasOutputLatticeSize()
      {
        return flagHasOutputLatticeSize;
      }

    public BigInteger  getOutputLatticeSize()
      {
        return storeOutputLatticeSize;
      }

    public bool  hasMatchingMutations()
      {
        return flagHasMatchingMutations;
      }

    public MatchingMutationsJSON   getMatchingMutations()
      {
        Debug.Assert(flagHasMatchingMutations);
        return storeMatchingMutations;
      }

    public bool  hasUseFormattedTranscriptionAsDefault()
      {
        return flagHasUseFormattedTranscriptionAsDefault;
      }

    public bool  getUseFormattedTranscriptionAsDefault()
      {
        return storeUseFormattedTranscriptionAsDefault;
      }

    public bool  hasResponseRanking()
      {
        return flagHasResponseRanking;
      }

    public ResponseRankingJSON   getResponseRanking()
      {
        Debug.Assert(flagHasResponseRanking);
        return storeResponseRanking;
      }


    public virtual int extraRequestInfoComponentCount()
      {
        Debug.Assert(extraKeys.Count == extraValues.Count);
        return extraKeys.Count;
      }
    public virtual string extraRequestInfoComponentKey(int component_num)
      {
        Debug.Assert(extraKeys.Count == extraValues.Count);
        Debug.Assert(component_num < extraValues.Count);
        return extraKeys[component_num];
      }
    public virtual JSONValue extraRequestInfoComponentValue(int component_num)
      {
        Debug.Assert(extraKeys.Count == extraValues.Count);
        Debug.Assert(component_num < extraValues.Count);
        return extraValues[component_num];
      }
    public virtual JSONValue extraRequestInfoLookup(string field_name)
      {
        JSONValue result = (extraIndex.ContainsKey(field_name) ? extraIndex[field_name] : null);
        return result;
      }

    public void setLatitude(double new_value)
      {
        flagHasLatitude = true;
        if (new_value < -90)
            throw new Exception("The value for field Latitude of RequestInfoJSON is less than the lower bound (-90) for that field.");
        if (new_value > 90)
            throw new Exception("The value for field Latitude of RequestInfoJSON is greater than the upper bound (90) for that field.");
        storeLatitude = new_value;
        textStoreLatitude = "";
      }
    public void setLatitudeText(string new_value)
      {
        flagHasLatitude = true;
        if (!(JSONWriter.is_valid_number_format(new_value)))
            throw new Exception("The text value for field Latitude of RequestInfoJSON is not a valid number.");
        if (JSONWriter.compare_number_text_to_repeating_fraction(new_value, true, "90", "", false, "2") < 0)
            throw new Exception("The value for field Latitude of RequestInfoJSON is less than the lower bound (-90) for that field.");
        if (JSONWriter.compare_number_text_to_repeating_fraction(new_value, false, "90", "", false, "2") > 0)
            throw new Exception("The value for field Latitude of RequestInfoJSON is greater than the upper bound (90) for that field.");
        textStoreLatitude = new_value;
      }
    public void unsetLatitude()
      {
        flagHasLatitude = false;
      }
    public void setLongitude(double new_value)
      {
        flagHasLongitude = true;
        if (new_value < -180)
            throw new Exception("The value for field Longitude of RequestInfoJSON is less than the lower bound (-180) for that field.");
        if (new_value > 180)
            throw new Exception("The value for field Longitude of RequestInfoJSON is greater than the upper bound (180) for that field.");
        storeLongitude = new_value;
        textStoreLongitude = "";
      }
    public void setLongitudeText(string new_value)
      {
        flagHasLongitude = true;
        if (!(JSONWriter.is_valid_number_format(new_value)))
            throw new Exception("The text value for field Longitude of RequestInfoJSON is not a valid number.");
        if (JSONWriter.compare_number_text_to_repeating_fraction(new_value, true, "180", "", false, "3") < 0)
            throw new Exception("The value for field Longitude of RequestInfoJSON is less than the lower bound (-180) for that field.");
        if (JSONWriter.compare_number_text_to_repeating_fraction(new_value, false, "180", "", false, "3") > 0)
            throw new Exception("The value for field Longitude of RequestInfoJSON is greater than the upper bound (180) for that field.");
        textStoreLongitude = new_value;
      }
    public void unsetLongitude()
      {
        flagHasLongitude = false;
      }
    public void setPositionTime(BigInteger new_value)
      {
        flagHasPositionTime = true;
        storePositionTime = new_value;
      }
    public void unsetPositionTime()
      {
        flagHasPositionTime = false;
      }
    public void setPositionHorizontalAccuracy(double new_value)
      {
        flagHasPositionHorizontalAccuracy = true;
        if (new_value < 0)
            throw new Exception("The value for field PositionHorizontalAccuracy of RequestInfoJSON is less than the lower bound (0) for that field.");
        storePositionHorizontalAccuracy = new_value;
        textStorePositionHorizontalAccuracy = "";
      }
    public void setPositionHorizontalAccuracyText(string new_value)
      {
        flagHasPositionHorizontalAccuracy = true;
        if (!(JSONWriter.is_valid_number_format(new_value)))
            throw new Exception("The text value for field PositionHorizontalAccuracy of RequestInfoJSON is not a valid number.");
        if (JSONWriter.compare_number_text_to_repeating_fraction(new_value, false, "", "", false, "") < 0)
            throw new Exception("The value for field PositionHorizontalAccuracy of RequestInfoJSON is less than the lower bound (0) for that field.");
        textStorePositionHorizontalAccuracy = new_value;
      }
    public void unsetPositionHorizontalAccuracy()
      {
        flagHasPositionHorizontalAccuracy = false;
      }
    public void setStreet(string new_value)
      {
        flagHasStreet = true;
        storeStreet = new_value;
      }
    public void unsetStreet()
      {
        flagHasStreet = false;
      }
    public void setCity(string new_value)
      {
        flagHasCity = true;
        storeCity = new_value;
      }
    public void unsetCity()
      {
        flagHasCity = false;
      }
    public void setState(string new_value)
      {
        flagHasState = true;
        storeState = new_value;
      }
    public void unsetState()
      {
        flagHasState = false;
      }
    public void setCountry(string new_value)
      {
        flagHasCountry = true;
        storeCountry = new_value;
      }
    public void unsetCountry()
      {
        flagHasCountry = false;
      }
    public void setRoutePoints(RoutePointsJSON  new_value)
      {
        if (flagHasRoutePoints)
          {
          }
        flagHasRoutePoints = true;
        storeRoutePoints = new_value;
      }
    public void unsetRoutePoints()
      {
        if (flagHasRoutePoints)
          {
          }
        flagHasRoutePoints = false;
      }
    public void setRouteInformation(ClientRouteInformationJSON  new_value)
      {
        if (flagHasRouteInformation)
          {
          }
        flagHasRouteInformation = true;
        storeRouteInformation = new_value;
      }
    public void unsetRouteInformation()
      {
        if (flagHasRouteInformation)
          {
          }
        flagHasRouteInformation = false;
      }
    public void setControllableTrackPlaying(bool new_value)
      {
        flagHasControllableTrackPlaying = true;
        storeControllableTrackPlaying = new_value;
      }
    public void unsetControllableTrackPlaying()
      {
        flagHasControllableTrackPlaying = false;
      }
    public void setTimeStamp(BigInteger new_value)
      {
        flagHasTimeStamp = true;
        storeTimeStamp = new_value;
      }
    public void unsetTimeStamp()
      {
        flagHasTimeStamp = false;
      }
    public void setTimeZone(string new_value)
      {
        flagHasTimeZone = true;
        storeTimeZone = new_value;
      }
    public void unsetTimeZone()
      {
        flagHasTimeZone = false;
      }
    public void setConversationState(ConversationStateJSON  new_value)
      {
        if (flagHasConversationState)
          {
          }
        flagHasConversationState = true;
        storeConversationState = new_value;
      }
    public void unsetConversationState()
      {
        if (flagHasConversationState)
          {
          }
        flagHasConversationState = false;
      }
    public void setClientState(ClientStateJSON  new_value)
      {
        if (flagHasClientState)
          {
          }
        flagHasClientState = true;
        storeClientState = new_value;
      }
    public void unsetClientState()
      {
        if (flagHasClientState)
          {
          }
        flagHasClientState = false;
      }
    public void setDeviceInfo(TypeDeviceInfoJSON  new_value)
      {
        if (flagHasDeviceInfo)
          {
          }
        flagHasDeviceInfo = true;
        storeDeviceInfo = new_value;
      }
    public void unsetDeviceInfo()
      {
        if (flagHasDeviceInfo)
          {
          }
        flagHasDeviceInfo = false;
      }
    public void setSendBack(JSONValue  new_value)
      {
        if (flagHasSendBack)
          {
          }
        flagHasSendBack = true;
        storeSendBack = new_value;
      }
    public void unsetSendBack()
      {
        if (flagHasSendBack)
          {
          }
        flagHasSendBack = false;
      }
    public void initPreferredImageSize()
      {
        flagHasPreferredImageSize = true;
        storePreferredImageSize.Clear();
      }
    public void appendPreferredImageSize(BigInteger to_append)
      {
        if (!flagHasPreferredImageSize)
          {
            flagHasPreferredImageSize = true;
            storePreferredImageSize.Clear();
          }
        Debug.Assert(flagHasPreferredImageSize);
        storePreferredImageSize.Add(to_append);
      }
    public void unsetPreferredImageSize()
      {
        flagHasPreferredImageSize = false;
        storePreferredImageSize.Clear();
      }
    public void setInputLanguageEnglishName(string new_value)
      {
        flagHasInputLanguageEnglishName = true;
        storeInputLanguageEnglishName = new_value;
      }
    public void unsetInputLanguageEnglishName()
      {
        flagHasInputLanguageEnglishName = false;
      }
    public void setInputLanguageNativeName(string new_value)
      {
        flagHasInputLanguageNativeName = true;
        storeInputLanguageNativeName = new_value;
      }
    public void unsetInputLanguageNativeName()
      {
        flagHasInputLanguageNativeName = false;
      }
    public void setInputLanguageIETFTag(string new_value)
      {
        flagHasInputLanguageIETFTag = true;
        storeInputLanguageIETFTag = new_value;
      }
    public void unsetInputLanguageIETFTag()
      {
        flagHasInputLanguageIETFTag = false;
      }
    public void setOutputLanguageEnglishName(string new_value)
      {
        flagHasOutputLanguageEnglishName = true;
        storeOutputLanguageEnglishName = new_value;
      }
    public void unsetOutputLanguageEnglishName()
      {
        flagHasOutputLanguageEnglishName = false;
      }
    public void setOutputLanguageNativeName(string new_value)
      {
        flagHasOutputLanguageNativeName = true;
        storeOutputLanguageNativeName = new_value;
      }
    public void unsetOutputLanguageNativeName()
      {
        flagHasOutputLanguageNativeName = false;
      }
    public void setOutputLanguageIETFTag(string new_value)
      {
        flagHasOutputLanguageIETFTag = true;
        storeOutputLanguageIETFTag = new_value;
      }
    public void unsetOutputLanguageIETFTag()
      {
        flagHasOutputLanguageIETFTag = false;
      }
    public void setResultVersionAccepted(double new_value)
      {
        flagHasResultVersionAccepted = true;
        if (new_value < 1)
            throw new Exception("The value for field ResultVersionAccepted of RequestInfoJSON is less than the lower bound (1) for that field.");
        storeResultVersionAccepted = new_value;
        textStoreResultVersionAccepted = "";
      }
    public void setResultVersionAcceptedText(string new_value)
      {
        flagHasResultVersionAccepted = true;
        if (!(JSONWriter.is_valid_number_format(new_value)))
            throw new Exception("The text value for field ResultVersionAccepted of RequestInfoJSON is not a valid number.");
        if (JSONWriter.compare_number_text_to_repeating_fraction(new_value, false, "1", "", false, "1") < 0)
            throw new Exception("The value for field ResultVersionAccepted of RequestInfoJSON is less than the lower bound (1) for that field.");
        textStoreResultVersionAccepted = new_value;
      }
    public void unsetResultVersionAccepted()
      {
        flagHasResultVersionAccepted = false;
      }
    public void setUnitPreference(TypeUnitPreference new_value)
      {
        flagHasUnitPreference = true;
        storeUnitPreference = new_value;
      }
    public void setUnitPreference(string chars)
      {
        setUnitPreference(stringToUnitPreference(chars));
      }
    public void unsetUnitPreference()
      {
        flagHasUnitPreference = false;
      }
    public void setDefaultTimeFormat24Hours(bool new_value)
      {
        flagHasDefaultTimeFormat24Hours = true;
        storeDefaultTimeFormat24Hours = new_value;
      }
    public void unsetDefaultTimeFormat24Hours()
      {
        flagHasDefaultTimeFormat24Hours = false;
      }
    public void setClientID(string new_value)
      {
        flagHasClientID = true;
        storeClientID = new_value;
      }
    public void unsetClientID()
      {
        flagHasClientID = false;
      }
    public void setClientVersion(TypeClientVersion new_value)
      {
        flagHasClientVersion = true;
        switch (new_value.key)
          {
            case 0:
                break;
            case 1:
                if (new_value.choice1 < 0)
                    throw new Exception("The value for case 1 of field ClientVersion of RequestInfoJSON is less than the lower bound (0) for that field.");
                break;
            default:
                Debug.Assert(false);
                break;
          }
        storeClientVersion = new_value;
      }
    public void unsetClientVersion()
      {
        flagHasClientVersion = false;
      }
    public void setDeviceID(string new_value)
      {
        flagHasDeviceID = true;
        storeDeviceID = new_value;
      }
    public void unsetDeviceID()
      {
        flagHasDeviceID = false;
      }
    public void setSDK(string new_value)
      {
        flagHasSDK = true;
        storeSDK = new_value;
      }
    public void unsetSDK()
      {
        flagHasSDK = false;
      }
    public void setSDKInfo(JSONObjectValue  new_value)
      {
        if (flagHasSDKInfo)
          {
          }
        flagHasSDKInfo = true;
        storeSDKInfo = new_value;
      }
    public void unsetSDKInfo()
      {
        if (flagHasSDKInfo)
          {
          }
        flagHasSDKInfo = false;
      }
    public void setFirstPersonSelf(string new_value)
      {
        flagHasFirstPersonSelf = true;
        storeFirstPersonSelf = new_value;
      }
    public void unsetFirstPersonSelf()
      {
        flagHasFirstPersonSelf = false;
      }
    public void setFirstPersonSelfSpoken(string new_value)
      {
        flagHasFirstPersonSelfSpoken = true;
        storeFirstPersonSelfSpoken = new_value;
      }
    public void unsetFirstPersonSelfSpoken()
      {
        flagHasFirstPersonSelfSpoken = false;
      }
    public void initSecondPersonSelf()
      {
        flagHasSecondPersonSelf = true;
        storeSecondPersonSelf.Clear();
      }
    public void appendSecondPersonSelf(string to_append)
      {
        if (!flagHasSecondPersonSelf)
          {
            flagHasSecondPersonSelf = true;
            storeSecondPersonSelf.Clear();
          }
        Debug.Assert(flagHasSecondPersonSelf);
        storeSecondPersonSelf.Add(to_append);
      }
    public void unsetSecondPersonSelf()
      {
        flagHasSecondPersonSelf = false;
        storeSecondPersonSelf.Clear();
      }
    public void initSecondPersonSelfSpoken()
      {
        flagHasSecondPersonSelfSpoken = true;
        storeSecondPersonSelfSpoken.Clear();
      }
    public void appendSecondPersonSelfSpoken(string to_append)
      {
        if (!flagHasSecondPersonSelfSpoken)
          {
            flagHasSecondPersonSelfSpoken = true;
            storeSecondPersonSelfSpoken.Clear();
          }
        Debug.Assert(flagHasSecondPersonSelfSpoken);
        storeSecondPersonSelfSpoken.Add(to_append);
      }
    public void unsetSecondPersonSelfSpoken()
      {
        flagHasSecondPersonSelfSpoken = false;
        storeSecondPersonSelfSpoken.Clear();
      }
    public void setWakeUpPhraseIncludedInAudio(bool new_value)
      {
        flagHasWakeUpPhraseIncludedInAudio = true;
        storeWakeUpPhraseIncludedInAudio = new_value;
      }
    public void unsetWakeUpPhraseIncludedInAudio()
      {
        flagHasWakeUpPhraseIncludedInAudio = false;
      }
    public void setInitialSecondsOfAudioToIgnore(double new_value)
      {
        flagHasInitialSecondsOfAudioToIgnore = true;
        storeInitialSecondsOfAudioToIgnore = new_value;
        textStoreInitialSecondsOfAudioToIgnore = "";
      }
    public void setInitialSecondsOfAudioToIgnoreText(string new_value)
      {
        flagHasInitialSecondsOfAudioToIgnore = true;
        if (!(JSONWriter.is_valid_number_format(new_value)))
            throw new Exception("The text value for field InitialSecondsOfAudioToIgnore of RequestInfoJSON is not a valid number.");
        textStoreInitialSecondsOfAudioToIgnore = new_value;
      }
    public void unsetInitialSecondsOfAudioToIgnore()
      {
        flagHasInitialSecondsOfAudioToIgnore = false;
      }
    public void setWakeUpPattern(string new_value)
      {
        flagHasWakeUpPattern = true;
        storeWakeUpPattern = new_value;
      }
    public void unsetWakeUpPattern()
      {
        flagHasWakeUpPattern = false;
      }
    public void setUserID(string new_value)
      {
        flagHasUserID = true;
        storeUserID = new_value;
      }
    public void unsetUserID()
      {
        flagHasUserID = false;
      }
    public void setRequestID(string new_value)
      {
        flagHasRequestID = true;
        storeRequestID = new_value;
      }
    public void unsetRequestID()
      {
        flagHasRequestID = false;
      }
    public void setSessionID(string new_value)
      {
        flagHasSessionID = true;
        storeSessionID = new_value;
      }
    public void unsetSessionID()
      {
        flagHasSessionID = false;
      }
    public void setDomains(DomainsJSON  new_value)
      {
        if (flagHasDomains)
          {
          }
        flagHasDomains = true;
        storeDomains = new_value;
      }
    public void unsetDomains()
      {
        if (flagHasDomains)
          {
          }
        flagHasDomains = false;
      }
    public void setResultUpdateAllowed(bool new_value)
      {
        flagHasResultUpdateAllowed = true;
        storeResultUpdateAllowed = new_value;
      }
    public void unsetResultUpdateAllowed()
      {
        flagHasResultUpdateAllowed = false;
      }
    public void setPartialTranscriptsDesired(bool new_value)
      {
        flagHasPartialTranscriptsDesired = true;
        storePartialTranscriptsDesired = new_value;
      }
    public void unsetPartialTranscriptsDesired()
      {
        flagHasPartialTranscriptsDesired = false;
      }
    public void setMinResults(BigInteger new_value)
      {
        flagHasMinResults = true;
        if (new_value < 1)
            throw new Exception("The value for field MinResults of RequestInfoJSON is less than the lower bound (1) for that field.");
        storeMinResults = new_value;
      }
    public void unsetMinResults()
      {
        flagHasMinResults = false;
      }
    public void setMaxResults(BigInteger new_value)
      {
        flagHasMaxResults = true;
        if (new_value < 1)
            throw new Exception("The value for field MaxResults of RequestInfoJSON is less than the lower bound (1) for that field.");
        storeMaxResults = new_value;
      }
    public void unsetMaxResults()
      {
        flagHasMaxResults = false;
      }
    public void setObjectByteCountPrefix(bool new_value)
      {
        flagHasObjectByteCountPrefix = true;
        storeObjectByteCountPrefix = new_value;
      }
    public void unsetObjectByteCountPrefix()
      {
        flagHasObjectByteCountPrefix = false;
      }
    public void setProfanityFilter(TypeProfanityFilter new_value)
      {
        flagHasProfanityFilter = true;
        storeProfanityFilter = new_value;
      }
    public void setProfanityFilter(string chars)
      {
        setProfanityFilter(stringToProfanityFilter(chars));
      }
    public void unsetProfanityFilter()
      {
        flagHasProfanityFilter = false;
      }
    public void initClientMatches()
      {
        if (flagHasClientMatches)
          {
            for (int num1 = 0; num1 < storeClientMatches.Count; ++num1)
              {
              }
          }
        flagHasClientMatches = true;
        storeClientMatches.Clear();
      }
    public void appendClientMatches(ClientMatchJSON  to_append)
      {
        if (!flagHasClientMatches)
          {
            flagHasClientMatches = true;
            storeClientMatches.Clear();
          }
        Debug.Assert(flagHasClientMatches);
        storeClientMatches.Add(to_append);
      }
    public void unsetClientMatches()
      {
        if (flagHasClientMatches)
          {
            for (int num2 = 0; num2 < storeClientMatches.Count; ++num2)
              {
              }
          }
        flagHasClientMatches = false;
        storeClientMatches.Clear();
      }
    public void setClientMatchesOnly(bool new_value)
      {
        flagHasClientMatchesOnly = true;
        storeClientMatchesOnly = new_value;
      }
    public void unsetClientMatchesOnly()
      {
        flagHasClientMatchesOnly = false;
      }
    public void setPagination(PaginationJSON  new_value)
      {
        if (flagHasPagination)
          {
          }
        flagHasPagination = true;
        storePagination = new_value;
      }
    public void unsetPagination()
      {
        if (flagHasPagination)
          {
          }
        flagHasPagination = false;
      }
    public void setResponseAudioVoice(string new_value)
      {
        flagHasResponseAudioVoice = true;
        storeResponseAudioVoice = new_value;
      }
    public void unsetResponseAudioVoice()
      {
        flagHasResponseAudioVoice = false;
      }
    public void setResponseAudioShortOrLong(TypeResponseAudioShortOrLong new_value)
      {
        flagHasResponseAudioShortOrLong = true;
        storeResponseAudioShortOrLong = new_value;
      }
    public void setResponseAudioShortOrLong(string chars)
      {
        setResponseAudioShortOrLong(stringToResponseAudioShortOrLong(chars));
      }
    public void unsetResponseAudioShortOrLong()
      {
        flagHasResponseAudioShortOrLong = false;
      }
    public void initResponseAudioAcceptedEncodings()
      {
        flagHasResponseAudioAcceptedEncodings = true;
        storeResponseAudioAcceptedEncodings.Clear();
      }
    public void appendResponseAudioAcceptedEncodings(TypeResponseAudioAcceptedEncodings to_append)
      {
        if (!flagHasResponseAudioAcceptedEncodings)
          {
            flagHasResponseAudioAcceptedEncodings = true;
            storeResponseAudioAcceptedEncodings.Clear();
          }
        Debug.Assert(flagHasResponseAudioAcceptedEncodings);
        storeResponseAudioAcceptedEncodings.Add(to_append);
      }
    public void appendResponseAudioAcceptedEncodings(string chars)
      {
        TypeResponseAudioAcceptedEncodingsKnownValues known = stringToResponseAudioAcceptedEncodings(chars);
        TypeResponseAudioAcceptedEncodings new_value = new TypeResponseAudioAcceptedEncodings();
        if (known == TypeResponseAudioAcceptedEncodingsKnownValues.ResponseAudioAcceptedEncodings__none)
          {
            new_value.in_known_list = false;
            new_value.string_value = chars;
          }
        else
          {
            new_value.in_known_list = true;
            new_value.list_value = known;
          }
        appendResponseAudioAcceptedEncodings(new_value);
      }
    public void appendResponseAudioAcceptedEncodings(TypeResponseAudioAcceptedEncodingsKnownValues new_value)
      {
        TypeResponseAudioAcceptedEncodings new_full_value = new TypeResponseAudioAcceptedEncodings();
        Debug.Assert(new_value != TypeResponseAudioAcceptedEncodingsKnownValues.ResponseAudioAcceptedEncodings__none);
        new_full_value.in_known_list = true;
        new_full_value.list_value = new_value;
        appendResponseAudioAcceptedEncodings(new_full_value);
      }
    public void unsetResponseAudioAcceptedEncodings()
      {
        flagHasResponseAudioAcceptedEncodings = false;
        storeResponseAudioAcceptedEncodings.Clear();
      }
    public void setReturnResponseAudioAsURL(bool new_value)
      {
        flagHasReturnResponseAudioAsURL = true;
        storeReturnResponseAudioAsURL = new_value;
      }
    public void unsetReturnResponseAudioAsURL()
      {
        flagHasReturnResponseAudioAsURL = false;
      }
    public void setVoiceActivityDetection(VoiceActivityDetectionJSON  new_value)
      {
        if (flagHasVoiceActivityDetection)
          {
          }
        flagHasVoiceActivityDetection = true;
        storeVoiceActivityDetection = new_value;
      }
    public void unsetVoiceActivityDetection()
      {
        if (flagHasVoiceActivityDetection)
          {
          }
        flagHasVoiceActivityDetection = false;
      }
    public void setServerDeterminesEndOfAudio(bool new_value)
      {
        flagHasServerDeterminesEndOfAudio = true;
        storeServerDeterminesEndOfAudio = new_value;
      }
    public void unsetServerDeterminesEndOfAudio()
      {
        flagHasServerDeterminesEndOfAudio = false;
      }
    public void setIntentOnly(bool new_value)
      {
        flagHasIntentOnly = true;
        storeIntentOnly = new_value;
      }
    public void unsetIntentOnly()
      {
        flagHasIntentOnly = false;
      }
    public void setDisableSpellCorrection(bool new_value)
      {
        flagHasDisableSpellCorrection = true;
        storeDisableSpellCorrection = new_value;
      }
    public void unsetDisableSpellCorrection()
      {
        flagHasDisableSpellCorrection = false;
      }
    public void setUseContactData(bool new_value)
      {
        flagHasUseContactData = true;
        storeUseContactData = new_value;
      }
    public void unsetUseContactData()
      {
        flagHasUseContactData = false;
      }
    public void setUseClientTime(bool new_value)
      {
        flagHasUseClientTime = true;
        storeUseClientTime = new_value;
      }
    public void unsetUseClientTime()
      {
        flagHasUseClientTime = false;
      }
    public void setForceConversationStateTime(BigInteger new_value)
      {
        flagHasForceConversationStateTime = true;
        storeForceConversationStateTime = new_value;
      }
    public void unsetForceConversationStateTime()
      {
        flagHasForceConversationStateTime = false;
      }
    public void setOutputLatticeSize(BigInteger new_value)
      {
        flagHasOutputLatticeSize = true;
        if (new_value < 0)
            throw new Exception("The value for field OutputLatticeSize of RequestInfoJSON is less than the lower bound (0) for that field.");
        storeOutputLatticeSize = new_value;
      }
    public void unsetOutputLatticeSize()
      {
        flagHasOutputLatticeSize = false;
      }
    public void setMatchingMutations(MatchingMutationsJSON  new_value)
      {
        if (flagHasMatchingMutations)
          {
          }
        flagHasMatchingMutations = true;
        storeMatchingMutations = new_value;
      }
    public void unsetMatchingMutations()
      {
        if (flagHasMatchingMutations)
          {
          }
        flagHasMatchingMutations = false;
      }
    public void setUseFormattedTranscriptionAsDefault(bool new_value)
      {
        flagHasUseFormattedTranscriptionAsDefault = true;
        storeUseFormattedTranscriptionAsDefault = new_value;
      }
    public void unsetUseFormattedTranscriptionAsDefault()
      {
        flagHasUseFormattedTranscriptionAsDefault = false;
      }
    public void setResponseRanking(ResponseRankingJSON  new_value)
      {
        if (flagHasResponseRanking)
          {
          }
        flagHasResponseRanking = true;
        storeResponseRanking = new_value;
      }
    public void unsetResponseRanking()
      {
        if (flagHasResponseRanking)
          {
          }
        flagHasResponseRanking = false;
      }

    public virtual void extraRequestInfoAppendPair(string key, JSONValue new_component)
      {
        Debug.Assert(key != null);
        Debug.Assert(new_component != null);

        Debug.Assert(extraKeys.Count == extraValues.Count);
        extraKeys.Add(key);
        extraValues.Add(new_component);
        extraIndex.Add(key, new_component);
      }
    public virtual void extraRequestInfoSetField(string key, JSONValue new_component)
      {
        Debug.Assert(key != null);
        Debug.Assert(new_component != null);

        JSONValue old_field = extraRequestInfoLookup(key);
        if (old_field == null)
          {
            extraRequestInfoAppendPair(key, new_component);
          }
        else
          {
            int count = extraKeys.Count;
            Debug.Assert(count == extraValues.Count);
            for (int num = 0; num < count; ++num)
              {
                if (extraKeys[num].Equals( key))
                  {
                    extraValues[num] = new_component;
                    break;
                  }
              }
            extraIndex.Add(key, new_component);
          }
      }

    public override void write_as_json(JSONHandler handler)
      {
        handler.start_object();
        write_fields_as_json(handler);
        int extra_count = extraKeys.Count;
        Debug.Assert(extra_count == extraValues.Count);
        for (int extra_num = 0; extra_num < extra_count; ++extra_num)
          {
            handler.start_pair(extraKeys[extra_num]);
            extraValues[extra_num].write(handler);
          }
        handler.finish_object();
      }

    public virtual void write_fields_as_json(JSONHandler handler)
      {
        write_fields_as_json(handler, false);
      }

    public virtual void write_fields_as_json(JSONHandler handler, bool partial_allowed)
      {
        if (flagHasLatitude)
          {
            handler.start_pair("Latitude");
            if (textStoreLatitude != "")
                handler.number_value(textStoreLatitude);
            else if (((double)(long)storeLatitude) == storeLatitude)
                handler.number_value((long)storeLatitude);
            else
                handler.number_value(storeLatitude);
          }
        if (flagHasLongitude)
          {
            handler.start_pair("Longitude");
            if (textStoreLongitude != "")
                handler.number_value(textStoreLongitude);
            else if (((double)(long)storeLongitude) == storeLongitude)
                handler.number_value((long)storeLongitude);
            else
                handler.number_value(storeLongitude);
          }
        if (flagHasPositionTime)
          {
            handler.start_pair("PositionTime");
            handler.number_value(storePositionTime);
          }
        if (flagHasPositionHorizontalAccuracy)
          {
            handler.start_pair("PositionHorizontalAccuracy");
            if (textStorePositionHorizontalAccuracy != "")
                handler.number_value(textStorePositionHorizontalAccuracy);
            else if (((double)(long)storePositionHorizontalAccuracy) == storePositionHorizontalAccuracy)
                handler.number_value((long)storePositionHorizontalAccuracy);
            else
                handler.number_value(storePositionHorizontalAccuracy);
          }
        if (flagHasStreet)
          {
            handler.start_pair("Street");
            handler.string_value(storeStreet);
          }
        if (flagHasCity)
          {
            handler.start_pair("City");
            handler.string_value(storeCity);
          }
        if (flagHasState)
          {
            handler.start_pair("State");
            handler.string_value(storeState);
          }
        if (flagHasCountry)
          {
            handler.start_pair("Country");
            handler.string_value(storeCountry);
          }
        if (flagHasRoutePoints)
          {
            handler.start_pair("RoutePoints");
            if (partial_allowed)
                storeRoutePoints.write_partial_as_json(handler);
            else
                storeRoutePoints.write_as_json(handler);
          }
        if (flagHasRouteInformation)
          {
            handler.start_pair("RouteInformation");
            if (partial_allowed)
                storeRouteInformation.write_partial_as_json(handler);
            else
                storeRouteInformation.write_as_json(handler);
          }
        if (flagHasControllableTrackPlaying)
          {
            handler.start_pair("ControllableTrackPlaying");
            handler.boolean_value(storeControllableTrackPlaying);
          }
        if (flagHasTimeStamp)
          {
            handler.start_pair("TimeStamp");
            handler.number_value(storeTimeStamp);
          }
        if (flagHasTimeZone)
          {
            handler.start_pair("TimeZone");
            handler.string_value(storeTimeZone);
          }
        if (flagHasConversationState)
          {
            handler.start_pair("ConversationState");
            if (partial_allowed)
                storeConversationState.write_partial_as_json(handler);
            else
                storeConversationState.write_as_json(handler);
          }
        if (flagHasClientState)
          {
            handler.start_pair("ClientState");
            if (partial_allowed)
                storeClientState.write_partial_as_json(handler);
            else
                storeClientState.write_as_json(handler);
          }
        if (flagHasDeviceInfo)
          {
            handler.start_pair("DeviceInfo");
            if (partial_allowed)
                storeDeviceInfo.write_partial_as_json(handler);
            else
                storeDeviceInfo.write_as_json(handler);
          }
        if (flagHasSendBack)
          {
            handler.start_pair("SendBack");
            storeSendBack.write(handler);
          }
        if (flagHasPreferredImageSize)
          {
            handler.start_pair("PreferredImageSize");
            Debug.Assert(storePreferredImageSize.Count >= 2);
            handler.start_array();
            for (int num1 = 0; num1 < storePreferredImageSize.Count; ++num1)
              {
                handler.number_value(storePreferredImageSize[num1]);
              }
            handler.finish_array();
          }
        if (flagHasInputLanguageEnglishName)
          {
            handler.start_pair("InputLanguageEnglishName");
            handler.string_value(storeInputLanguageEnglishName);
          }
        if (flagHasInputLanguageNativeName)
          {
            handler.start_pair("InputLanguageNativeName");
            handler.string_value(storeInputLanguageNativeName);
          }
        if (flagHasInputLanguageIETFTag)
          {
            handler.start_pair("InputLanguageIETFTag");
            handler.string_value(storeInputLanguageIETFTag);
          }
        if (flagHasOutputLanguageEnglishName)
          {
            handler.start_pair("OutputLanguageEnglishName");
            handler.string_value(storeOutputLanguageEnglishName);
          }
        if (flagHasOutputLanguageNativeName)
          {
            handler.start_pair("OutputLanguageNativeName");
            handler.string_value(storeOutputLanguageNativeName);
          }
        if (flagHasOutputLanguageIETFTag)
          {
            handler.start_pair("OutputLanguageIETFTag");
            handler.string_value(storeOutputLanguageIETFTag);
          }
        if (flagHasResultVersionAccepted)
          {
            handler.start_pair("ResultVersionAccepted");
            if (textStoreResultVersionAccepted != "")
                handler.number_value(textStoreResultVersionAccepted);
            else if (((double)(long)storeResultVersionAccepted) == storeResultVersionAccepted)
                handler.number_value((long)storeResultVersionAccepted);
            else
                handler.number_value(storeResultVersionAccepted);
          }
        if (flagHasUnitPreference)
          {
            handler.start_pair("UnitPreference");
            switch (storeUnitPreference)
              {
                case TypeUnitPreference.UnitPreference_US:
                    handler.string_value("US");
                    break;
                case TypeUnitPreference.UnitPreference_METRIC:
                    handler.string_value("METRIC");
                    break;
                default:
                    Debug.Assert(false);
                    break;
              }
          }
        if (flagHasDefaultTimeFormat24Hours)
          {
            handler.start_pair("DefaultTimeFormat24Hours");
            handler.boolean_value(storeDefaultTimeFormat24Hours);
          }
        if (flagHasClientID)
          {
            handler.start_pair("ClientID");
            handler.string_value(storeClientID);
          }
        if (flagHasClientVersion)
          {
            handler.start_pair("ClientVersion");
            switch (storeClientVersion.key)
              {
                case 0:
                    handler.string_value(storeClientVersion.choice0);
                    break;
                case 1:
                    handler.number_value(storeClientVersion.choice1);
                    break;
                default:
                    Debug.Assert(false);
                    break;
              }
          }
        if (flagHasDeviceID)
          {
            handler.start_pair("DeviceID");
            handler.string_value(storeDeviceID);
          }
        if (flagHasSDK)
          {
            handler.start_pair("SDK");
            handler.string_value(storeSDK);
          }
        if (flagHasSDKInfo)
          {
            handler.start_pair("SDKInfo");
            storeSDKInfo.write(handler);
          }
        if (flagHasFirstPersonSelf)
          {
            handler.start_pair("FirstPersonSelf");
            handler.string_value(storeFirstPersonSelf);
          }
        if (flagHasFirstPersonSelfSpoken)
          {
            handler.start_pair("FirstPersonSelfSpoken");
            handler.string_value(storeFirstPersonSelfSpoken);
          }
        if (flagHasSecondPersonSelf)
          {
            handler.start_pair("SecondPersonSelf");
            handler.start_array();
            for (int num2 = 0; num2 < storeSecondPersonSelf.Count; ++num2)
              {
                handler.string_value(storeSecondPersonSelf[num2]);
              }
            handler.finish_array();
          }
        if (flagHasSecondPersonSelfSpoken)
          {
            handler.start_pair("SecondPersonSelfSpoken");
            handler.start_array();
            for (int num3 = 0; num3 < storeSecondPersonSelfSpoken.Count; ++num3)
              {
                handler.string_value(storeSecondPersonSelfSpoken[num3]);
              }
            handler.finish_array();
          }
        if (flagHasWakeUpPhraseIncludedInAudio)
          {
            handler.start_pair("WakeUpPhraseIncludedInAudio");
            handler.boolean_value(storeWakeUpPhraseIncludedInAudio);
          }
        if (flagHasInitialSecondsOfAudioToIgnore)
          {
            handler.start_pair("InitialSecondsOfAudioToIgnore");
            if (textStoreInitialSecondsOfAudioToIgnore != "")
                handler.number_value(textStoreInitialSecondsOfAudioToIgnore);
            else if (((double)(long)storeInitialSecondsOfAudioToIgnore) == storeInitialSecondsOfAudioToIgnore)
                handler.number_value((long)storeInitialSecondsOfAudioToIgnore);
            else
                handler.number_value(storeInitialSecondsOfAudioToIgnore);
          }
        if (flagHasWakeUpPattern)
          {
            handler.start_pair("WakeUpPattern");
            handler.string_value(storeWakeUpPattern);
          }
        if (flagHasUserID)
          {
            handler.start_pair("UserID");
            handler.string_value(storeUserID);
          }
        if (flagHasRequestID)
          {
            handler.start_pair("RequestID");
            handler.string_value(storeRequestID);
          }
        if (flagHasSessionID)
          {
            handler.start_pair("SessionID");
            handler.string_value(storeSessionID);
          }
        if (flagHasDomains)
          {
            handler.start_pair("Domains");
            if (partial_allowed)
                storeDomains.write_partial_as_json(handler);
            else
                storeDomains.write_as_json(handler);
          }
        if (flagHasResultUpdateAllowed)
          {
            handler.start_pair("ResultUpdateAllowed");
            handler.boolean_value(storeResultUpdateAllowed);
          }
        if (flagHasPartialTranscriptsDesired)
          {
            handler.start_pair("PartialTranscriptsDesired");
            handler.boolean_value(storePartialTranscriptsDesired);
          }
        if (flagHasMinResults)
          {
            handler.start_pair("MinResults");
            handler.number_value(storeMinResults);
          }
        if (flagHasMaxResults)
          {
            handler.start_pair("MaxResults");
            handler.number_value(storeMaxResults);
          }
        if (flagHasObjectByteCountPrefix)
          {
            handler.start_pair("ObjectByteCountPrefix");
            handler.boolean_value(storeObjectByteCountPrefix);
          }
        if (flagHasProfanityFilter)
          {
            handler.start_pair("ProfanityFilter");
            switch (storeProfanityFilter)
              {
                case TypeProfanityFilter.ProfanityFilter_AllowAll:
                    handler.string_value("AllowAll");
                    break;
                case TypeProfanityFilter.ProfanityFilter_StarAllButFirst:
                    handler.string_value("StarAllButFirst");
                    break;
                case TypeProfanityFilter.ProfanityFilter_StarAll:
                    handler.string_value("StarAll");
                    break;
                default:
                    Debug.Assert(false);
                    break;
              }
          }
        if (flagHasClientMatches)
          {
            handler.start_pair("ClientMatches");
            Debug.Assert(storeClientMatches.Count >= 1);
            handler.start_array();
            for (int num4 = 0; num4 < storeClientMatches.Count; ++num4)
              {
                if (partial_allowed)
                    storeClientMatches[num4].write_partial_as_json(handler);
                else
                    storeClientMatches[num4].write_as_json(handler);
              }
            handler.finish_array();
          }
        if (flagHasClientMatchesOnly)
          {
            handler.start_pair("ClientMatchesOnly");
            handler.boolean_value(storeClientMatchesOnly);
          }
        if (flagHasPagination)
          {
            handler.start_pair("Pagination");
            if (partial_allowed)
                storePagination.write_partial_as_json(handler);
            else
                storePagination.write_as_json(handler);
          }
        if (flagHasResponseAudioVoice)
          {
            handler.start_pair("ResponseAudioVoice");
            handler.string_value(storeResponseAudioVoice);
          }
        if (flagHasResponseAudioShortOrLong)
          {
            handler.start_pair("ResponseAudioShortOrLong");
            switch (storeResponseAudioShortOrLong)
              {
                case TypeResponseAudioShortOrLong.ResponseAudioShortOrLong_Short:
                    handler.string_value("Short");
                    break;
                case TypeResponseAudioShortOrLong.ResponseAudioShortOrLong_Long:
                    handler.string_value("Long");
                    break;
                default:
                    Debug.Assert(false);
                    break;
              }
          }
        if (flagHasResponseAudioAcceptedEncodings)
          {
            handler.start_pair("ResponseAudioAcceptedEncodings");
            Debug.Assert(storeResponseAudioAcceptedEncodings.Count >= 1);
            handler.start_array();
            for (int num5 = 0; num5 < storeResponseAudioAcceptedEncodings.Count; ++num5)
              {
                if (storeResponseAudioAcceptedEncodings[num5].in_known_list)
                  {
                    switch (storeResponseAudioAcceptedEncodings[num5].list_value)
                      {
                        case TypeResponseAudioAcceptedEncodingsKnownValues.ResponseAudioAcceptedEncodings_WAV:
                            handler.string_value("WAV");
                            break;
                        case TypeResponseAudioAcceptedEncodingsKnownValues.ResponseAudioAcceptedEncodings_Speex:
                            handler.string_value("Speex");
                            break;
                        default:
                            Debug.Assert(false);
                            break;
                      }
                  }
                else
                  {
                            handler.string_value(storeResponseAudioAcceptedEncodings[num5].string_value);
                  }
              }
            handler.finish_array();
          }
        if (flagHasReturnResponseAudioAsURL)
          {
            handler.start_pair("ReturnResponseAudioAsURL");
            handler.boolean_value(storeReturnResponseAudioAsURL);
          }
        if (flagHasVoiceActivityDetection)
          {
            handler.start_pair("VoiceActivityDetection");
            if (partial_allowed)
                storeVoiceActivityDetection.write_partial_as_json(handler);
            else
                storeVoiceActivityDetection.write_as_json(handler);
          }
        if (flagHasServerDeterminesEndOfAudio)
          {
            handler.start_pair("ServerDeterminesEndOfAudio");
            handler.boolean_value(storeServerDeterminesEndOfAudio);
          }
        if (flagHasIntentOnly)
          {
            handler.start_pair("IntentOnly");
            handler.boolean_value(storeIntentOnly);
          }
        if (flagHasDisableSpellCorrection)
          {
            handler.start_pair("DisableSpellCorrection");
            handler.boolean_value(storeDisableSpellCorrection);
          }
        if (flagHasUseContactData)
          {
            handler.start_pair("UseContactData");
            handler.boolean_value(storeUseContactData);
          }
        if (flagHasUseClientTime)
          {
            handler.start_pair("UseClientTime");
            handler.boolean_value(storeUseClientTime);
          }
        if (flagHasForceConversationStateTime)
          {
            handler.start_pair("ForceConversationStateTime");
            handler.number_value(storeForceConversationStateTime);
          }
        if (flagHasOutputLatticeSize)
          {
            handler.start_pair("OutputLatticeSize");
            handler.number_value(storeOutputLatticeSize);
          }
        if (flagHasMatchingMutations)
          {
            handler.start_pair("MatchingMutations");
            if (partial_allowed)
                storeMatchingMutations.write_partial_as_json(handler);
            else
                storeMatchingMutations.write_as_json(handler);
          }
        if (flagHasUseFormattedTranscriptionAsDefault)
          {
            handler.start_pair("UseFormattedTranscriptionAsDefault");
            handler.boolean_value(storeUseFormattedTranscriptionAsDefault);
          }
        if (flagHasResponseRanking)
          {
            handler.start_pair("ResponseRanking");
            if (partial_allowed)
                storeResponseRanking.write_partial_as_json(handler);
            else
                storeResponseRanking.write_as_json(handler);
          }
      }
    public override void write_partial_as_json(JSONHandler handler)
      {
        handler.start_object();
        write_fields_as_json(handler, true);
        int extra_count = extraKeys.Count;
        Debug.Assert(extra_count == extraValues.Count);
        for (int extra_num = 0; extra_num < extra_count; ++extra_num)
          {
            handler.start_pair(extraKeys[extra_num]);
            extraValues[extra_num].write(handler);
          }
        handler.finish_object();
      }
    public virtual string missing_field_error(bool allow_unpolished)
      {
        return null;
      }

    public static RequestInfoJSON from_json(JSONValue json_value, bool ignore_extras, bool allow_incomplete, bool allow_unpolished)
      {
        RequestInfoJSON result;
          {
            HoldingGenerator generator = new HoldingGenerator("Type RequestInfo", ignore_extras);
            generator.set_allow_incomplete(allow_incomplete);
            generator.set_allow_unpolished(allow_unpolished);
            if (allow_incomplete || allow_unpolished)
                json_value.write(generator);
            else
                json_value.write(generator);
            Debug.Assert(generator.have_value);
            result = generator.value;
          };
        return result;
      }
    public static RequestInfoJSON from_json(JSONValue json_value, bool ignore_extras, bool allow_incomplete)
  {
    return from_json(json_value, ignore_extras, allow_incomplete, false);
  }
    public static RequestInfoJSON from_json(JSONBase json_value, bool ignore_extras, bool allow_incomplete, bool allow_unpolished)
      {
        RequestInfoJSON result;
          {
            HoldingGenerator generator = new HoldingGenerator("Type RequestInfo", ignore_extras);
            generator.set_allow_incomplete(allow_incomplete);
            generator.set_allow_unpolished(allow_unpolished);
            if (allow_incomplete || allow_unpolished)
                json_value.write_partial_as_json(generator);
            else
                json_value.write_as_json(generator);
            Debug.Assert(generator.have_value);
            result = generator.value;
          };
        return result;
      }
    public static RequestInfoJSON from_json(JSONBase json_value, bool ignore_extras, bool allow_incomplete)
  {
    return from_json(json_value, ignore_extras, allow_incomplete, false);
  }
    public static RequestInfoJSON from_text(string text, bool ignore_extras)
      {
        RequestInfoJSON result;
          {
            HoldingGenerator generator = new HoldingGenerator("Type RequestInfo", ignore_extras);
            JSONParse.parse_json_value(text, "Text for RequestInfoJSON", generator);
            Debug.Assert(generator.have_value);
            result = generator.value;
          };
        return result;
      }
    public static RequestInfoJSON from_file(TextReader fp, string file_name, bool ignore_extras)
      {
        RequestInfoJSON result;
          {
            HoldingGenerator generator = new HoldingGenerator("Type RequestInfo", ignore_extras);
            JSONParse.parse_json_value(fp, file_name, generator);
            Debug.Assert(generator.have_value);
            result = generator.value;
          };
        return result;
      }
    public abstract class Generator : JSONObjectGenerator
      {
        private JSONHoldingNumberTextGenerator fieldGeneratorLatitude;
        private JSONHoldingNumberTextGenerator fieldGeneratorLongitude;
    private class FieldHoldingGeneratorPositionTime : JSONHoldingIntegerUnboundRangeGenerator
          {
            public FieldHoldingGeneratorPositionTime(String what) : base(what)
              {
              }
          };
    private class FieldHoldingArrayGeneratorPositionTime : JSONHoldingIntegerUnboundRangeArrayGenerator
          {
            public FieldHoldingArrayGeneratorPositionTime(String what) : base(what)
              {
              }
          };
        private FieldHoldingGeneratorPositionTime fieldGeneratorPositionTime;
        private JSONHoldingNumberTextGenerator fieldGeneratorPositionHorizontalAccuracy;
        private JSONHoldingStringGenerator fieldGeneratorStreet;
        private JSONHoldingStringGenerator fieldGeneratorCity;
        private JSONHoldingStringGenerator fieldGeneratorState;
        private JSONHoldingStringGenerator fieldGeneratorCountry;
        private RoutePointsJSON.HoldingGenerator fieldGeneratorRoutePoints;
        private ClientRouteInformationJSON.HoldingGenerator fieldGeneratorRouteInformation;
        private JSONHoldingBooleanGenerator fieldGeneratorControllableTrackPlaying;
    private class FieldHoldingGeneratorTimeStamp : JSONHoldingIntegerUnboundRangeGenerator
          {
            public FieldHoldingGeneratorTimeStamp(String what) : base(what)
              {
              }
          };
    private class FieldHoldingArrayGeneratorTimeStamp : JSONHoldingIntegerUnboundRangeArrayGenerator
          {
            public FieldHoldingArrayGeneratorTimeStamp(String what) : base(what)
              {
              }
          };
        private FieldHoldingGeneratorTimeStamp fieldGeneratorTimeStamp;
        private JSONHoldingStringGenerator fieldGeneratorTimeZone;
        private ConversationStateJSON.HoldingGenerator fieldGeneratorConversationState;
        private ClientStateJSON.HoldingGenerator fieldGeneratorClientState;
        private TypeDeviceInfoJSON.HoldingGenerator fieldGeneratorDeviceInfo;
        private JSONHoldingValueGenerator fieldGeneratorSendBack;
    private class FieldHoldingGeneratorPreferredImageSize : JSONHoldingIntegerLowerBoundOnlyGenerator
          {
            public FieldHoldingGeneratorPreferredImageSize(String what) : base(what, (BigInteger)(1))
              {
              }
          };
    private class FieldHoldingArrayGeneratorPreferredImageSize : JSONHoldingIntegerLowerBoundOnlyArrayGenerator
          {
            public FieldHoldingArrayGeneratorPreferredImageSize(String what) : base(what, (BigInteger)(1))
              {
              }
          };
        private FieldHoldingArrayGeneratorPreferredImageSize fieldGeneratorPreferredImageSize;
        private JSONHoldingStringGenerator fieldGeneratorInputLanguageEnglishName;
        private JSONHoldingStringGenerator fieldGeneratorInputLanguageNativeName;
        private JSONHoldingStringGenerator fieldGeneratorInputLanguageIETFTag;
        private JSONHoldingStringGenerator fieldGeneratorOutputLanguageEnglishName;
        private JSONHoldingStringGenerator fieldGeneratorOutputLanguageNativeName;
        private JSONHoldingStringGenerator fieldGeneratorOutputLanguageIETFTag;
        private JSONHoldingNumberTextGenerator fieldGeneratorResultVersionAccepted;
    private abstract class FieldGeneratorUnitPreference : JSONStringGenerator
          {
            protected FieldGeneratorUnitPreference(string what)
              {
                set_what(what);
              }
            protected FieldGeneratorUnitPreference()
              {
              }
            protected override void handle_result(string result)
              {
                handle_result(stringToUnitPreference(result));
              }
            protected abstract void handle_result(TypeUnitPreference result);
          };
    private class FieldHoldingGeneratorUnitPreference : FieldGeneratorUnitPreference
  {
    protected override void handle_result(TypeUnitPreference result)
      {
//@@@        Debug.Assert(!have_value);
        have_value = true;
        value = result;
      }

    public FieldHoldingGeneratorUnitPreference(String what)
      {
        have_value = false;
        base.set_what(what);
      }

    public override void reset()
      {
        have_value = false;
        base.reset();
      }

    public bool have_value;
    public TypeUnitPreference value;
  };
    private class FieldHoldingArrayGeneratorUnitPreference : JSONArrayGenerator
  {
    protected class ElementHandler : FieldGeneratorUnitPreference
      {
        private FieldHoldingArrayGeneratorUnitPreference top;

        protected override void handle_result(TypeUnitPreference result)
          {
            top.value.Add(result);
          }
        protected override string get_what()
          {
            return "element " + top.value.Count + " of " + top.get_what();
          }

        public ElementHandler(FieldHoldingArrayGeneratorUnitPreference init_top)
          {
            top = init_top;
          }
      };

    private ElementHandler element_handler;

    protected override JSONHandler start()
      {
        have_value = true;
        value.Clear();
        return element_handler;
      }
    protected override void finish()
      {
        Debug.Assert(have_value);
        handle_result(value);
        element_handler.reset();
      }
    protected virtual void handle_result(List<TypeUnitPreference> result)
      {
      }

    public FieldHoldingArrayGeneratorUnitPreference(string what)
      {
        element_handler = new ElementHandler(this);
        have_value = false;
        value = new List<TypeUnitPreference>();
        base.set_what(what);
      }
    public FieldHoldingArrayGeneratorUnitPreference()
      {
        element_handler = new ElementHandler(this);
        have_value = false;
        value = new List<TypeUnitPreference>();
      }

    public override void reset()
      {
        element_handler.reset();
        have_value = false;
        value.Clear();
        base.reset();
      }

    public bool have_value;
    public List<TypeUnitPreference> value;
  };
        private FieldHoldingGeneratorUnitPreference fieldGeneratorUnitPreference;
        private JSONHoldingBooleanGenerator fieldGeneratorDefaultTimeFormat24Hours;
        private JSONHoldingStringGenerator fieldGeneratorClientID;
    private class FieldHoldingGeneratorClientVersion_1 : JSONHoldingIntegerLowerBoundOnlyGenerator
          {
            public FieldHoldingGeneratorClientVersion_1(String what) : base(what, (BigInteger)(0))
              {
              }
          };
    private class FieldHoldingArrayGeneratorClientVersion_1 : JSONHoldingIntegerLowerBoundOnlyArrayGenerator
          {
            public FieldHoldingArrayGeneratorClientVersion_1(String what) : base(what, (BigInteger)(0))
              {
              }
          };
    private abstract class FieldGeneratorClientVersion : JSONParallelGenerator
          {
            private JSONHoldingStringGenerator field0;
            private FieldHoldingGeneratorClientVersion_1 field1;
            private JSONHandler [] all_handlers = new JSONHandler [2];
            protected override JSONHandler[] start()
              {
                return all_handlers;
              }
            protected override void finish(int winning_index)
              {
                TypeClientVersion result = new TypeClientVersion();
                result.key = winning_index;
                switch (winning_index)
                  {
                    case 0:
                      {
                        Debug.Assert(field0.have_value);
                        result.choice0 = field0.value;
                        break;
                      }
                    case 1:
                      {
                        Debug.Assert(field1.have_value);
                        result.choice1 = field1.value;
                        break;
                      }
                    default:
                      {
                        Debug.Assert(false);
                        break;
                      }
                  }
                handle_result(result);
              }
            protected abstract void handle_result(TypeClientVersion result);
            public FieldGeneratorClientVersion(bool ignore_extras)
              {
                    field0 = new JSONHoldingStringGenerator("option 0 of field \"ClientVersion\"");
                    field1 = new FieldHoldingGeneratorClientVersion_1("option 1 of field \"ClientVersion\"");
                all_handlers[0] = field0;
                all_handlers[1] = field1;
              }
            public FieldGeneratorClientVersion(string what, bool ignore_extras)
              {
                    field0 = new JSONHoldingStringGenerator("option 0 of field \"ClientVersion\"");
                    field1 = new FieldHoldingGeneratorClientVersion_1("option 1 of field \"ClientVersion\"");
                all_handlers[0] = field0;
                all_handlers[1] = field1;
              }

            public override void reset()
              {
                field0.reset();
                field1.reset();
                base.reset();
              }
          };
        private class HolderClientVersion
          {
            private bool have_data;
            private TypeClientVersion data;
            public HolderClientVersion()  { have_data = false; }
            public HolderClientVersion(TypeClientVersion init_data)
              {
                have_data = true;
                data = init_data;
                if (have_data)
                  {
                  }
              }
            public HolderClientVersion(HolderClientVersion other)
              {
                have_data = other.haveData();
                data = other.referenced();
                if (have_data)
                  {
                  }
              }

            public bool haveData()
              {
                return have_data;
              }
            public TypeClientVersion referenced()
              {
                return data;
              }
          };
    private class FieldHoldingGeneratorClientVersion : FieldGeneratorClientVersion
          {
            protected override void handle_result(TypeClientVersion result)
              {
        //@@@        Debug.Assert(!have_value);
                have_value = true;
                value = new HolderClientVersion(result);
              }

            public FieldHoldingGeneratorClientVersion(String what, bool ignore_extras) : base(ignore_extras)
              {
                have_value = false;
                base.set_what(what);
              }

            public override void reset()
              {
                have_value = false;
                base.reset();
              }

            public bool have_value;
            public HolderClientVersion value;
          };
    private class FieldHoldingArrayGeneratorClientVersion : JSONArrayGenerator
  {
    protected class ElementHandler : FieldGeneratorClientVersion
      {
        private FieldHoldingArrayGeneratorClientVersion top;

        protected override void handle_result(TypeClientVersion result)
          {
            top.value.Add(result);
          }
        protected override string get_what()
          {
            return "element " + top.value.Count + " of " + top.get_what();
          }

        public ElementHandler(FieldHoldingArrayGeneratorClientVersion init_top, bool ignore_extras) : base(ignore_extras)
          {
            top = init_top;
          }
      };

    private ElementHandler element_handler;

    protected override JSONHandler start()
      {
        have_value = true;
        value.Clear();
        return element_handler;
      }
    protected override void finish()
      {
        Debug.Assert(have_value);
        handle_result(value);
        element_handler.reset();
      }
    protected virtual void handle_result(List<TypeClientVersion> result)
      {
      }

    public FieldHoldingArrayGeneratorClientVersion(string what, bool ignore_extras)
      {
        element_handler = new ElementHandler(this, ignore_extras);
        have_value = false;
        value = new List<TypeClientVersion>();
        base.set_what(what);
      }
    public FieldHoldingArrayGeneratorClientVersion(bool ignore_extras)
      {
        element_handler = new ElementHandler(this, ignore_extras);
        have_value = false;
        value = new List<TypeClientVersion>();
      }

    public override void reset()
      {
        element_handler.reset();
        have_value = false;
        value.Clear();
        base.reset();
      }

    public bool have_value;
    public List<TypeClientVersion> value;
  };
        private FieldHoldingGeneratorClientVersion fieldGeneratorClientVersion;
        private JSONHoldingStringGenerator fieldGeneratorDeviceID;
        private JSONHoldingStringGenerator fieldGeneratorSDK;
        private JSONHoldingObjectValueGenerator fieldGeneratorSDKInfo;
        private JSONHoldingStringGenerator fieldGeneratorFirstPersonSelf;
        private JSONHoldingStringGenerator fieldGeneratorFirstPersonSelfSpoken;
        private JSONHoldingStringArrayGenerator fieldGeneratorSecondPersonSelf;
        private JSONHoldingStringArrayGenerator fieldGeneratorSecondPersonSelfSpoken;
        private JSONHoldingBooleanGenerator fieldGeneratorWakeUpPhraseIncludedInAudio;
        private JSONHoldingNumberTextGenerator fieldGeneratorInitialSecondsOfAudioToIgnore;
        private JSONHoldingStringGenerator fieldGeneratorWakeUpPattern;
        private JSONHoldingStringGenerator fieldGeneratorUserID;
        private JSONHoldingStringGenerator fieldGeneratorRequestID;
        private JSONHoldingStringGenerator fieldGeneratorSessionID;
        private DomainsJSON.HoldingGenerator fieldGeneratorDomains;
        private JSONHoldingBooleanGenerator fieldGeneratorResultUpdateAllowed;
        private JSONHoldingBooleanGenerator fieldGeneratorPartialTranscriptsDesired;
    private class FieldHoldingGeneratorMinResults : JSONHoldingIntegerLowerBoundOnlyGenerator
          {
            public FieldHoldingGeneratorMinResults(String what) : base(what, (BigInteger)(1))
              {
              }
          };
    private class FieldHoldingArrayGeneratorMinResults : JSONHoldingIntegerLowerBoundOnlyArrayGenerator
          {
            public FieldHoldingArrayGeneratorMinResults(String what) : base(what, (BigInteger)(1))
              {
              }
          };
        private FieldHoldingGeneratorMinResults fieldGeneratorMinResults;
    private class FieldHoldingGeneratorMaxResults : JSONHoldingIntegerLowerBoundOnlyGenerator
          {
            public FieldHoldingGeneratorMaxResults(String what) : base(what, (BigInteger)(1))
              {
              }
          };
    private class FieldHoldingArrayGeneratorMaxResults : JSONHoldingIntegerLowerBoundOnlyArrayGenerator
          {
            public FieldHoldingArrayGeneratorMaxResults(String what) : base(what, (BigInteger)(1))
              {
              }
          };
        private FieldHoldingGeneratorMaxResults fieldGeneratorMaxResults;
        private JSONHoldingBooleanGenerator fieldGeneratorObjectByteCountPrefix;
    private abstract class FieldGeneratorProfanityFilter : JSONStringGenerator
          {
            protected FieldGeneratorProfanityFilter(string what)
              {
                set_what(what);
              }
            protected FieldGeneratorProfanityFilter()
              {
              }
            protected override void handle_result(string result)
              {
                handle_result(stringToProfanityFilter(result));
              }
            protected abstract void handle_result(TypeProfanityFilter result);
          };
    private class FieldHoldingGeneratorProfanityFilter : FieldGeneratorProfanityFilter
  {
    protected override void handle_result(TypeProfanityFilter result)
      {
//@@@        Debug.Assert(!have_value);
        have_value = true;
        value = result;
      }

    public FieldHoldingGeneratorProfanityFilter(String what)
      {
        have_value = false;
        base.set_what(what);
      }

    public override void reset()
      {
        have_value = false;
        base.reset();
      }

    public bool have_value;
    public TypeProfanityFilter value;
  };
    private class FieldHoldingArrayGeneratorProfanityFilter : JSONArrayGenerator
  {
    protected class ElementHandler : FieldGeneratorProfanityFilter
      {
        private FieldHoldingArrayGeneratorProfanityFilter top;

        protected override void handle_result(TypeProfanityFilter result)
          {
            top.value.Add(result);
          }
        protected override string get_what()
          {
            return "element " + top.value.Count + " of " + top.get_what();
          }

        public ElementHandler(FieldHoldingArrayGeneratorProfanityFilter init_top)
          {
            top = init_top;
          }
      };

    private ElementHandler element_handler;

    protected override JSONHandler start()
      {
        have_value = true;
        value.Clear();
        return element_handler;
      }
    protected override void finish()
      {
        Debug.Assert(have_value);
        handle_result(value);
        element_handler.reset();
      }
    protected virtual void handle_result(List<TypeProfanityFilter> result)
      {
      }

    public FieldHoldingArrayGeneratorProfanityFilter(string what)
      {
        element_handler = new ElementHandler(this);
        have_value = false;
        value = new List<TypeProfanityFilter>();
        base.set_what(what);
      }
    public FieldHoldingArrayGeneratorProfanityFilter()
      {
        element_handler = new ElementHandler(this);
        have_value = false;
        value = new List<TypeProfanityFilter>();
      }

    public override void reset()
      {
        element_handler.reset();
        have_value = false;
        value.Clear();
        base.reset();
      }

    public bool have_value;
    public List<TypeProfanityFilter> value;
  };
        private FieldHoldingGeneratorProfanityFilter fieldGeneratorProfanityFilter;
        private ClientMatchJSON.HoldingArrayGenerator fieldGeneratorClientMatches;
        private JSONHoldingBooleanGenerator fieldGeneratorClientMatchesOnly;
        private PaginationJSON.HoldingGenerator fieldGeneratorPagination;
        private JSONHoldingStringGenerator fieldGeneratorResponseAudioVoice;
    private abstract class FieldGeneratorResponseAudioShortOrLong : JSONStringGenerator
          {
            protected FieldGeneratorResponseAudioShortOrLong(string what)
              {
                set_what(what);
              }
            protected FieldGeneratorResponseAudioShortOrLong()
              {
              }
            protected override void handle_result(string result)
              {
                handle_result(stringToResponseAudioShortOrLong(result));
              }
            protected abstract void handle_result(TypeResponseAudioShortOrLong result);
          };
    private class FieldHoldingGeneratorResponseAudioShortOrLong : FieldGeneratorResponseAudioShortOrLong
  {
    protected override void handle_result(TypeResponseAudioShortOrLong result)
      {
//@@@        Debug.Assert(!have_value);
        have_value = true;
        value = result;
      }

    public FieldHoldingGeneratorResponseAudioShortOrLong(String what)
      {
        have_value = false;
        base.set_what(what);
      }

    public override void reset()
      {
        have_value = false;
        base.reset();
      }

    public bool have_value;
    public TypeResponseAudioShortOrLong value;
  };
    private class FieldHoldingArrayGeneratorResponseAudioShortOrLong : JSONArrayGenerator
  {
    protected class ElementHandler : FieldGeneratorResponseAudioShortOrLong
      {
        private FieldHoldingArrayGeneratorResponseAudioShortOrLong top;

        protected override void handle_result(TypeResponseAudioShortOrLong result)
          {
            top.value.Add(result);
          }
        protected override string get_what()
          {
            return "element " + top.value.Count + " of " + top.get_what();
          }

        public ElementHandler(FieldHoldingArrayGeneratorResponseAudioShortOrLong init_top)
          {
            top = init_top;
          }
      };

    private ElementHandler element_handler;

    protected override JSONHandler start()
      {
        have_value = true;
        value.Clear();
        return element_handler;
      }
    protected override void finish()
      {
        Debug.Assert(have_value);
        handle_result(value);
        element_handler.reset();
      }
    protected virtual void handle_result(List<TypeResponseAudioShortOrLong> result)
      {
      }

    public FieldHoldingArrayGeneratorResponseAudioShortOrLong(string what)
      {
        element_handler = new ElementHandler(this);
        have_value = false;
        value = new List<TypeResponseAudioShortOrLong>();
        base.set_what(what);
      }
    public FieldHoldingArrayGeneratorResponseAudioShortOrLong()
      {
        element_handler = new ElementHandler(this);
        have_value = false;
        value = new List<TypeResponseAudioShortOrLong>();
      }

    public override void reset()
      {
        element_handler.reset();
        have_value = false;
        value.Clear();
        base.reset();
      }

    public bool have_value;
    public List<TypeResponseAudioShortOrLong> value;
  };
        private FieldHoldingGeneratorResponseAudioShortOrLong fieldGeneratorResponseAudioShortOrLong;
    private abstract class FieldGeneratorResponseAudioAcceptedEncodings : JSONStringGenerator
          {
            protected FieldGeneratorResponseAudioAcceptedEncodings(string what)
              {
                set_what(what);
              }
            protected FieldGeneratorResponseAudioAcceptedEncodings()
              {
              }
            protected override void handle_result(string result)
              {
                TypeResponseAudioAcceptedEncodingsKnownValues known = stringToResponseAudioAcceptedEncodings(result);
                TypeResponseAudioAcceptedEncodings new_value = new TypeResponseAudioAcceptedEncodings();
                if (known == TypeResponseAudioAcceptedEncodingsKnownValues.ResponseAudioAcceptedEncodings__none)
                  {
                    new_value.in_known_list = false;
                    new_value.string_value = result;
                  }
                else
                  {
                    new_value.in_known_list = true;
                    new_value.list_value = known;
                  }
                handle_result(new_value);
              }
            protected abstract void handle_result(TypeResponseAudioAcceptedEncodings result);
          };
    private class FieldHoldingGeneratorResponseAudioAcceptedEncodings : FieldGeneratorResponseAudioAcceptedEncodings
  {
    protected override void handle_result(TypeResponseAudioAcceptedEncodings result)
      {
//@@@        Debug.Assert(!have_value);
        have_value = true;
        value = result;
      }

    public FieldHoldingGeneratorResponseAudioAcceptedEncodings(String what)
      {
        have_value = false;
        base.set_what(what);
      }

    public override void reset()
      {
        have_value = false;
        base.reset();
      }

    public bool have_value;
    public TypeResponseAudioAcceptedEncodings value;
  };
    private class FieldHoldingArrayGeneratorResponseAudioAcceptedEncodings : JSONArrayGenerator
  {
    protected class ElementHandler : FieldGeneratorResponseAudioAcceptedEncodings
      {
        private FieldHoldingArrayGeneratorResponseAudioAcceptedEncodings top;

        protected override void handle_result(TypeResponseAudioAcceptedEncodings result)
          {
            top.value.Add(result);
          }
        protected override string get_what()
          {
            return "element " + top.value.Count + " of " + top.get_what();
          }

        public ElementHandler(FieldHoldingArrayGeneratorResponseAudioAcceptedEncodings init_top)
          {
            top = init_top;
          }
      };

    private ElementHandler element_handler;

    protected override JSONHandler start()
      {
        have_value = true;
        value.Clear();
        return element_handler;
      }
    protected override void finish()
      {
        Debug.Assert(have_value);
        handle_result(value);
        element_handler.reset();
      }
    protected virtual void handle_result(List<TypeResponseAudioAcceptedEncodings> result)
      {
      }

    public FieldHoldingArrayGeneratorResponseAudioAcceptedEncodings(string what)
      {
        element_handler = new ElementHandler(this);
        have_value = false;
        value = new List<TypeResponseAudioAcceptedEncodings>();
        base.set_what(what);
      }
    public FieldHoldingArrayGeneratorResponseAudioAcceptedEncodings()
      {
        element_handler = new ElementHandler(this);
        have_value = false;
        value = new List<TypeResponseAudioAcceptedEncodings>();
      }

    public override void reset()
      {
        element_handler.reset();
        have_value = false;
        value.Clear();
        base.reset();
      }

    public bool have_value;
    public List<TypeResponseAudioAcceptedEncodings> value;
  };
        private FieldHoldingArrayGeneratorResponseAudioAcceptedEncodings fieldGeneratorResponseAudioAcceptedEncodings;
        private JSONHoldingBooleanGenerator fieldGeneratorReturnResponseAudioAsURL;
        private VoiceActivityDetectionJSON.HoldingGenerator fieldGeneratorVoiceActivityDetection;
        private JSONHoldingBooleanGenerator fieldGeneratorServerDeterminesEndOfAudio;
        private JSONHoldingBooleanGenerator fieldGeneratorIntentOnly;
        private JSONHoldingBooleanGenerator fieldGeneratorDisableSpellCorrection;
        private JSONHoldingBooleanGenerator fieldGeneratorUseContactData;
        private JSONHoldingBooleanGenerator fieldGeneratorUseClientTime;
    private class FieldHoldingGeneratorForceConversationStateTime : JSONHoldingIntegerUnboundRangeGenerator
          {
            public FieldHoldingGeneratorForceConversationStateTime(String what) : base(what)
              {
              }
          };
    private class FieldHoldingArrayGeneratorForceConversationStateTime : JSONHoldingIntegerUnboundRangeArrayGenerator
          {
            public FieldHoldingArrayGeneratorForceConversationStateTime(String what) : base(what)
              {
              }
          };
        private FieldHoldingGeneratorForceConversationStateTime fieldGeneratorForceConversationStateTime;
    private class FieldHoldingGeneratorOutputLatticeSize : JSONHoldingIntegerLowerBoundOnlyGenerator
          {
            public FieldHoldingGeneratorOutputLatticeSize(String what) : base(what, (BigInteger)(0))
              {
              }
          };
    private class FieldHoldingArrayGeneratorOutputLatticeSize : JSONHoldingIntegerLowerBoundOnlyArrayGenerator
          {
            public FieldHoldingArrayGeneratorOutputLatticeSize(String what) : base(what, (BigInteger)(0))
              {
              }
          };
        private FieldHoldingGeneratorOutputLatticeSize fieldGeneratorOutputLatticeSize;
        private MatchingMutationsJSON.HoldingGenerator fieldGeneratorMatchingMutations;
        private JSONHoldingBooleanGenerator fieldGeneratorUseFormattedTranscriptionAsDefault;
        private ResponseRankingJSON.HoldingGenerator fieldGeneratorResponseRanking;
        private class UnknownFieldGenerator : JSONValueHandler
          {
            public bool ignore;
            public List<string> field_names;
            public List<JSONValue > field_values;
            public Dictionary<string, JSONValue > index;
            public UnknownFieldGenerator(bool init_ignore)
              {
                ignore = init_ignore;
                field_names = new List<string>();
                field_values = new List<JSONValue >();
            index = new Dictionary<string, JSONValue >();
              }

            protected override void new_value(JSONValue item)
              {
                if (ignore)
                    return;
                Debug.Assert(field_names.Count == (field_values.Count + 1));
                index.Add(field_names[field_values.Count], item);
                field_values.Add(item);
              }
            public override void reset()
              {
                field_names.Clear();
                field_values.Clear();
            index = new Dictionary<string, JSONValue >();
              }
          };
        private UnknownFieldGenerator unknownFieldGenerator;

        protected bool allow_incomplete;

        protected bool allow_unpolished;

        protected override void start()
          {
          }
        protected override JSONHandler start_field(string field_name)
          {
            JSONHandler result = start_known_field(field_name);
            if (result != null)
                return result;
            Debug.Assert(unknownFieldGenerator.field_names.Count ==
                   unknownFieldGenerator.field_values.Count);
            if (unknownFieldGenerator.ignore)
              {
                Debug.Assert(unknownFieldGenerator.field_names.Count == 0);
              }
            else
              {
                unknownFieldGenerator.field_names.Add(field_name);
              }
            return unknownFieldGenerator;
          }
        protected override void finish_field(string field_name, JSONHandler field_handler)
          {
          }
        protected override void finish()
          {
            RequestInfoJSON result = new RequestInfoJSON();
            Debug.Assert(result != null);
            finish(result);
            int extra_count = unknownFieldGenerator.field_names.Count;
            Debug.Assert(extra_count == unknownFieldGenerator.field_values.Count);
            for (int extra_num = 0; extra_num < extra_count; ++extra_num)
              {
                result.extraRequestInfoAppendPair(unknownFieldGenerator.field_names[extra_num], unknownFieldGenerator.field_values[extra_num]);
              }
            unknownFieldGenerator.field_names.Clear();
            unknownFieldGenerator.field_values.Clear();
            unknownFieldGenerator.index = new Dictionary<string, JSONValue >();
            handle_result(result);
          }
        protected void finish(RequestInfoJSON result)
          {
            if (fieldGeneratorLatitude.have_value)
              {
                result.setLatitudeText(fieldGeneratorLatitude.value);
                fieldGeneratorLatitude.have_value = false;
              }
            if (fieldGeneratorLongitude.have_value)
              {
                result.setLongitudeText(fieldGeneratorLongitude.value);
                fieldGeneratorLongitude.have_value = false;
              }
            if (fieldGeneratorPositionTime.have_value)
              {
                result.setPositionTime(fieldGeneratorPositionTime.value);
                fieldGeneratorPositionTime.have_value = false;
              }
            if (fieldGeneratorPositionHorizontalAccuracy.have_value)
              {
                result.setPositionHorizontalAccuracyText(fieldGeneratorPositionHorizontalAccuracy.value);
                fieldGeneratorPositionHorizontalAccuracy.have_value = false;
              }
            if (fieldGeneratorStreet.have_value)
              {
                result.setStreet(fieldGeneratorStreet.value);
                fieldGeneratorStreet.have_value = false;
              }
            if (fieldGeneratorCity.have_value)
              {
                result.setCity(fieldGeneratorCity.value);
                fieldGeneratorCity.have_value = false;
              }
            if (fieldGeneratorState.have_value)
              {
                result.setState(fieldGeneratorState.value);
                fieldGeneratorState.have_value = false;
              }
            if (fieldGeneratorCountry.have_value)
              {
                result.setCountry(fieldGeneratorCountry.value);
                fieldGeneratorCountry.have_value = false;
              }
            if (fieldGeneratorRoutePoints.have_value)
              {
                result.setRoutePoints(fieldGeneratorRoutePoints.value);
                fieldGeneratorRoutePoints.have_value = false;
              }
            if (fieldGeneratorRouteInformation.have_value)
              {
                result.setRouteInformation(fieldGeneratorRouteInformation.value);
                fieldGeneratorRouteInformation.have_value = false;
              }
            if (fieldGeneratorControllableTrackPlaying.have_value)
              {
                result.setControllableTrackPlaying(fieldGeneratorControllableTrackPlaying.value);
                fieldGeneratorControllableTrackPlaying.have_value = false;
              }
            if (fieldGeneratorTimeStamp.have_value)
              {
                result.setTimeStamp(fieldGeneratorTimeStamp.value);
                fieldGeneratorTimeStamp.have_value = false;
              }
            if (fieldGeneratorTimeZone.have_value)
              {
                result.setTimeZone(fieldGeneratorTimeZone.value);
                fieldGeneratorTimeZone.have_value = false;
              }
            if (fieldGeneratorConversationState.have_value)
              {
                result.setConversationState(fieldGeneratorConversationState.value);
                fieldGeneratorConversationState.have_value = false;
              }
            if (fieldGeneratorClientState.have_value)
              {
                result.setClientState(fieldGeneratorClientState.value);
                fieldGeneratorClientState.have_value = false;
              }
            if (fieldGeneratorDeviceInfo.have_value)
              {
                result.setDeviceInfo(fieldGeneratorDeviceInfo.value);
                fieldGeneratorDeviceInfo.have_value = false;
              }
            if (fieldGeneratorSendBack.have_value)
              {
                result.setSendBack(fieldGeneratorSendBack.value);
                fieldGeneratorSendBack.have_value = false;
              }
            if (fieldGeneratorPreferredImageSize.have_value)
              {
                result.initPreferredImageSize();
                int count = fieldGeneratorPreferredImageSize.value.Count;
                for (int num = 0; num < count; ++num)
                  {
                    result.appendPreferredImageSize(fieldGeneratorPreferredImageSize.value[num]);
                  }
                fieldGeneratorPreferredImageSize.value.Clear();
                fieldGeneratorPreferredImageSize.have_value = false;
              }
            if (fieldGeneratorInputLanguageEnglishName.have_value)
              {
                result.setInputLanguageEnglishName(fieldGeneratorInputLanguageEnglishName.value);
                fieldGeneratorInputLanguageEnglishName.have_value = false;
              }
            if (fieldGeneratorInputLanguageNativeName.have_value)
              {
                result.setInputLanguageNativeName(fieldGeneratorInputLanguageNativeName.value);
                fieldGeneratorInputLanguageNativeName.have_value = false;
              }
            if (fieldGeneratorInputLanguageIETFTag.have_value)
              {
                result.setInputLanguageIETFTag(fieldGeneratorInputLanguageIETFTag.value);
                fieldGeneratorInputLanguageIETFTag.have_value = false;
              }
            if (fieldGeneratorOutputLanguageEnglishName.have_value)
              {
                result.setOutputLanguageEnglishName(fieldGeneratorOutputLanguageEnglishName.value);
                fieldGeneratorOutputLanguageEnglishName.have_value = false;
              }
            if (fieldGeneratorOutputLanguageNativeName.have_value)
              {
                result.setOutputLanguageNativeName(fieldGeneratorOutputLanguageNativeName.value);
                fieldGeneratorOutputLanguageNativeName.have_value = false;
              }
            if (fieldGeneratorOutputLanguageIETFTag.have_value)
              {
                result.setOutputLanguageIETFTag(fieldGeneratorOutputLanguageIETFTag.value);
                fieldGeneratorOutputLanguageIETFTag.have_value = false;
              }
            if (fieldGeneratorResultVersionAccepted.have_value)
              {
                result.setResultVersionAcceptedText(fieldGeneratorResultVersionAccepted.value);
                fieldGeneratorResultVersionAccepted.have_value = false;
              }
            if (fieldGeneratorUnitPreference.have_value)
              {
                result.setUnitPreference(fieldGeneratorUnitPreference.value);
                fieldGeneratorUnitPreference.have_value = false;
              }
            if (fieldGeneratorDefaultTimeFormat24Hours.have_value)
              {
                result.setDefaultTimeFormat24Hours(fieldGeneratorDefaultTimeFormat24Hours.value);
                fieldGeneratorDefaultTimeFormat24Hours.have_value = false;
              }
            if (fieldGeneratorClientID.have_value)
              {
                result.setClientID(fieldGeneratorClientID.value);
                fieldGeneratorClientID.have_value = false;
              }
            if (fieldGeneratorClientVersion.have_value)
              {
                result.setClientVersion(fieldGeneratorClientVersion.value.referenced());
                fieldGeneratorClientVersion.have_value = false;
              }
            if (fieldGeneratorDeviceID.have_value)
              {
                result.setDeviceID(fieldGeneratorDeviceID.value);
                fieldGeneratorDeviceID.have_value = false;
              }
            if (fieldGeneratorSDK.have_value)
              {
                result.setSDK(fieldGeneratorSDK.value);
                fieldGeneratorSDK.have_value = false;
              }
            if (fieldGeneratorSDKInfo.have_value)
              {
                result.setSDKInfo(fieldGeneratorSDKInfo.value);
                fieldGeneratorSDKInfo.have_value = false;
              }
            if (fieldGeneratorFirstPersonSelf.have_value)
              {
                result.setFirstPersonSelf(fieldGeneratorFirstPersonSelf.value);
                fieldGeneratorFirstPersonSelf.have_value = false;
              }
            if (fieldGeneratorFirstPersonSelfSpoken.have_value)
              {
                result.setFirstPersonSelfSpoken(fieldGeneratorFirstPersonSelfSpoken.value);
                fieldGeneratorFirstPersonSelfSpoken.have_value = false;
              }
            if (fieldGeneratorSecondPersonSelf.have_value)
              {
                result.initSecondPersonSelf();
                int count = fieldGeneratorSecondPersonSelf.value.Count;
                for (int num = 0; num < count; ++num)
                  {
                    result.appendSecondPersonSelf(fieldGeneratorSecondPersonSelf.value[num]);
                  }
                fieldGeneratorSecondPersonSelf.value.Clear();
                fieldGeneratorSecondPersonSelf.have_value = false;
              }
            if (fieldGeneratorSecondPersonSelfSpoken.have_value)
              {
                result.initSecondPersonSelfSpoken();
                int count = fieldGeneratorSecondPersonSelfSpoken.value.Count;
                for (int num = 0; num < count; ++num)
                  {
                    result.appendSecondPersonSelfSpoken(fieldGeneratorSecondPersonSelfSpoken.value[num]);
                  }
                fieldGeneratorSecondPersonSelfSpoken.value.Clear();
                fieldGeneratorSecondPersonSelfSpoken.have_value = false;
              }
            if (fieldGeneratorWakeUpPhraseIncludedInAudio.have_value)
              {
                result.setWakeUpPhraseIncludedInAudio(fieldGeneratorWakeUpPhraseIncludedInAudio.value);
                fieldGeneratorWakeUpPhraseIncludedInAudio.have_value = false;
              }
            if (fieldGeneratorInitialSecondsOfAudioToIgnore.have_value)
              {
                result.setInitialSecondsOfAudioToIgnoreText(fieldGeneratorInitialSecondsOfAudioToIgnore.value);
                fieldGeneratorInitialSecondsOfAudioToIgnore.have_value = false;
              }
            if (fieldGeneratorWakeUpPattern.have_value)
              {
                result.setWakeUpPattern(fieldGeneratorWakeUpPattern.value);
                fieldGeneratorWakeUpPattern.have_value = false;
              }
            if (fieldGeneratorUserID.have_value)
              {
                result.setUserID(fieldGeneratorUserID.value);
                fieldGeneratorUserID.have_value = false;
              }
            if (fieldGeneratorRequestID.have_value)
              {
                result.setRequestID(fieldGeneratorRequestID.value);
                fieldGeneratorRequestID.have_value = false;
              }
            if (fieldGeneratorSessionID.have_value)
              {
                result.setSessionID(fieldGeneratorSessionID.value);
                fieldGeneratorSessionID.have_value = false;
              }
            if (fieldGeneratorDomains.have_value)
              {
                result.setDomains(fieldGeneratorDomains.value);
                fieldGeneratorDomains.have_value = false;
              }
            if (fieldGeneratorResultUpdateAllowed.have_value)
              {
                result.setResultUpdateAllowed(fieldGeneratorResultUpdateAllowed.value);
                fieldGeneratorResultUpdateAllowed.have_value = false;
              }
            if (fieldGeneratorPartialTranscriptsDesired.have_value)
              {
                result.setPartialTranscriptsDesired(fieldGeneratorPartialTranscriptsDesired.value);
                fieldGeneratorPartialTranscriptsDesired.have_value = false;
              }
            if (fieldGeneratorMinResults.have_value)
              {
                result.setMinResults(fieldGeneratorMinResults.value);
                fieldGeneratorMinResults.have_value = false;
              }
            if (fieldGeneratorMaxResults.have_value)
              {
                result.setMaxResults(fieldGeneratorMaxResults.value);
                fieldGeneratorMaxResults.have_value = false;
              }
            if (fieldGeneratorObjectByteCountPrefix.have_value)
              {
                result.setObjectByteCountPrefix(fieldGeneratorObjectByteCountPrefix.value);
                fieldGeneratorObjectByteCountPrefix.have_value = false;
              }
            if (fieldGeneratorProfanityFilter.have_value)
              {
                result.setProfanityFilter(fieldGeneratorProfanityFilter.value);
                fieldGeneratorProfanityFilter.have_value = false;
              }
            if (fieldGeneratorClientMatches.have_value)
              {
                result.initClientMatches();
                int count = fieldGeneratorClientMatches.value.Count;
                for (int num = 0; num < count; ++num)
                  {
                    result.appendClientMatches(fieldGeneratorClientMatches.value[num]);
                  }
                fieldGeneratorClientMatches.value.Clear();
                fieldGeneratorClientMatches.have_value = false;
              }
            if (fieldGeneratorClientMatchesOnly.have_value)
              {
                result.setClientMatchesOnly(fieldGeneratorClientMatchesOnly.value);
                fieldGeneratorClientMatchesOnly.have_value = false;
              }
            if (fieldGeneratorPagination.have_value)
              {
                result.setPagination(fieldGeneratorPagination.value);
                fieldGeneratorPagination.have_value = false;
              }
            if (fieldGeneratorResponseAudioVoice.have_value)
              {
                result.setResponseAudioVoice(fieldGeneratorResponseAudioVoice.value);
                fieldGeneratorResponseAudioVoice.have_value = false;
              }
            if (fieldGeneratorResponseAudioShortOrLong.have_value)
              {
                result.setResponseAudioShortOrLong(fieldGeneratorResponseAudioShortOrLong.value);
                fieldGeneratorResponseAudioShortOrLong.have_value = false;
              }
            if (fieldGeneratorResponseAudioAcceptedEncodings.have_value)
              {
                result.initResponseAudioAcceptedEncodings();
                int count = fieldGeneratorResponseAudioAcceptedEncodings.value.Count;
                for (int num = 0; num < count; ++num)
                  {
                    result.appendResponseAudioAcceptedEncodings(fieldGeneratorResponseAudioAcceptedEncodings.value[num]);
                  }
                fieldGeneratorResponseAudioAcceptedEncodings.value.Clear();
                fieldGeneratorResponseAudioAcceptedEncodings.have_value = false;
              }
            if (fieldGeneratorReturnResponseAudioAsURL.have_value)
              {
                result.setReturnResponseAudioAsURL(fieldGeneratorReturnResponseAudioAsURL.value);
                fieldGeneratorReturnResponseAudioAsURL.have_value = false;
              }
            if (fieldGeneratorVoiceActivityDetection.have_value)
              {
                result.setVoiceActivityDetection(fieldGeneratorVoiceActivityDetection.value);
                fieldGeneratorVoiceActivityDetection.have_value = false;
              }
            if (fieldGeneratorServerDeterminesEndOfAudio.have_value)
              {
                result.setServerDeterminesEndOfAudio(fieldGeneratorServerDeterminesEndOfAudio.value);
                fieldGeneratorServerDeterminesEndOfAudio.have_value = false;
              }
            if (fieldGeneratorIntentOnly.have_value)
              {
                result.setIntentOnly(fieldGeneratorIntentOnly.value);
                fieldGeneratorIntentOnly.have_value = false;
              }
            if (fieldGeneratorDisableSpellCorrection.have_value)
              {
                result.setDisableSpellCorrection(fieldGeneratorDisableSpellCorrection.value);
                fieldGeneratorDisableSpellCorrection.have_value = false;
              }
            if (fieldGeneratorUseContactData.have_value)
              {
                result.setUseContactData(fieldGeneratorUseContactData.value);
                fieldGeneratorUseContactData.have_value = false;
              }
            if (fieldGeneratorUseClientTime.have_value)
              {
                result.setUseClientTime(fieldGeneratorUseClientTime.value);
                fieldGeneratorUseClientTime.have_value = false;
              }
            if (fieldGeneratorForceConversationStateTime.have_value)
              {
                result.setForceConversationStateTime(fieldGeneratorForceConversationStateTime.value);
                fieldGeneratorForceConversationStateTime.have_value = false;
              }
            if (fieldGeneratorOutputLatticeSize.have_value)
              {
                result.setOutputLatticeSize(fieldGeneratorOutputLatticeSize.value);
                fieldGeneratorOutputLatticeSize.have_value = false;
              }
            if (fieldGeneratorMatchingMutations.have_value)
              {
                result.setMatchingMutations(fieldGeneratorMatchingMutations.value);
                fieldGeneratorMatchingMutations.have_value = false;
              }
            if (fieldGeneratorUseFormattedTranscriptionAsDefault.have_value)
              {
                result.setUseFormattedTranscriptionAsDefault(fieldGeneratorUseFormattedTranscriptionAsDefault.value);
                fieldGeneratorUseFormattedTranscriptionAsDefault.have_value = false;
              }
            if (fieldGeneratorResponseRanking.have_value)
              {
                result.setResponseRanking(fieldGeneratorResponseRanking.value);
                fieldGeneratorResponseRanking.have_value = false;
              }
          }
        protected abstract void handle_result(RequestInfoJSON new_result);
        protected virtual JSONHandler start_known_field(string field_name)
          {
            switch (field_name[0])
              {
                case 'C':
                    switch (field_name[1])
                      {
                        case 'i':
                            if ((String.Compare(field_name, 2, "ty", 0, 2, false) == 0) && (field_name.Length == 4))
                                return fieldGeneratorCity;
                            break;
                        case 'l':
                            if (String.Compare(field_name, 2, "ient", 0, 4, false) == 0)
                              {
                                switch (field_name[6])
                                  {
                                    case 'I':
                                        if ((String.Compare(field_name, 7, "D", 0, 1, false) == 0) && (field_name.Length == 8))
                                            return fieldGeneratorClientID;
                                        break;
                                    case 'M':
                                        if (String.Compare(field_name, 7, "atches", 0, 6, false) == 0)
                                          {
                                            if (field_name.Length == 13)
                                              {
                                                return fieldGeneratorClientMatches;
                                              }
                                            switch (field_name[13])
                                              {
                                                case 'O':
                                                    if ((String.Compare(field_name, 14, "nly", 0, 3, false) == 0) && (field_name.Length == 17))
                                                        return fieldGeneratorClientMatchesOnly;
                                                    break;
                                                default:
                                                    break;
                                              }
                                          }
                                        break;
                                    case 'S':
                                        if ((String.Compare(field_name, 7, "tate", 0, 4, false) == 0) && (field_name.Length == 11))
                                            return fieldGeneratorClientState;
                                        break;
                                    case 'V':
                                        if ((String.Compare(field_name, 7, "ersion", 0, 6, false) == 0) && (field_name.Length == 13))
                                            return fieldGeneratorClientVersion;
                                        break;
                                    default:
                                        break;
                                  }
                              }
                            break;
                        case 'o':
                            switch (field_name[2])
                              {
                                case 'n':
                                    switch (field_name[3])
                                      {
                                        case 't':
                                            if ((String.Compare(field_name, 4, "rollableTrackPlaying", 0, 20, false) == 0) && (field_name.Length == 24))
                                                return fieldGeneratorControllableTrackPlaying;
                                            break;
                                        case 'v':
                                            if ((String.Compare(field_name, 4, "ersationState", 0, 13, false) == 0) && (field_name.Length == 17))
                                                return fieldGeneratorConversationState;
                                            break;
                                        default:
                                            break;
                                      }
                                    break;
                                case 'u':
                                    if ((String.Compare(field_name, 3, "ntry", 0, 4, false) == 0) && (field_name.Length == 7))
                                        return fieldGeneratorCountry;
                                    break;
                                default:
                                    break;
                              }
                            break;
                        default:
                            break;
                      }
                    break;
                case 'D':
                    switch (field_name[1])
                      {
                        case 'e':
                            switch (field_name[2])
                              {
                                case 'f':
                                    if ((String.Compare(field_name, 3, "aultTimeFormat24Hours", 0, 21, false) == 0) && (field_name.Length == 24))
                                        return fieldGeneratorDefaultTimeFormat24Hours;
                                    break;
                                case 'v':
                                    if (String.Compare(field_name, 3, "iceI", 0, 4, false) == 0)
                                      {
                                        switch (field_name[7])
                                          {
                                            case 'D':
                                                if (field_name.Length == 8)
                                                    return fieldGeneratorDeviceID;
                                                break;
                                            case 'n':
                                                if ((String.Compare(field_name, 8, "fo", 0, 2, false) == 0) && (field_name.Length == 10))
                                                    return fieldGeneratorDeviceInfo;
                                                break;
                                            default:
                                                break;
                                          }
                                      }
                                    break;
                                default:
                                    break;
                              }
                            break;
                        case 'i':
                            if ((String.Compare(field_name, 2, "sableSpellCorrection", 0, 20, false) == 0) && (field_name.Length == 22))
                                return fieldGeneratorDisableSpellCorrection;
                            break;
                        case 'o':
                            if ((String.Compare(field_name, 2, "mains", 0, 5, false) == 0) && (field_name.Length == 7))
                                return fieldGeneratorDomains;
                            break;
                        default:
                            break;
                      }
                    break;
                case 'F':
                    switch (field_name[1])
                      {
                        case 'i':
                            if (String.Compare(field_name, 2, "rstPersonSelf", 0, 13, false) == 0)
                              {
                                if (field_name.Length == 15)
                                  {
                                    return fieldGeneratorFirstPersonSelf;
                                  }
                                switch (field_name[15])
                                  {
                                    case 'S':
                                        if ((String.Compare(field_name, 16, "poken", 0, 5, false) == 0) && (field_name.Length == 21))
                                            return fieldGeneratorFirstPersonSelfSpoken;
                                        break;
                                    default:
                                        break;
                                  }
                              }
                            break;
                        case 'o':
                            if ((String.Compare(field_name, 2, "rceConversationStateTime", 0, 24, false) == 0) && (field_name.Length == 26))
                                return fieldGeneratorForceConversationStateTime;
                            break;
                        default:
                            break;
                      }
                    break;
                case 'I':
                    if (String.Compare(field_name, 1, "n", 0, 1, false) == 0)
                      {
                        switch (field_name[2])
                          {
                            case 'i':
                                if ((String.Compare(field_name, 3, "tialSecondsOfAudioToIgnore", 0, 26, false) == 0) && (field_name.Length == 29))
                                    return fieldGeneratorInitialSecondsOfAudioToIgnore;
                                break;
                            case 'p':
                                if (String.Compare(field_name, 3, "utLanguage", 0, 10, false) == 0)
                                  {
                                    switch (field_name[13])
                                      {
                                        case 'E':
                                            if ((String.Compare(field_name, 14, "nglishName", 0, 10, false) == 0) && (field_name.Length == 24))
                                                return fieldGeneratorInputLanguageEnglishName;
                                            break;
                                        case 'I':
                                            if ((String.Compare(field_name, 14, "ETFTag", 0, 6, false) == 0) && (field_name.Length == 20))
                                                return fieldGeneratorInputLanguageIETFTag;
                                            break;
                                        case 'N':
                                            if ((String.Compare(field_name, 14, "ativeName", 0, 9, false) == 0) && (field_name.Length == 23))
                                                return fieldGeneratorInputLanguageNativeName;
                                            break;
                                        default:
                                            break;
                                      }
                                  }
                                break;
                            case 't':
                                if ((String.Compare(field_name, 3, "entOnly", 0, 7, false) == 0) && (field_name.Length == 10))
                                    return fieldGeneratorIntentOnly;
                                break;
                            default:
                                break;
                          }
                      }
                    break;
                case 'L':
                    switch (field_name[1])
                      {
                        case 'a':
                            if ((String.Compare(field_name, 2, "titude", 0, 6, false) == 0) && (field_name.Length == 8))
                                return fieldGeneratorLatitude;
                            break;
                        case 'o':
                            if ((String.Compare(field_name, 2, "ngitude", 0, 7, false) == 0) && (field_name.Length == 9))
                                return fieldGeneratorLongitude;
                            break;
                        default:
                            break;
                      }
                    break;
                case 'M':
                    switch (field_name[1])
                      {
                        case 'a':
                            switch (field_name[2])
                              {
                                case 't':
                                    if ((String.Compare(field_name, 3, "chingMutations", 0, 14, false) == 0) && (field_name.Length == 17))
                                        return fieldGeneratorMatchingMutations;
                                    break;
                                case 'x':
                                    if ((String.Compare(field_name, 3, "Results", 0, 7, false) == 0) && (field_name.Length == 10))
                                        return fieldGeneratorMaxResults;
                                    break;
                                default:
                                    break;
                              }
                            break;
                        case 'i':
                            if ((String.Compare(field_name, 2, "nResults", 0, 8, false) == 0) && (field_name.Length == 10))
                                return fieldGeneratorMinResults;
                            break;
                        default:
                            break;
                      }
                    break;
                case 'O':
                    switch (field_name[1])
                      {
                        case 'b':
                            if ((String.Compare(field_name, 2, "jectByteCountPrefix", 0, 19, false) == 0) && (field_name.Length == 21))
                                return fieldGeneratorObjectByteCountPrefix;
                            break;
                        case 'u':
                            if (String.Compare(field_name, 2, "tputLa", 0, 6, false) == 0)
                              {
                                switch (field_name[8])
                                  {
                                    case 'n':
                                        if (String.Compare(field_name, 9, "guage", 0, 5, false) == 0)
                                          {
                                            switch (field_name[14])
                                              {
                                                case 'E':
                                                    if ((String.Compare(field_name, 15, "nglishName", 0, 10, false) == 0) && (field_name.Length == 25))
                                                        return fieldGeneratorOutputLanguageEnglishName;
                                                    break;
                                                case 'I':
                                                    if ((String.Compare(field_name, 15, "ETFTag", 0, 6, false) == 0) && (field_name.Length == 21))
                                                        return fieldGeneratorOutputLanguageIETFTag;
                                                    break;
                                                case 'N':
                                                    if ((String.Compare(field_name, 15, "ativeName", 0, 9, false) == 0) && (field_name.Length == 24))
                                                        return fieldGeneratorOutputLanguageNativeName;
                                                    break;
                                                default:
                                                    break;
                                              }
                                          }
                                        break;
                                    case 't':
                                        if ((String.Compare(field_name, 9, "ticeSize", 0, 8, false) == 0) && (field_name.Length == 17))
                                            return fieldGeneratorOutputLatticeSize;
                                        break;
                                    default:
                                        break;
                                  }
                              }
                            break;
                        default:
                            break;
                      }
                    break;
                case 'P':
                    switch (field_name[1])
                      {
                        case 'a':
                            switch (field_name[2])
                              {
                                case 'g':
                                    if ((String.Compare(field_name, 3, "ination", 0, 7, false) == 0) && (field_name.Length == 10))
                                        return fieldGeneratorPagination;
                                    break;
                                case 'r':
                                    if ((String.Compare(field_name, 3, "tialTranscriptsDesired", 0, 22, false) == 0) && (field_name.Length == 25))
                                        return fieldGeneratorPartialTranscriptsDesired;
                                    break;
                                default:
                                    break;
                              }
                            break;
                        case 'o':
                            if (String.Compare(field_name, 2, "sition", 0, 6, false) == 0)
                              {
                                switch (field_name[8])
                                  {
                                    case 'H':
                                        if ((String.Compare(field_name, 9, "orizontalAccuracy", 0, 17, false) == 0) && (field_name.Length == 26))
                                            return fieldGeneratorPositionHorizontalAccuracy;
                                        break;
                                    case 'T':
                                        if ((String.Compare(field_name, 9, "ime", 0, 3, false) == 0) && (field_name.Length == 12))
                                            return fieldGeneratorPositionTime;
                                        break;
                                    default:
                                        break;
                                  }
                              }
                            break;
                        case 'r':
                            switch (field_name[2])
                              {
                                case 'e':
                                    if ((String.Compare(field_name, 3, "ferredImageSize", 0, 15, false) == 0) && (field_name.Length == 18))
                                        return fieldGeneratorPreferredImageSize;
                                    break;
                                case 'o':
                                    if ((String.Compare(field_name, 3, "fanityFilter", 0, 12, false) == 0) && (field_name.Length == 15))
                                        return fieldGeneratorProfanityFilter;
                                    break;
                                default:
                                    break;
                              }
                            break;
                        default:
                            break;
                      }
                    break;
                case 'R':
                    switch (field_name[1])
                      {
                        case 'e':
                            switch (field_name[2])
                              {
                                case 'q':
                                    if ((String.Compare(field_name, 3, "uestID", 0, 6, false) == 0) && (field_name.Length == 9))
                                        return fieldGeneratorRequestID;
                                    break;
                                case 's':
                                    switch (field_name[3])
                                      {
                                        case 'p':
                                            if (String.Compare(field_name, 4, "onse", 0, 4, false) == 0)
                                              {
                                                switch (field_name[8])
                                                  {
                                                    case 'A':
                                                        if (String.Compare(field_name, 9, "udio", 0, 4, false) == 0)
                                                          {
                                                            switch (field_name[13])
                                                              {
                                                                case 'A':
                                                                    if ((String.Compare(field_name, 14, "cceptedEncodings", 0, 16, false) == 0) && (field_name.Length == 30))
                                                                        return fieldGeneratorResponseAudioAcceptedEncodings;
                                                                    break;
                                                                case 'S':
                                                                    if ((String.Compare(field_name, 14, "hortOrLong", 0, 10, false) == 0) && (field_name.Length == 24))
                                                                        return fieldGeneratorResponseAudioShortOrLong;
                                                                    break;
                                                                case 'V':
                                                                    if ((String.Compare(field_name, 14, "oice", 0, 4, false) == 0) && (field_name.Length == 18))
                                                                        return fieldGeneratorResponseAudioVoice;
                                                                    break;
                                                                default:
                                                                    break;
                                                              }
                                                          }
                                                        break;
                                                    case 'R':
                                                        if ((String.Compare(field_name, 9, "anking", 0, 6, false) == 0) && (field_name.Length == 15))
                                                            return fieldGeneratorResponseRanking;
                                                        break;
                                                    default:
                                                        break;
                                                  }
                                              }
                                            break;
                                        case 'u':
                                            if (String.Compare(field_name, 4, "lt", 0, 2, false) == 0)
                                              {
                                                switch (field_name[6])
                                                  {
                                                    case 'U':
                                                        if ((String.Compare(field_name, 7, "pdateAllowed", 0, 12, false) == 0) && (field_name.Length == 19))
                                                            return fieldGeneratorResultUpdateAllowed;
                                                        break;
                                                    case 'V':
                                                        if ((String.Compare(field_name, 7, "ersionAccepted", 0, 14, false) == 0) && (field_name.Length == 21))
                                                            return fieldGeneratorResultVersionAccepted;
                                                        break;
                                                    default:
                                                        break;
                                                  }
                                              }
                                            break;
                                        default:
                                            break;
                                      }
                                    break;
                                case 't':
                                    if ((String.Compare(field_name, 3, "urnResponseAudioAsURL", 0, 21, false) == 0) && (field_name.Length == 24))
                                        return fieldGeneratorReturnResponseAudioAsURL;
                                    break;
                                default:
                                    break;
                              }
                            break;
                        case 'o':
                            if (String.Compare(field_name, 2, "ute", 0, 3, false) == 0)
                              {
                                switch (field_name[5])
                                  {
                                    case 'I':
                                        if ((String.Compare(field_name, 6, "nformation", 0, 10, false) == 0) && (field_name.Length == 16))
                                            return fieldGeneratorRouteInformation;
                                        break;
                                    case 'P':
                                        if ((String.Compare(field_name, 6, "oints", 0, 5, false) == 0) && (field_name.Length == 11))
                                            return fieldGeneratorRoutePoints;
                                        break;
                                    default:
                                        break;
                                  }
                              }
                            break;
                        default:
                            break;
                      }
                    break;
                case 'S':
                    switch (field_name[1])
                      {
                        case 'D':
                            if (String.Compare(field_name, 2, "K", 0, 1, false) == 0)
                              {
                                if (field_name.Length == 3)
                                  {
                                    return fieldGeneratorSDK;
                                  }
                                switch (field_name[3])
                                  {
                                    case 'I':
                                        if ((String.Compare(field_name, 4, "nfo", 0, 3, false) == 0) && (field_name.Length == 7))
                                            return fieldGeneratorSDKInfo;
                                        break;
                                    default:
                                        break;
                                  }
                              }
                            break;
                        case 'e':
                            switch (field_name[2])
                              {
                                case 'c':
                                    if (String.Compare(field_name, 3, "ondPersonSelf", 0, 13, false) == 0)
                                      {
                                        if (field_name.Length == 16)
                                          {
                                            return fieldGeneratorSecondPersonSelf;
                                          }
                                        switch (field_name[16])
                                          {
                                            case 'S':
                                                if ((String.Compare(field_name, 17, "poken", 0, 5, false) == 0) && (field_name.Length == 22))
                                                    return fieldGeneratorSecondPersonSelfSpoken;
                                                break;
                                            default:
                                                break;
                                          }
                                      }
                                    break;
                                case 'n':
                                    if ((String.Compare(field_name, 3, "dBack", 0, 5, false) == 0) && (field_name.Length == 8))
                                        return fieldGeneratorSendBack;
                                    break;
                                case 'r':
                                    if ((String.Compare(field_name, 3, "verDeterminesEndOfAudio", 0, 23, false) == 0) && (field_name.Length == 26))
                                        return fieldGeneratorServerDeterminesEndOfAudio;
                                    break;
                                case 's':
                                    if ((String.Compare(field_name, 3, "sionID", 0, 6, false) == 0) && (field_name.Length == 9))
                                        return fieldGeneratorSessionID;
                                    break;
                                default:
                                    break;
                              }
                            break;
                        case 't':
                            switch (field_name[2])
                              {
                                case 'a':
                                    if ((String.Compare(field_name, 3, "te", 0, 2, false) == 0) && (field_name.Length == 5))
                                        return fieldGeneratorState;
                                    break;
                                case 'r':
                                    if ((String.Compare(field_name, 3, "eet", 0, 3, false) == 0) && (field_name.Length == 6))
                                        return fieldGeneratorStreet;
                                    break;
                                default:
                                    break;
                              }
                            break;
                        default:
                            break;
                      }
                    break;
                case 'T':
                    if (String.Compare(field_name, 1, "ime", 0, 3, false) == 0)
                      {
                        switch (field_name[4])
                          {
                            case 'S':
                                if ((String.Compare(field_name, 5, "tamp", 0, 4, false) == 0) && (field_name.Length == 9))
                                    return fieldGeneratorTimeStamp;
                                break;
                            case 'Z':
                                if ((String.Compare(field_name, 5, "one", 0, 3, false) == 0) && (field_name.Length == 8))
                                    return fieldGeneratorTimeZone;
                                break;
                            default:
                                break;
                          }
                      }
                    break;
                case 'U':
                    switch (field_name[1])
                      {
                        case 'n':
                            if ((String.Compare(field_name, 2, "itPreference", 0, 12, false) == 0) && (field_name.Length == 14))
                                return fieldGeneratorUnitPreference;
                            break;
                        case 's':
                            if (String.Compare(field_name, 2, "e", 0, 1, false) == 0)
                              {
                                switch (field_name[3])
                                  {
                                    case 'C':
                                        switch (field_name[4])
                                          {
                                            case 'l':
                                                if ((String.Compare(field_name, 5, "ientTime", 0, 8, false) == 0) && (field_name.Length == 13))
                                                    return fieldGeneratorUseClientTime;
                                                break;
                                            case 'o':
                                                if ((String.Compare(field_name, 5, "ntactData", 0, 9, false) == 0) && (field_name.Length == 14))
                                                    return fieldGeneratorUseContactData;
                                                break;
                                            default:
                                                break;
                                          }
                                        break;
                                    case 'F':
                                        if ((String.Compare(field_name, 4, "ormattedTranscriptionAsDefault", 0, 30, false) == 0) && (field_name.Length == 34))
                                            return fieldGeneratorUseFormattedTranscriptionAsDefault;
                                        break;
                                    case 'r':
                                        if ((String.Compare(field_name, 4, "ID", 0, 2, false) == 0) && (field_name.Length == 6))
                                            return fieldGeneratorUserID;
                                        break;
                                    default:
                                        break;
                                  }
                              }
                            break;
                        default:
                            break;
                      }
                    break;
                case 'V':
                    if ((String.Compare(field_name, 1, "oiceActivityDetection", 0, 21, false) == 0) && (field_name.Length == 22))
                        return fieldGeneratorVoiceActivityDetection;
                    break;
                case 'W':
                    if (String.Compare(field_name, 1, "akeUpP", 0, 6, false) == 0)
                      {
                        switch (field_name[7])
                          {
                            case 'a':
                                if ((String.Compare(field_name, 8, "ttern", 0, 5, false) == 0) && (field_name.Length == 13))
                                    return fieldGeneratorWakeUpPattern;
                                break;
                            case 'h':
                                if ((String.Compare(field_name, 8, "raseIncludedInAudio", 0, 19, false) == 0) && (field_name.Length == 27))
                                    return fieldGeneratorWakeUpPhraseIncludedInAudio;
                                break;
                            default:
                                break;
                          }
                      }
                    break;
                default:
                    break;
              }
            return null;
          }
        public Generator(bool ignore_extras)
          {
            fieldGeneratorLatitude = new JSONHoldingNumberTextGenerator("field \"Latitude\" of the RequestInfo class");
            fieldGeneratorLongitude = new JSONHoldingNumberTextGenerator("field \"Longitude\" of the RequestInfo class");
            fieldGeneratorPositionTime = new FieldHoldingGeneratorPositionTime("field \"PositionTime\" of the RequestInfo class");
            fieldGeneratorPositionHorizontalAccuracy = new JSONHoldingNumberTextGenerator("field \"PositionHorizontalAccuracy\" of the RequestInfo class");
            fieldGeneratorStreet = new JSONHoldingStringGenerator("field \"Street\" of the RequestInfo class");
            fieldGeneratorCity = new JSONHoldingStringGenerator("field \"City\" of the RequestInfo class");
            fieldGeneratorState = new JSONHoldingStringGenerator("field \"State\" of the RequestInfo class");
            fieldGeneratorCountry = new JSONHoldingStringGenerator("field \"Country\" of the RequestInfo class");
            fieldGeneratorRoutePoints = new RoutePointsJSON.HoldingGenerator("field \"RoutePoints\" of the RequestInfo class", ignore_extras);
            fieldGeneratorRouteInformation = new ClientRouteInformationJSON.HoldingGenerator("field \"RouteInformation\" of the RequestInfo class", ignore_extras);
            fieldGeneratorControllableTrackPlaying = new JSONHoldingBooleanGenerator("field \"ControllableTrackPlaying\" of the RequestInfo class");
            fieldGeneratorTimeStamp = new FieldHoldingGeneratorTimeStamp("field \"TimeStamp\" of the RequestInfo class");
            fieldGeneratorTimeZone = new JSONHoldingStringGenerator("field \"TimeZone\" of the RequestInfo class");
            fieldGeneratorConversationState = new ConversationStateJSON.HoldingGenerator("field \"ConversationState\" of the RequestInfo class", ignore_extras);
            fieldGeneratorClientState = new ClientStateJSON.HoldingGenerator("field \"ClientState\" of the RequestInfo class", ignore_extras);
            fieldGeneratorDeviceInfo = new TypeDeviceInfoJSON.HoldingGenerator("field \"DeviceInfo\" of the RequestInfo class", ignore_extras);
            fieldGeneratorSendBack = new JSONHoldingValueGenerator("field \"SendBack\" of the RequestInfo class");
            fieldGeneratorPreferredImageSize = new FieldHoldingArrayGeneratorPreferredImageSize("field \"PreferredImageSize\" of the RequestInfo class");
            fieldGeneratorInputLanguageEnglishName = new JSONHoldingStringGenerator("field \"InputLanguageEnglishName\" of the RequestInfo class");
            fieldGeneratorInputLanguageNativeName = new JSONHoldingStringGenerator("field \"InputLanguageNativeName\" of the RequestInfo class");
            fieldGeneratorInputLanguageIETFTag = new JSONHoldingStringGenerator("field \"InputLanguageIETFTag\" of the RequestInfo class");
            fieldGeneratorOutputLanguageEnglishName = new JSONHoldingStringGenerator("field \"OutputLanguageEnglishName\" of the RequestInfo class");
            fieldGeneratorOutputLanguageNativeName = new JSONHoldingStringGenerator("field \"OutputLanguageNativeName\" of the RequestInfo class");
            fieldGeneratorOutputLanguageIETFTag = new JSONHoldingStringGenerator("field \"OutputLanguageIETFTag\" of the RequestInfo class");
            fieldGeneratorResultVersionAccepted = new JSONHoldingNumberTextGenerator("field \"ResultVersionAccepted\" of the RequestInfo class");
            fieldGeneratorUnitPreference = new FieldHoldingGeneratorUnitPreference("field \"UnitPreference\" of the RequestInfo class");
            fieldGeneratorDefaultTimeFormat24Hours = new JSONHoldingBooleanGenerator("field \"DefaultTimeFormat24Hours\" of the RequestInfo class");
            fieldGeneratorClientID = new JSONHoldingStringGenerator("field \"ClientID\" of the RequestInfo class");
            fieldGeneratorClientVersion = new FieldHoldingGeneratorClientVersion("field \"ClientVersion\" of the RequestInfo class", ignore_extras);
            fieldGeneratorDeviceID = new JSONHoldingStringGenerator("field \"DeviceID\" of the RequestInfo class");
            fieldGeneratorSDK = new JSONHoldingStringGenerator("field \"SDK\" of the RequestInfo class");
            fieldGeneratorSDKInfo = new JSONHoldingObjectValueGenerator("field \"SDKInfo\" of the RequestInfo class");
            fieldGeneratorFirstPersonSelf = new JSONHoldingStringGenerator("field \"FirstPersonSelf\" of the RequestInfo class");
            fieldGeneratorFirstPersonSelfSpoken = new JSONHoldingStringGenerator("field \"FirstPersonSelfSpoken\" of the RequestInfo class");
            fieldGeneratorSecondPersonSelf = new JSONHoldingStringArrayGenerator("field \"SecondPersonSelf\" of the RequestInfo class");
            fieldGeneratorSecondPersonSelfSpoken = new JSONHoldingStringArrayGenerator("field \"SecondPersonSelfSpoken\" of the RequestInfo class");
            fieldGeneratorWakeUpPhraseIncludedInAudio = new JSONHoldingBooleanGenerator("field \"WakeUpPhraseIncludedInAudio\" of the RequestInfo class");
            fieldGeneratorInitialSecondsOfAudioToIgnore = new JSONHoldingNumberTextGenerator("field \"InitialSecondsOfAudioToIgnore\" of the RequestInfo class");
            fieldGeneratorWakeUpPattern = new JSONHoldingStringGenerator("field \"WakeUpPattern\" of the RequestInfo class");
            fieldGeneratorUserID = new JSONHoldingStringGenerator("field \"UserID\" of the RequestInfo class");
            fieldGeneratorRequestID = new JSONHoldingStringGenerator("field \"RequestID\" of the RequestInfo class");
            fieldGeneratorSessionID = new JSONHoldingStringGenerator("field \"SessionID\" of the RequestInfo class");
            fieldGeneratorDomains = new DomainsJSON.HoldingGenerator("field \"Domains\" of the RequestInfo class", ignore_extras);
            fieldGeneratorResultUpdateAllowed = new JSONHoldingBooleanGenerator("field \"ResultUpdateAllowed\" of the RequestInfo class");
            fieldGeneratorPartialTranscriptsDesired = new JSONHoldingBooleanGenerator("field \"PartialTranscriptsDesired\" of the RequestInfo class");
            fieldGeneratorMinResults = new FieldHoldingGeneratorMinResults("field \"MinResults\" of the RequestInfo class");
            fieldGeneratorMaxResults = new FieldHoldingGeneratorMaxResults("field \"MaxResults\" of the RequestInfo class");
            fieldGeneratorObjectByteCountPrefix = new JSONHoldingBooleanGenerator("field \"ObjectByteCountPrefix\" of the RequestInfo class");
            fieldGeneratorProfanityFilter = new FieldHoldingGeneratorProfanityFilter("field \"ProfanityFilter\" of the RequestInfo class");
            fieldGeneratorClientMatches = new ClientMatchJSON.HoldingArrayGenerator("field \"ClientMatches\" of the RequestInfo class", ignore_extras);
            fieldGeneratorClientMatchesOnly = new JSONHoldingBooleanGenerator("field \"ClientMatchesOnly\" of the RequestInfo class");
            fieldGeneratorPagination = new PaginationJSON.HoldingGenerator("field \"Pagination\" of the RequestInfo class", ignore_extras);
            fieldGeneratorResponseAudioVoice = new JSONHoldingStringGenerator("field \"ResponseAudioVoice\" of the RequestInfo class");
            fieldGeneratorResponseAudioShortOrLong = new FieldHoldingGeneratorResponseAudioShortOrLong("field \"ResponseAudioShortOrLong\" of the RequestInfo class");
            fieldGeneratorResponseAudioAcceptedEncodings = new FieldHoldingArrayGeneratorResponseAudioAcceptedEncodings("field \"ResponseAudioAcceptedEncodings\" of the RequestInfo class");
            fieldGeneratorReturnResponseAudioAsURL = new JSONHoldingBooleanGenerator("field \"ReturnResponseAudioAsURL\" of the RequestInfo class");
            fieldGeneratorVoiceActivityDetection = new VoiceActivityDetectionJSON.HoldingGenerator("field \"VoiceActivityDetection\" of the RequestInfo class", ignore_extras);
            fieldGeneratorServerDeterminesEndOfAudio = new JSONHoldingBooleanGenerator("field \"ServerDeterminesEndOfAudio\" of the RequestInfo class");
            fieldGeneratorIntentOnly = new JSONHoldingBooleanGenerator("field \"IntentOnly\" of the RequestInfo class");
            fieldGeneratorDisableSpellCorrection = new JSONHoldingBooleanGenerator("field \"DisableSpellCorrection\" of the RequestInfo class");
            fieldGeneratorUseContactData = new JSONHoldingBooleanGenerator("field \"UseContactData\" of the RequestInfo class");
            fieldGeneratorUseClientTime = new JSONHoldingBooleanGenerator("field \"UseClientTime\" of the RequestInfo class");
            fieldGeneratorForceConversationStateTime = new FieldHoldingGeneratorForceConversationStateTime("field \"ForceConversationStateTime\" of the RequestInfo class");
            fieldGeneratorOutputLatticeSize = new FieldHoldingGeneratorOutputLatticeSize("field \"OutputLatticeSize\" of the RequestInfo class");
            fieldGeneratorMatchingMutations = new MatchingMutationsJSON.HoldingGenerator("field \"MatchingMutations\" of the RequestInfo class", ignore_extras);
            fieldGeneratorUseFormattedTranscriptionAsDefault = new JSONHoldingBooleanGenerator("field \"UseFormattedTranscriptionAsDefault\" of the RequestInfo class");
            fieldGeneratorResponseRanking = new ResponseRankingJSON.HoldingGenerator("field \"ResponseRanking\" of the RequestInfo class", ignore_extras);
            unknownFieldGenerator = new UnknownFieldGenerator(ignore_extras);
            set_what("the RequestInfo class");
            allow_incomplete = false;
            allow_unpolished = false;
          }
        public Generator()
          {
            fieldGeneratorLatitude = new JSONHoldingNumberTextGenerator("field \"Latitude\" of the RequestInfo class");
            fieldGeneratorLongitude = new JSONHoldingNumberTextGenerator("field \"Longitude\" of the RequestInfo class");
            fieldGeneratorPositionTime = new FieldHoldingGeneratorPositionTime("field \"PositionTime\" of the RequestInfo class");
            fieldGeneratorPositionHorizontalAccuracy = new JSONHoldingNumberTextGenerator("field \"PositionHorizontalAccuracy\" of the RequestInfo class");
            fieldGeneratorStreet = new JSONHoldingStringGenerator("field \"Street\" of the RequestInfo class");
            fieldGeneratorCity = new JSONHoldingStringGenerator("field \"City\" of the RequestInfo class");
            fieldGeneratorState = new JSONHoldingStringGenerator("field \"State\" of the RequestInfo class");
            fieldGeneratorCountry = new JSONHoldingStringGenerator("field \"Country\" of the RequestInfo class");
            fieldGeneratorRoutePoints = new RoutePointsJSON.HoldingGenerator("field \"RoutePoints\" of the RequestInfo class", false);
            fieldGeneratorRouteInformation = new ClientRouteInformationJSON.HoldingGenerator("field \"RouteInformation\" of the RequestInfo class", false);
            fieldGeneratorControllableTrackPlaying = new JSONHoldingBooleanGenerator("field \"ControllableTrackPlaying\" of the RequestInfo class");
            fieldGeneratorTimeStamp = new FieldHoldingGeneratorTimeStamp("field \"TimeStamp\" of the RequestInfo class");
            fieldGeneratorTimeZone = new JSONHoldingStringGenerator("field \"TimeZone\" of the RequestInfo class");
            fieldGeneratorConversationState = new ConversationStateJSON.HoldingGenerator("field \"ConversationState\" of the RequestInfo class", false);
            fieldGeneratorClientState = new ClientStateJSON.HoldingGenerator("field \"ClientState\" of the RequestInfo class", false);
            fieldGeneratorDeviceInfo = new TypeDeviceInfoJSON.HoldingGenerator("field \"DeviceInfo\" of the RequestInfo class", false);
            fieldGeneratorSendBack = new JSONHoldingValueGenerator("field \"SendBack\" of the RequestInfo class");
            fieldGeneratorPreferredImageSize = new FieldHoldingArrayGeneratorPreferredImageSize("field \"PreferredImageSize\" of the RequestInfo class");
            fieldGeneratorInputLanguageEnglishName = new JSONHoldingStringGenerator("field \"InputLanguageEnglishName\" of the RequestInfo class");
            fieldGeneratorInputLanguageNativeName = new JSONHoldingStringGenerator("field \"InputLanguageNativeName\" of the RequestInfo class");
            fieldGeneratorInputLanguageIETFTag = new JSONHoldingStringGenerator("field \"InputLanguageIETFTag\" of the RequestInfo class");
            fieldGeneratorOutputLanguageEnglishName = new JSONHoldingStringGenerator("field \"OutputLanguageEnglishName\" of the RequestInfo class");
            fieldGeneratorOutputLanguageNativeName = new JSONHoldingStringGenerator("field \"OutputLanguageNativeName\" of the RequestInfo class");
            fieldGeneratorOutputLanguageIETFTag = new JSONHoldingStringGenerator("field \"OutputLanguageIETFTag\" of the RequestInfo class");
            fieldGeneratorResultVersionAccepted = new JSONHoldingNumberTextGenerator("field \"ResultVersionAccepted\" of the RequestInfo class");
            fieldGeneratorUnitPreference = new FieldHoldingGeneratorUnitPreference("field \"UnitPreference\" of the RequestInfo class");
            fieldGeneratorDefaultTimeFormat24Hours = new JSONHoldingBooleanGenerator("field \"DefaultTimeFormat24Hours\" of the RequestInfo class");
            fieldGeneratorClientID = new JSONHoldingStringGenerator("field \"ClientID\" of the RequestInfo class");
            fieldGeneratorClientVersion = new FieldHoldingGeneratorClientVersion("field \"ClientVersion\" of the RequestInfo class", false);
            fieldGeneratorDeviceID = new JSONHoldingStringGenerator("field \"DeviceID\" of the RequestInfo class");
            fieldGeneratorSDK = new JSONHoldingStringGenerator("field \"SDK\" of the RequestInfo class");
            fieldGeneratorSDKInfo = new JSONHoldingObjectValueGenerator("field \"SDKInfo\" of the RequestInfo class");
            fieldGeneratorFirstPersonSelf = new JSONHoldingStringGenerator("field \"FirstPersonSelf\" of the RequestInfo class");
            fieldGeneratorFirstPersonSelfSpoken = new JSONHoldingStringGenerator("field \"FirstPersonSelfSpoken\" of the RequestInfo class");
            fieldGeneratorSecondPersonSelf = new JSONHoldingStringArrayGenerator("field \"SecondPersonSelf\" of the RequestInfo class");
            fieldGeneratorSecondPersonSelfSpoken = new JSONHoldingStringArrayGenerator("field \"SecondPersonSelfSpoken\" of the RequestInfo class");
            fieldGeneratorWakeUpPhraseIncludedInAudio = new JSONHoldingBooleanGenerator("field \"WakeUpPhraseIncludedInAudio\" of the RequestInfo class");
            fieldGeneratorInitialSecondsOfAudioToIgnore = new JSONHoldingNumberTextGenerator("field \"InitialSecondsOfAudioToIgnore\" of the RequestInfo class");
            fieldGeneratorWakeUpPattern = new JSONHoldingStringGenerator("field \"WakeUpPattern\" of the RequestInfo class");
            fieldGeneratorUserID = new JSONHoldingStringGenerator("field \"UserID\" of the RequestInfo class");
            fieldGeneratorRequestID = new JSONHoldingStringGenerator("field \"RequestID\" of the RequestInfo class");
            fieldGeneratorSessionID = new JSONHoldingStringGenerator("field \"SessionID\" of the RequestInfo class");
            fieldGeneratorDomains = new DomainsJSON.HoldingGenerator("field \"Domains\" of the RequestInfo class", false);
            fieldGeneratorResultUpdateAllowed = new JSONHoldingBooleanGenerator("field \"ResultUpdateAllowed\" of the RequestInfo class");
            fieldGeneratorPartialTranscriptsDesired = new JSONHoldingBooleanGenerator("field \"PartialTranscriptsDesired\" of the RequestInfo class");
            fieldGeneratorMinResults = new FieldHoldingGeneratorMinResults("field \"MinResults\" of the RequestInfo class");
            fieldGeneratorMaxResults = new FieldHoldingGeneratorMaxResults("field \"MaxResults\" of the RequestInfo class");
            fieldGeneratorObjectByteCountPrefix = new JSONHoldingBooleanGenerator("field \"ObjectByteCountPrefix\" of the RequestInfo class");
            fieldGeneratorProfanityFilter = new FieldHoldingGeneratorProfanityFilter("field \"ProfanityFilter\" of the RequestInfo class");
            fieldGeneratorClientMatches = new ClientMatchJSON.HoldingArrayGenerator("field \"ClientMatches\" of the RequestInfo class", false);
            fieldGeneratorClientMatchesOnly = new JSONHoldingBooleanGenerator("field \"ClientMatchesOnly\" of the RequestInfo class");
            fieldGeneratorPagination = new PaginationJSON.HoldingGenerator("field \"Pagination\" of the RequestInfo class", false);
            fieldGeneratorResponseAudioVoice = new JSONHoldingStringGenerator("field \"ResponseAudioVoice\" of the RequestInfo class");
            fieldGeneratorResponseAudioShortOrLong = new FieldHoldingGeneratorResponseAudioShortOrLong("field \"ResponseAudioShortOrLong\" of the RequestInfo class");
            fieldGeneratorResponseAudioAcceptedEncodings = new FieldHoldingArrayGeneratorResponseAudioAcceptedEncodings("field \"ResponseAudioAcceptedEncodings\" of the RequestInfo class");
            fieldGeneratorReturnResponseAudioAsURL = new JSONHoldingBooleanGenerator("field \"ReturnResponseAudioAsURL\" of the RequestInfo class");
            fieldGeneratorVoiceActivityDetection = new VoiceActivityDetectionJSON.HoldingGenerator("field \"VoiceActivityDetection\" of the RequestInfo class", false);
            fieldGeneratorServerDeterminesEndOfAudio = new JSONHoldingBooleanGenerator("field \"ServerDeterminesEndOfAudio\" of the RequestInfo class");
            fieldGeneratorIntentOnly = new JSONHoldingBooleanGenerator("field \"IntentOnly\" of the RequestInfo class");
            fieldGeneratorDisableSpellCorrection = new JSONHoldingBooleanGenerator("field \"DisableSpellCorrection\" of the RequestInfo class");
            fieldGeneratorUseContactData = new JSONHoldingBooleanGenerator("field \"UseContactData\" of the RequestInfo class");
            fieldGeneratorUseClientTime = new JSONHoldingBooleanGenerator("field \"UseClientTime\" of the RequestInfo class");
            fieldGeneratorForceConversationStateTime = new FieldHoldingGeneratorForceConversationStateTime("field \"ForceConversationStateTime\" of the RequestInfo class");
            fieldGeneratorOutputLatticeSize = new FieldHoldingGeneratorOutputLatticeSize("field \"OutputLatticeSize\" of the RequestInfo class");
            fieldGeneratorMatchingMutations = new MatchingMutationsJSON.HoldingGenerator("field \"MatchingMutations\" of the RequestInfo class", false);
            fieldGeneratorUseFormattedTranscriptionAsDefault = new JSONHoldingBooleanGenerator("field \"UseFormattedTranscriptionAsDefault\" of the RequestInfo class");
            fieldGeneratorResponseRanking = new ResponseRankingJSON.HoldingGenerator("field \"ResponseRanking\" of the RequestInfo class", false);
            unknownFieldGenerator = new UnknownFieldGenerator(false);
            set_what("the RequestInfo class");
            allow_incomplete = false;
            allow_unpolished = false;
          }

        public override void reset()
          {
            fieldGeneratorLatitude.reset();
            fieldGeneratorLongitude.reset();
            fieldGeneratorPositionTime.reset();
            fieldGeneratorPositionHorizontalAccuracy.reset();
            fieldGeneratorStreet.reset();
            fieldGeneratorCity.reset();
            fieldGeneratorState.reset();
            fieldGeneratorCountry.reset();
            fieldGeneratorRoutePoints.reset();
            fieldGeneratorRouteInformation.reset();
            fieldGeneratorControllableTrackPlaying.reset();
            fieldGeneratorTimeStamp.reset();
            fieldGeneratorTimeZone.reset();
            fieldGeneratorConversationState.reset();
            fieldGeneratorClientState.reset();
            fieldGeneratorDeviceInfo.reset();
            fieldGeneratorSendBack.reset();
            fieldGeneratorPreferredImageSize.reset();
            fieldGeneratorInputLanguageEnglishName.reset();
            fieldGeneratorInputLanguageNativeName.reset();
            fieldGeneratorInputLanguageIETFTag.reset();
            fieldGeneratorOutputLanguageEnglishName.reset();
            fieldGeneratorOutputLanguageNativeName.reset();
            fieldGeneratorOutputLanguageIETFTag.reset();
            fieldGeneratorResultVersionAccepted.reset();
            fieldGeneratorUnitPreference.reset();
            fieldGeneratorDefaultTimeFormat24Hours.reset();
            fieldGeneratorClientID.reset();
            fieldGeneratorClientVersion.reset();
            fieldGeneratorDeviceID.reset();
            fieldGeneratorSDK.reset();
            fieldGeneratorSDKInfo.reset();
            fieldGeneratorFirstPersonSelf.reset();
            fieldGeneratorFirstPersonSelfSpoken.reset();
            fieldGeneratorSecondPersonSelf.reset();
            fieldGeneratorSecondPersonSelfSpoken.reset();
            fieldGeneratorWakeUpPhraseIncludedInAudio.reset();
            fieldGeneratorInitialSecondsOfAudioToIgnore.reset();
            fieldGeneratorWakeUpPattern.reset();
            fieldGeneratorUserID.reset();
            fieldGeneratorRequestID.reset();
            fieldGeneratorSessionID.reset();
            fieldGeneratorDomains.reset();
            fieldGeneratorResultUpdateAllowed.reset();
            fieldGeneratorPartialTranscriptsDesired.reset();
            fieldGeneratorMinResults.reset();
            fieldGeneratorMaxResults.reset();
            fieldGeneratorObjectByteCountPrefix.reset();
            fieldGeneratorProfanityFilter.reset();
            fieldGeneratorClientMatches.reset();
            fieldGeneratorClientMatchesOnly.reset();
            fieldGeneratorPagination.reset();
            fieldGeneratorResponseAudioVoice.reset();
            fieldGeneratorResponseAudioShortOrLong.reset();
            fieldGeneratorResponseAudioAcceptedEncodings.reset();
            fieldGeneratorReturnResponseAudioAsURL.reset();
            fieldGeneratorVoiceActivityDetection.reset();
            fieldGeneratorServerDeterminesEndOfAudio.reset();
            fieldGeneratorIntentOnly.reset();
            fieldGeneratorDisableSpellCorrection.reset();
            fieldGeneratorUseContactData.reset();
            fieldGeneratorUseClientTime.reset();
            fieldGeneratorForceConversationStateTime.reset();
            fieldGeneratorOutputLatticeSize.reset();
            fieldGeneratorMatchingMutations.reset();
            fieldGeneratorUseFormattedTranscriptionAsDefault.reset();
            fieldGeneratorResponseRanking.reset();
            base.reset();
            unknownFieldGenerator.reset();
          }
        public void set_allow_incomplete(bool new_allow_incomplete)
          {
            allow_incomplete = new_allow_incomplete;
            fieldGeneratorRoutePoints.set_allow_incomplete(new_allow_incomplete);
            fieldGeneratorRouteInformation.set_allow_incomplete(new_allow_incomplete);
            fieldGeneratorConversationState.set_allow_incomplete(new_allow_incomplete);
            fieldGeneratorClientState.set_allow_incomplete(new_allow_incomplete);
            fieldGeneratorDeviceInfo.set_allow_incomplete(new_allow_incomplete);
            fieldGeneratorDomains.set_allow_incomplete(new_allow_incomplete);
            fieldGeneratorClientMatches.set_allow_incomplete(new_allow_incomplete);
            fieldGeneratorPagination.set_allow_incomplete(new_allow_incomplete);
            fieldGeneratorVoiceActivityDetection.set_allow_incomplete(new_allow_incomplete);
            fieldGeneratorMatchingMutations.set_allow_incomplete(new_allow_incomplete);
            fieldGeneratorResponseRanking.set_allow_incomplete(new_allow_incomplete);
          }
        public void set_allow_unpolished(bool new_allow_unpolished)
          {
            allow_unpolished = new_allow_unpolished;
            fieldGeneratorRoutePoints.set_allow_unpolished(new_allow_unpolished);
            fieldGeneratorRouteInformation.set_allow_unpolished(new_allow_unpolished);
            fieldGeneratorConversationState.set_allow_unpolished(new_allow_unpolished);
            fieldGeneratorClientState.set_allow_unpolished(new_allow_unpolished);
            fieldGeneratorDeviceInfo.set_allow_unpolished(new_allow_unpolished);
            fieldGeneratorDomains.set_allow_unpolished(new_allow_unpolished);
            fieldGeneratorClientMatches.set_allow_unpolished(new_allow_unpolished);
            fieldGeneratorPagination.set_allow_unpolished(new_allow_unpolished);
            fieldGeneratorVoiceActivityDetection.set_allow_unpolished(new_allow_unpolished);
            fieldGeneratorMatchingMutations.set_allow_unpolished(new_allow_unpolished);
            fieldGeneratorResponseRanking.set_allow_unpolished(new_allow_unpolished);
          }
      };
    public class HoldingGenerator : Generator
      {
        protected override void handle_result(RequestInfoJSON  result)
          {
//@@@            Debug.Assert(!have_value);
            have_value = true;
            value = result;
          }

        public HoldingGenerator(String what, bool ignore_extras) : base(ignore_extras)
          {
            have_value = false;
            base.set_what(what);
          }

        public HoldingGenerator(String what) : base(false)
          {
            have_value = false;
            base.set_what(what);
          }

        public override void reset()
          {
            have_value = false;
            base.reset();
          }

        public bool have_value;
        public RequestInfoJSON value;
      };
    public class HoldingArrayGenerator : JSONArrayGenerator
  {
    protected class ElementHandler : Generator
      {
        private HoldingArrayGenerator top;

        protected override void handle_result(RequestInfoJSON  result)
          {
            top.value.Add(result);
          }
        protected override string get_what()
          {
            return "element " + top.value.Count + " of " + top.get_what();
          }

        public ElementHandler(HoldingArrayGenerator init_top, bool ignore_extras) : base(ignore_extras)
          {
            top = init_top;
          }
      };

    private ElementHandler element_handler;

    protected override JSONHandler start()
      {
        have_value = true;
        value.Clear();
        return element_handler;
      }
    protected override void finish()
      {
        Debug.Assert(have_value);
        handle_result(value);
        element_handler.reset();
      }
    protected virtual void handle_result(List<RequestInfoJSON> result)

      {
      }

    public HoldingArrayGenerator(string what, bool ignore_extras)
      {
        element_handler = new ElementHandler(this, ignore_extras);
        have_value = false;
        value = new List<RequestInfoJSON>();
        base.set_what(what);
      }
    public HoldingArrayGenerator(bool ignore_extras)
      {
        element_handler = new ElementHandler(this, ignore_extras);
        value = new List<RequestInfoJSON>();
        have_value = false;
      }

    public void set_allow_incomplete(bool new_allow_incomplete)
      {
        element_handler.set_allow_incomplete(new_allow_incomplete);
      }

    public void set_allow_unpolished(bool new_allow_unpolished)
      {
        element_handler.set_allow_unpolished(new_allow_unpolished);
      }

    public override void reset()
      {
        element_handler.reset();
        have_value = false;
        value.Clear();
        base.reset();
      }

    public bool have_value;
    public List<RequestInfoJSON> value;
  };
  };
