using System.Collections;


namespace Houndify {
    public class Settings
    {
        public static string ClientId = "ADD-YOUR-CLIENT-ID-HERE";
        public static string ClientKey = "ADD-YOUR-CLIENT-KEY-HERE";
        public static string UserId = "someuser"; // this can be different for every user

        // Change this to the voice you'd like to use. You can try different voices like:
        // Lily, Ryan, Tracy or any of the ones mentioned in the documentation of 
        // the TTS domain (link below)
        // https://www.houndify.com/domains/540c271e-cf06-4f33-ab26-731f0bd9b79d
        public static string TTSVoice = "Ryan";
    }
}