using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Houndify;
using WWUtils.Audio;
using UnityToolbag;

public class CapsuleScript : MonoBehaviour
{
    public TextMesh caption;
    public Material defaultMat;
    public Material listeningMat;

    private AudioSource audioSource;
    private HoundCloudRequester requester;
    private RequestInfoJSON request_info;
    private HoundRequester.VoiceRequest request;
    private HoundServerJSON hound_result;
    private PartialHandler partialHandler;

    private AudioClip m_clip;
    private int lastPos, pos;
    private bool sending = false;
    private bool stopRecording = false;
    private AudioClip wav_clip;
    private int diff;
    private String audio_file = "request_audio.wav";
    private byte[] audio_stream;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        // New requester and partial response handler
        requester = new HoundCloudRequester(Settings.ClientId, Settings.ClientKey, Settings.UserId);
        partialHandler = new PartialHandler(true, this);

        // RequestInfoJSON class contains metadata about the current request,
        // like session_id, request id, client_version, etc.
        request_info = new RequestInfoJSON();
        request_info.setRequestID(Guid.NewGuid().ToString());
        request_info.setUnitPreference(RequestInfoJSON.TypeUnitPreference.UnitPreference_US);
        request_info.setLatitude(37.322911);
        request_info.setLongitude(-122.032308);

        // We need to set these two flag so the responses are vocalized
        request_info.setResponseAudioVoice(Settings.TTSVoice);
        request_info.setResponseAudioShortOrLong("Long");
        
        // Set the language for input and output
        request_info.setInputLanguageEnglishName("English");
        request_info.setOutputLanguageEnglishName("English");

        RequestInfoJSON.TypeClientVersion client_version = new RequestInfoJSON.TypeClientVersion(); 
        client_version.key = 0;
        client_version.choice0 = "1.0";
        request_info.setClientVersion(client_version);
    }

    // Update is called once per frame
    void Update()
    {
        // Check if the spacebar was pressed
        if (Input.GetKeyDown(KeyCode.Space))
        {
            this.GetComponent<Renderer>().material = listeningMat;
            StartAudioQuery();
            caption.text = "Listening...";
        }

        // Update the cube rotation
        transform.Rotate(30.0f * Time.deltaTime, 0f, 30.0f * Time.deltaTime);

        // Handle voice query tasks depending on state
        if (sending)
        {
            if (stopRecording)
            {
                Microphone.End(Microphone.devices[0]);
            }
            else
            {
                pos = Microphone.GetPosition(null);
                diff = pos - lastPos;
                if (diff > 0)
                {
                    float[] samples = new float[diff * m_clip.channels];
                    m_clip.GetData(samples, lastPos);
                    wav_clip = AudioClip.Create("", samples.Length, m_clip.channels, 16000, true, false);
                    wav_clip.SetData(samples, 0);
                    if (lastPos == 0)
                    {
                        audio_stream = WavUtility.FromAudioClip(wav_clip, out audio_file, true);
                    }
                    else
                    {
                        audio_stream = WavUtility.ConvertToByteArray(wav_clip);
                    }
                    request.add_audio(audio_stream.Length, audio_stream);
                    lastPos = pos;
                }
            }
        }
    }

    private void StartAudioQuery()
    {
        m_clip = Microphone.Start(Microphone.devices[0], true, 1000, 16000);
        lastPos = 0;
        sending = true;
        stopRecording = false;
        StartCoroutine("Record");
    }

    private IEnumerator Record()
    {
        // Create a HoundRequester.VoiceRequest object
        request = requester.start_voice_request(null, request_info, partialHandler);

        sending = true;
        stopRecording = false;
        while (Microphone.IsRecording(Microphone.devices[0]))
        {
            yield return null;
        }
        sending = false;

        // This is on a different thread to prevent blocking the animation
        // when processing the response
        Task.Run(() => {
            hound_result = request.finish();

            Debug.Log("stopped!");

            // Uncomment below if you want to print out the entire response JSON
            // StringWriter txt = new StringWriter();
            // JSONStreamWriter h = new JSONStreamWriter(txt);
            // hound_result.write_as_json(h);
            // Debug.Log(txt.GetStringBuilder().ToString());


            Dispatcher.Invoke(() =>
            {
                // Update caption
                caption.text = hound_result.getAllResults()[0].getWrittenResponseLong();

                // Play the clip back
                if (hound_result.getAllResults().Count > 0)
                {
                    WAV wav = new WAV(System.Convert.FromBase64String(hound_result.getAllResults()[0].getResponseAudioBytes()));
                    AudioClip audioClip = AudioClip.Create("outputSound", wav.SampleCount, 1, wav.Frequency, false, false);
                    audioClip.SetData(wav.LeftChannel, 0);
                    audioSource.clip = audioClip;
                    audioSource.Play();
                } else
                {
                    Debug.Log("NO RESULT - ERROR FROM HOUNDIFY");
                }
                this.GetComponent<Renderer>().material = defaultMat;
            });
        });
        

    }


    // A partial handler class (used to handle partial transcripts)
    // Here we update the caption on the screen so that the user
    // can see what they said being transcribed. We can also
    // print this to the debug output
    class PartialHandler : HoundRequester.PartialHandler
    {
        private bool show_transcript;
        private CapsuleScript parent;

        public PartialHandler(bool show_transcript, CapsuleScript parent)
        {
            this.show_transcript = show_transcript;
            this.parent = parent;
        }

        // The handle method is called whenever a partial transcript is received
        public override void handle(HoundPartialTranscriptJSON partial)
        {
            if (show_transcript)
            {
                //Debug.Log(partial.getPartialTranscript());
                Dispatcher.Invoke(() => {
                    parent.caption.text = partial.getPartialTranscript();
                });

                if (partial.getSafeToStopAudio())
                {
                    parent.stopRecording = true;
                }
            }
        }
    }

}
